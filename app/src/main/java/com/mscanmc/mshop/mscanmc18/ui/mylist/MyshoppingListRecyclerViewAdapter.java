package com.mscanmc.mshop.mscanmc18.ui.mylist;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListDetail;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentUserListItemBinding;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

import static com.mscanmc.mshop.mscanmc18.data.PreferencesHelper.PREF_FILE_NAME;
import static com.mscanmc.mshop.mscanmc18.data.PreferencesHelper.SAVED_LIST;
import static com.mscanmc.mshop.mscanmc18.data.PreferencesHelper.SELECTED_LIST;


public class MyshoppingListRecyclerViewAdapter
        extends RecyclerView.Adapter<MyshoppingListRecyclerViewAdapter.ViewHolder> {

    private final List<ShoppingListDetail> mValues;
    private final ShoppingListFragment.onListItemClick mListener;
    private final OnMyListItemClick onMyListItemClick;

    @Inject
    PreferencesHelper preferencesHelper;
    FragmentUserListItemBinding mBinding;

    public MyshoppingListRecyclerViewAdapter(
            List<ShoppingListDetail> items, ShoppingListFragment.onListItemClick listener, OnMyListItemClick onMyListItemClick) {
        mValues = items;
        mListener = listener;
        this.onMyListItemClick = onMyListItemClick;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        mBinding =
                DataBindingUtil.inflate(
                        layoutInflater, R.layout.fragment_user_list_item, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.binding.executePendingBindings();
        mBinding.setShoppingDetail(mValues.get(position));
        holder.binding.setOnClickUserNow(v -> {
            SharedPreferences sharedPreferences = mBinding.getRoot().getContext().getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(SAVED_LIST,mValues.get(position).getLabel());
            editor.putBoolean(SELECTED_LIST,true);
            editor.apply();
            mListener.onListItemClick(mValues.get(position));
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void refreshData(List<ShoppingListDetail> shoppingList) {
        this.mValues.clear();
        this.mValues.addAll(shoppingList);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        FragmentUserListItemBinding binding;

        public ViewHolder(FragmentUserListItemBinding view) {
            super(view.getRoot());
            binding = view;
        }
    }

    public interface OnMyListItemClick {
        public void onMyListitemClick(int idShoppingList);
    }
}
