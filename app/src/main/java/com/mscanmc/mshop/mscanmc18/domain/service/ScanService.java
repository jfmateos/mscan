package com.mscanmc.mshop.mscanmc18.domain.service;

import com.mscanmc.mshop.mscanmc18.data.request.GetEntityListRequest;
import com.mscanmc.mshop.mscanmc18.data.request.ShopperScanRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoItemInitialRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TurnRequest;
import com.mscanmc.mshop.mscanmc18.data.request.UpdateConfigRequest;
import com.mscanmc.mshop.mscanmc18.data.response.DeskListGeneric;
import com.mscanmc.mshop.mscanmc18.data.response.FinishSessionResponse;
import com.mscanmc.mshop.mscanmc18.data.response.GenericMessageResponse;
import com.mscanmc.mshop.mscanmc18.data.response.HistoricGenericResponse;
import com.mscanmc.mshop.mscanmc18.data.response.NotificationResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ProductQuantityResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShopperScanResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingList;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListLastShopping;
import com.mscanmc.mshop.mscanmc18.data.response.StoreDetailResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface ScanService {


    @POST("/alcampoApp/services/scanDevice/UpdateConfig")
    Observable<GenericMessageResponse> updateConfig(@Body UpdateConfigRequest updateConfigRequest);

    @POST("/alcampoApp/services/registration/ConnectShopperScan")
    Observable<ShopperScanResponse> connectShopperScan(@Body ShopperScanRequest shopperScanRequest);

    @GET("/alcampoApp/services/storeGasStation/GetStoreDetails")
    Observable<StoreDetailResponse> getStoreDetail(@QueryMap Map<String, String> map);

    @GET("/alcampoApp/services/shoppingList/GetListOfShoppingList")
    Observable<ShoppingList> getListOfShoppingList(@QueryMap HashMap<String, String> params);


    @GET("/alcampoApp/services/historicBasket/GetListOfHistorics")
    Observable<HistoricGenericResponse> getHistoricList(@QueryMap Map<String, String> map);

    @POST("/alcampoApp/services/turn/GetDeskList")
    Observable<DeskListGeneric> getDeskTurn(@Body TurnRequest turnRequest);

    @GET("/alcampoApp/services/trip/EndOfTrip")
    Observable<FinishSessionResponse> endOfTrip(@QueryMap HashMap<String, String> params);

    @POST("/alcampoApp/services/turn/GiveTurn")
    Call<DeskListGeneric> getTurnRequest(@Body TurnRequest turnRequest);

    @GET("/alcampoApp/services/communication/GetListOfEvents")
    Observable<NotificationResponse> getNotifications(@QueryMap HashMap<String, String> stringStringHashMap);

    @POST("/alcampoApp/services/trip/SetProductQuantity")
    Observable<ProductQuantityResponse> SetProductQuantity(@Body TripDtoItemInitialRequest tripSetProductListDto);

    @POST("/alcampoApp/services/trip/SetProductQuantity")
    Observable<ProductQuantityResponse> SetProductQuantity(@Body TripDtoRequest tripSetProductListDto);

    @POST("/alcampoApp/services/trip/SetProductQuantity")
    Observable<ProductQuantityResponse> removeItem(@Body TripDtoRequest tripSetProductListDto);

    @POST("/alcampoApp/services/shoppingList/GetShoppingListDetails")
    Observable<ShoppingListLastShopping> getShoppingListDetails(
            @Body GetEntityListRequest updateConfigRequest);
/*
    @GET("/alcampoApp/services/products/GetPromotionsOfTheDay")
    Observable<PromoResponse> getPromotionsOfTheDay(@QueryMap HashMap<String, String> params);*/
}
