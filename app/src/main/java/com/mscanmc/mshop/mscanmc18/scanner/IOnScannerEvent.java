package com.mscanmc.mshop.mscanmc18.scanner;

import com.symbol.emdk.barcode.ScanDataCollection;

public interface IOnScannerEvent {

    //Function is called to pass scanned data
    void onDataScanned(ScanDataCollection scanData);

    //Function is called to pass scanner status
    void onStatusUpdate(String scanStatus);

    //Function to be called upon error or exception
    void onError();
}
