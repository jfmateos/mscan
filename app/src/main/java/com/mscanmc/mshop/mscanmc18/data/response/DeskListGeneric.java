package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeskListGeneric {

    @SerializedName("deskList")
    @Expose
    private List<DeskList> deskList = null;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     */
    public DeskListGeneric() {
    }

    /**
     * @param executeTime
     * @param deskList
     */
    public DeskListGeneric(List<DeskList> deskList, double executeTime) {
        super();
        this.deskList = deskList;
        this.executeTime = executeTime;
    }

    public List<DeskList> getDeskList() {
        return deskList;
    }

    public void setDeskList(List<DeskList> deskList) {
        this.deskList = deskList;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

}
