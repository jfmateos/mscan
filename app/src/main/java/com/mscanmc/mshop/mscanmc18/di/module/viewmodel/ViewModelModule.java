package com.mscanmc.mshop.mscanmc18.di.module.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.mscanmc.mshop.mscanmc18.ui.MainViewModel;
import com.mscanmc.mshop.mscanmc18.ui.base.ViewModelFactory;
import com.mscanmc.mshop.mscanmc18.ui.cart.CartViewModel;
import com.mscanmc.mshop.mscanmc18.ui.mylist.ShoppingListViewModel;
import com.mscanmc.mshop.mscanmc18.ui.notifications.NotificationViewModel;
import com.mscanmc.mshop.mscanmc18.ui.splash.SplashViewModel;
import com.mscanmc.mshop.mscanmc18.ui.turn.TurnViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Module
public interface ViewModelModule {
    @Binds
    ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    ViewModel bindSplashViewModel(SplashViewModel splashViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    ViewModel bindMainViewmodel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CartViewModel.class)
    ViewModel bindCartViewModel(CartViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ShoppingListViewModel.class)
    ViewModel bindShoppingViewModel(ShoppingListViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TurnViewModel.class)
    ViewModel bindTurnViewModel(TurnViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NotificationViewModel.class)
    ViewModel bindNotificationViewModel(NotificationViewModel mainViewModel);
}
