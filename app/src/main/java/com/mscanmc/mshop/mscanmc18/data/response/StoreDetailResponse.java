package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StoreDetailResponse {

    @SerializedName("errors")
    @Expose
    private List<String> errors = null;
    @SerializedName("store")
    @Expose
    private StoreResponse store;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     */
    public StoreDetailResponse() {
    }

    /**
     * @param errors
     * @param store
     * @param executeTime
     */
    public StoreDetailResponse(List<String> errors, StoreResponse store, double executeTime) {
        super();
        this.errors = errors;
        this.store = store;
        this.executeTime = executeTime;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public StoreResponse getStore() {
        return store;
    }

    public void setStore(StoreResponse store) {
        this.store = store;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

}
