package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericMessageResponse {

    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    public GenericMessageResponse() {}

    public GenericMessageResponse(double executeTime) {
        this.executeTime = executeTime;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }
}
