package com.mscanmc.mshop.mscanmc18.utils;


public class Constants {
    public static final String TOKEN_SENT = "TOKEN_SENT";
    public static final String BROADCAST_PUSH = "BROADCAST_PUSH";
    public static final String BROADCAST_UPDATED_TOKEN = "BROADCAST_UPDATED_TOKEN";
    public static final String BROADCAST_PUSH_MESSAGE = "BROADCAST_PUSH_MESSAGE";
    public static final String BROADCAST_PUSH_ID = "BROADCAST_PUSH_ID";
    public static final String BROADCAST_PUSH_ENCODED_ID = "BROADCAST_PUSH_ENCODED_ID";
    public static final String STATUS = "STATUS";
    public static final String EAN13 = "EAN13";
    public static final String DISCOUNT_PROMO = "D";
    public static final String DISCOUNT_PRODUCT_PROMO = "P";
    public static final String OFFERS = "OFFERS";
    public static String RELEASE_SCANNER = "DEVICE_RELEASED";
    public static int REQUEST_CODE_TURNS = 1013;
    public static int REQUEST_CODE_CATALOGS = 1014;
    public static String END_OF_TRIP_CODE = "6712345";
    public static String AUDIT_REQUIRED = "AUDIT_REQUIRED";
    public static String WAITING_ASSISTANCE = "WAITING_ASSISTANCE";
    public static String WAITING_TO_PAY = "WAITING_TO_PAY";
    public static final int EOT_DIALOG = 1;
    public static final int AUDIT_DIALOG = 2;
    public static final int WAITING_DIALOG = 3;
    public static final String UNKNOWN_PRODUCT = "UNKNOWN_PRODUCT";

}

