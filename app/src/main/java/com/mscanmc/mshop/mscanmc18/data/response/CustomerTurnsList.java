package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerTurnsList {

    @SerializedName("ticketCode")
    @Expose
    private double ticketCode;

    @SerializedName("activeTurn")
    @Expose
    private boolean activeTurn;

    @SerializedName("entityName")
    @Expose
    private String entityName;

    @SerializedName("deskName")
    @Expose
    private String deskName;

    @SerializedName("myTurn")
    @Expose
    private String myTurn;

    /** No args constructor for use in serialization */
    public CustomerTurnsList() {}

    /**
     * @param activeTurn
     * @param ticketCode
     * @param myTurn
     * @param deskName
     * @param entityName
     */
    public CustomerTurnsList(
            double ticketCode,
            boolean activeTurn,
            String entityName,
            String deskName,
            String myTurn) {
        super();
        this.ticketCode = ticketCode;
        this.activeTurn = activeTurn;
        this.entityName = entityName;
        this.deskName = deskName;
        this.myTurn = myTurn;
    }

    public double getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(double ticketCode) {
        this.ticketCode = ticketCode;
    }

    public boolean getActiveTurn() {
        return activeTurn;
    }

    public void setActiveTurn(boolean activeTurn) {
        this.activeTurn = activeTurn;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getDeskName() {
        return deskName;
    }

    public void setDeskName(String deskName) {
        this.deskName = deskName;
    }

    public String getMyTurn() {
        return myTurn;
    }

    public void setMyTurn(String myTurn) {
        this.myTurn = myTurn;
    }
}

