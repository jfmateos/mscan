package com.mscanmc.mshop.mscanmc18.di.component;

import com.mscanmc.mshop.mscanmc18.App;
import com.mscanmc.mshop.mscanmc18.di.module.ActivityModule;
import com.mscanmc.mshop.mscanmc18.di.module.AndroidModule;
import com.mscanmc.mshop.mscanmc18.di.module.AppModule;
import com.mscanmc.mshop.mscanmc18.di.module.network.GsonModule;
import com.mscanmc.mshop.mscanmc18.di.module.network.OkHttpInterceptorsModule;
import com.mscanmc.mshop.mscanmc18.ui.MainActivity;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseActivity;
import com.mscanmc.mshop.mscanmc18.ui.cart.ProductFragment;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                AppModule.class,
                AndroidModule.class,
                GsonModule.class,
                OkHttpInterceptorsModule.class,
                ActivityModule.class
        })
public interface AppComponent {
    @Component.Builder
    interface Builder {
        AppComponent build();

        Builder appModule(AppModule appModule);

        Builder okHttpInterceptorsModule(OkHttpInterceptorsModule okHttpInterceptorsModule);

        Builder gsonModule(GsonModule gsonModule);

        Builder androidModule(AndroidModule androidModule);

        //Builder sharedModule(SharedPreferencesModule sharedPreferencesModule);
    }

    void inject(App application);

    void inject(MainActivity mainActivity);

    //void inject(SplashActivity splash);

    //void inject(SharedPreferencesModule sharedPreferencesModule);

    void inject(BaseActivity baseActivity);

    //void inject(ProductFragment productFragment);
}
