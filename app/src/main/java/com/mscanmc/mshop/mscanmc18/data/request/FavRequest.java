package com.mscanmc.mshop.mscanmc18.data.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavRequest {

    @SerializedName("tokenSession")
    @Expose
    private String tokenSession;

    @SerializedName("idEntity")
    @Expose
    private int idEntity;

    @SerializedName("entityFav")
    @Expose
    private int entityFav;

    /** No args constructor for use in serialization */
    public FavRequest() {}

    /**
     * @param tokenSession
     * @param idEntity
     * @param entityFav
     */
    public FavRequest(String tokenSession, int idEntity, int entityFav) {
        super();
        this.tokenSession = tokenSession;
        this.idEntity = idEntity;
        this.entityFav = entityFav;
    }

    public String getTokenSession() {
        return tokenSession;
    }

    public void setTokenSession(String tokenSession) {
        this.tokenSession = tokenSession;
    }

    public double getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(int idEntity) {
        this.idEntity = idEntity;
    }

    public int getEntityFav() {
        return entityFav;
    }

    public void setEntityFav(int entityFav) {
        this.entityFav = entityFav;
    }
}

