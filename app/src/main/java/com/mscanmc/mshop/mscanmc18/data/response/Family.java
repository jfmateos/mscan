package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Family {
    @SerializedName("id")
    @Expose
    private double id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("key")
    @Expose
    private double key;
    @SerializedName("new")
    @Expose
    private String _new;

    /**
     * No args constructor for use in serialization
     */
    public Family() {
    }

    /**
     * @param id
     * @param category
     * @param description
     * @param _new
     * @param code
     * @param key
     */
    public Family(double id, String code, String description, String category, double key, String _new) {
        super();
        this.id = id;
        this.code = code;
        this.description = description;
        this.category = category;
        this.key = key;
        this._new = _new;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getKey() {
        return key;
    }

    public void setKey(double key) {
        this.key = key;
    }

    public String getNew() {
        return _new;
    }

    public void setNew(String _new) {
        this._new = _new;
    }

}
