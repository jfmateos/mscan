package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Promo {

    @SerializedName("priceStore")
    @Expose
    private double priceStore;
    @SerializedName("unitPriceStore")
    @Expose
    private double unitPriceStore;
    @SerializedName("unitValuePromo")
    @Expose
    private double unitValuePromo;
    @SerializedName("previousPricePromo")
    @Expose
    private double previousPricePromo;
    @SerializedName("discountXy")
    @Expose
    private String discountXy;
    @SerializedName("promotionQtx")
    @Expose
    private String promotionQtx;
    @SerializedName("promotionQty")
    @Expose
    private String promotionQty;
    @SerializedName("unitMode")
    @Expose
    private String unitMode;
    @SerializedName("unitModeLabel")
    @Expose
    private String unitModeLabel;
    @SerializedName("urlImagenPromo")
    @Expose
    private String urlImagenPromo;
    @SerializedName("idProduct")
    @Expose
    private String idProduct;
    @SerializedName("productCodeValue")
    @Expose
    private String productCodeValue;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("idProductY")
    @Expose
    private String idProductY;
    @SerializedName("productYCodeValue")
    @Expose
    private String productYCodeValue;
    @SerializedName("productYName")
    @Expose
    private String productYName;
    @SerializedName("descriptionTextPromo")
    @Expose
    private String descriptionTextPromo;
    @SerializedName("promoFromDate")
    @Expose
    private String promoFromDate;


    @SerializedName("promoToDate")
    @Expose
    private String promoToDate;

    /**
     * No args constructor for use in serialization
     */
    public Promo() {
    }

    public Promo(double priceStore, double unitPriceStore, double unitValuePromo, double previousPricePromo, String discountXy, String promotionQtx, String promotionQty, String unitMode, String unitModeLabel, String urlImagenPromo, String idProduct, String productCodeValue, String productName, String idProductY, String productYCodeValue, String productYName, String descriptionTextPromo, String promoFromDate, String promoToDate) {
        this.priceStore = priceStore;
        this.unitPriceStore = unitPriceStore;
        this.unitValuePromo = unitValuePromo;
        this.previousPricePromo = previousPricePromo;
        this.discountXy = discountXy;
        this.promotionQtx = promotionQtx;
        this.promotionQty = promotionQty;
        this.unitMode = unitMode;
        this.unitModeLabel = unitModeLabel;
        this.urlImagenPromo = urlImagenPromo;
        this.idProduct = idProduct;
        this.productCodeValue = productCodeValue;
        this.productName = productName;
        this.idProductY = idProductY;
        this.productYCodeValue = productYCodeValue;
        this.productYName = productYName;
        this.descriptionTextPromo = descriptionTextPromo;
        this.promoFromDate = promoFromDate;
        this.promoToDate = promoToDate;
    }

    public double getPriceStore() {
        return priceStore;
    }

    public void setPriceStore(double priceStore) {
        this.priceStore = priceStore;
    }

    public double getUnitPriceStore() {
        return unitPriceStore;
    }

    public void setUnitPriceStore(double unitPriceStore) {
        this.unitPriceStore = unitPriceStore;
    }

    public double getUnitValuePromo() {
        return unitValuePromo;
    }

    public void setUnitValuePromo(double unitValuePromo) {
        this.unitValuePromo = unitValuePromo;
    }

    public double getPreviousPricePromo() {
        return previousPricePromo;
    }

    public void setPreviousPricePromo(double previousPricePromo) {
        this.previousPricePromo = previousPricePromo;
    }

    public String getDiscountXy() {
        return discountXy;
    }

    public void setDiscountXy(String discountXy) {
        this.discountXy = discountXy;
    }

    public String getPromotionQtx() {
        return promotionQtx;
    }

    public void setPromotionQtx(String promotionQtx) {
        this.promotionQtx = promotionQtx;
    }

    public String getPromotionQty() {
        return promotionQty;
    }

    public void setPromotionQty(String promotionQty) {
        this.promotionQty = promotionQty;
    }

    public String getUnitMode() {
        return unitMode;
    }

    public void setUnitMode(String unitMode) {
        this.unitMode = unitMode;
    }

    public String getUnitModeLabel() {
        return unitModeLabel;
    }

    public void setUnitModeLabel(String unitModeLabel) {
        this.unitModeLabel = unitModeLabel;
    }

    public String getUrlImagenPromo() {
        return urlImagenPromo;
    }

    public void setUrlImagenPromo(String urlImagenPromo) {
        this.urlImagenPromo = urlImagenPromo;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getProductCodeValue() {
        return productCodeValue;
    }

    public void setProductCodeValue(String productCodeValue) {
        this.productCodeValue = productCodeValue;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getIdProductY() {
        return idProductY;
    }

    public void setIdProductY(String idProductY) {
        this.idProductY = idProductY;
    }

    public String getProductYCodeValue() {
        return productYCodeValue;
    }

    public void setProductYCodeValue(String productYCodeValue) {
        this.productYCodeValue = productYCodeValue;
    }

    public String getProductYName() {
        return productYName;
    }

    public void setProductYName(String productYName) {
        this.productYName = productYName;
    }

    public String getDescriptionTextPromo() {
        return descriptionTextPromo;
    }

    public void setDescriptionTextPromo(String descriptionTextPromo) {
        this.descriptionTextPromo = descriptionTextPromo;
    }

    public String getPromoFromDate() {
        return promoFromDate;
    }

    public void setPromoFromDate(String promoFromDate) {
        this.promoFromDate = promoFromDate;
    }

    public String getPromoToDate() {
        return promoToDate;
    }

    public void setPromoToDate(String promoToDate) {
        this.promoToDate = promoToDate;
    }
}
