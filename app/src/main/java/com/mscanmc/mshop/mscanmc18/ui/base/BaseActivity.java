package com.mscanmc.mshop.mscanmc18.ui.base;

import android.app.Dialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.utils.DialogUtils;
import com.mscanmc.mshop.mscanmc18.utils.LifecycleAppCompatActivity;
import com.mscanmc.mshop.mscanmc18.utils.SnackBarUtil;

/** Created by Juan Francisco Mateos Redondo */
public class BaseActivity extends LifecycleAppCompatActivity{
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    protected Dialog progressDialog;
    private final static String mStrIntent = "com.symbol.intent.action.systembarvisibility";
    private final static String INTENT_EXTRA = "visibility";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        onWindowFocusChanged(false);
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
//                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }
}
