package com.mscanmc.mshop.mscanmc18.ui.cart;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.response.BasketItemResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ProductQuantityResponse;
import com.mscanmc.mshop.mscanmc18.data.response.Promo;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentCartItemBinding;
import com.mscanmc.mshop.mscanmc18.utils.Constants;
import com.mscanmc.mshop.mscanmc18.utils.DataUtil;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class CartRecyclerViewAdapter
        extends RecyclerView.Adapter<CartRecyclerViewAdapter.ViewHolder> {

    private List<BasketItemResponse> mValues = new ArrayList<>();
    private List<Promo> promoList = new ArrayList<>();
    private onItemAdapterIteractionListener mListener;
    private FragmentCartItemBinding mBinding;
    private Context context;

    public CartRecyclerViewAdapter(
            Context context,
            ProductQuantityResponse items,
            onItemAdapterIteractionListener listener) {
        BasketItemResponse basketItemResponseTmp = new BasketItemResponse();

        for (BasketItemResponse item : items.getBasket().getBasket()) {
            if (item.getQuantity() != 0) {
                mValues.add(item);
            }
        }
        if (items.getBasket().getPromo() != null) {
            for (Promo promo : items.getBasket().getPromo()) {
                if (promo != null) {
                    promoList.add(promo);
                }
            }
        }
        this.context = context;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        mBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.fragment_cart_item, parent, false);
        mBinding.setIsLoading(true);
        mBinding.setIsEdit(false);
        mBinding.setOnItemClickListener(this::onItemClick);
        return new ViewHolder(mBinding);
    }

    private void onItemClick(View view) {
        Timber.w("clid");
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.binding.setIsEdit(false);
        holder.binding.executePendingBindings();
        mBinding.setIsUnknonw(mValues.get(position).getProduct().getLabel().equalsIgnoreCase(Constants.UNKNOWN_PRODUCT));
        if (mValues.get(position).getQuantity() != 0) {
            mBinding.setBasket(mValues.get(position));
        }
        for (Promo promo : promoList) {
            if (promo.getProductCodeValue().equalsIgnoreCase(mValues.get(position).getProductInfoFull().getCodeValue())) {
                mBinding.tvPromoPrice.setText(DataUtil.setPromoPrice(mValues.get(position).getQuantity(), promo).concat("€"));
                mBinding.tvPromoPrice.setBackground(mBinding.getRoot().getResources().getDrawable(R.drawable.strike_line));
            }
        }

        if(mBinding.getBasket().getProductInfoFull().getUnitScannedPrice() != mBinding.getBasket().getProductInfoFull().getPrice()){
            mBinding.tvPromoPrice.setText(DataUtil.setPromoPrice(mValues.get(position).getQuantity(), mBinding.getBasket().getProductInfoFull().getPrice()).concat("€"));
            mBinding.tvPromoPrice.setBackground(mBinding.getRoot().getResources().getDrawable(R.drawable.strike_line));
        }

        holder.binding.clProductDetail.setOnTouchListener(
                new OnSwipeTouchListener(mBinding.getRoot().getContext()) {
                    @Override
                    public void onSwipeLeft() {
                        super.onSwipeLeft();
                        holder.binding.setIsEdit(true);
                        Timber.wtf(holder.binding.getBasket().getProductInfoFull().getProductName() + "   " + position);
                        holder.binding.clProductEdit.startAnimation(inFromRightAnimation());
                    }

                    @Override
                    public void onSwipeRight() {
                        super.onSwipeRight();
                        if (mBinding.getIsEdit()) {
                            holder.binding.clProductEdit.startAnimation(inFromLeftAnimation());
                        }
                        holder.binding.setIsEdit(false);
                        Timber.wtf(holder.binding.getBasket().getProductInfoFull().getProductName() + "   " + position);

                    }

                    @Override
                    public void onSingleClick() {
                        super.onSingleClick();
                        for (Promo promo : promoList) {
                            if (promo.getProductCodeValue().equalsIgnoreCase(mValues.get(position).getProductInfoFull().getCodeValue())) {
                                mListener.showDialogPromo(promo);
                            }
                        }
                    }
                });
        mBinding.setEditListener(view -> CartRecyclerViewAdapter.this.onClickEdit(view, mValues.get(position)));
        mBinding.setDeleteListener(view -> CartRecyclerViewAdapter.this.onClickDelete(view, mValues.get(position)));
        holder.binding.clProductDetail.setOnClickListener(v -> {
            for (Promo promo : promoList) {
                if (promo.getProductCodeValue().equalsIgnoreCase(mValues.get(position).getProductInfoFull().getCodeValue())) {
                    mListener.showDialogQuantity(mValues.get(position));
                }
            }
        });

        holder.binding.executePendingBindings();
        mBinding.setIsLoading(false);
    }


    private void onClickEdit(View view, BasketItemResponse basketItemResponse) {
        mBinding.clProductEdit.startAnimation(inFromLeftAnimation());
        mBinding.setIsEdit(false);
        mListener.showDialogQuantity(basketItemResponse);


    }

    private void onClickDelete(View view, BasketItemResponse basketItemResponse) {
        mBinding.clProductEdit.startAnimation(inFromLeftAnimation());
        mBinding.setIsEdit(false);
        mValues.remove(basketItemResponse);
        mListener.deleteItem(basketItemResponse);
        //notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private FragmentCartItemBinding binding;

        public ViewHolder(FragmentCartItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private Animation inFromRightAnimation() {
        Animation inFromRight =
                new TranslateAnimation(
                        Animation.RELATIVE_TO_PARENT, +1.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    private Animation inFromLeftAnimation() {
        Animation inFromLeft =
                new TranslateAnimation(
                        Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, +1.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    private Animation inFromCenterToLeftAnimation() {
        Animation inFromRight =
                new TranslateAnimation(
                        Animation.RELATIVE_TO_PARENT, 1.0f,
                        Animation.RELATIVE_TO_PARENT, -1.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    private Animation inFronLeftToCenterAnimation() {
        Animation inFromLeft =
                new TranslateAnimation(
                        Animation.RELATIVE_TO_PARENT, -1.0f,
                        Animation.RELATIVE_TO_PARENT, 1.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    public void updateList(ProductQuantityResponse items) {
        mValues.clear();

        for (BasketItemResponse item : items.getBasket().getBasket()) {
            if (item.getQuantity() != 0) {
                mValues.add(item);
            }
        }
        if (items.getBasket().getPromo() != null) {

            for (Promo promo : items.getBasket().getPromo()) {
                if (promo != null) {
                    promoList.add(promo);
                }
            }
        }
        notifyDataSetChanged();
    }

    public interface onItemAdapterIteractionListener {
        void showDialogQuantity(BasketItemResponse basketItemResponse);

        void deleteItem(BasketItemResponse basketItemResponse);

        void showDialogPromo(Promo promo);
        // TODO: Update argument type and name
    }

}
