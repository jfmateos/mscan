package com.mscanmc.mshop.mscanmc18.scanner;

import com.symbol.emdk.barcode.ScanDataCollection;

/**
 * Created by victor on 6/6/17.
 * Mshop Spain.
 */

public interface ScannerView {
    void onScanValue(ScanDataCollection.LabelType labelType, String value);
    void onCradleUnlocked();
    void onScannerInCradle();
}
