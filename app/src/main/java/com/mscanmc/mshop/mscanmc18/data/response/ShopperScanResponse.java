package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShopperScanResponse {

    @SerializedName("shopper_infos")
    @Expose
    private com.mscanmc.mshop.mscanmc18.data.response.ShopperInfoResponse ShopperInfoResponse;

    @SerializedName("shopperCtx")
    @Expose
    private String shopperCtx;

    @SerializedName("errors")
    @Expose
    private List<Error> errors = null;

    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /** No args constructor for use in serialization */
    public ShopperScanResponse() {}

    public ShopperScanResponse(
            com.mscanmc.mshop.mscanmc18.data.response.ShopperInfoResponse shopperInfoResponse,
            String shopperCtx,
            List<Error> errors,
            double executeTime) {
        ShopperInfoResponse = shopperInfoResponse;
        this.shopperCtx = shopperCtx;
        this.errors = errors;
        this.executeTime = executeTime;
    }

    public com.mscanmc.mshop.mscanmc18.data.response.ShopperInfoResponse getShopperInfoResponse() {
        return ShopperInfoResponse;
    }

    public void setShopperInfoResponse(com.mscanmc.mshop.mscanmc18.data.response.ShopperInfoResponse ShopperInfoResponse) {
        this.ShopperInfoResponse = ShopperInfoResponse;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

    public String getShopperCtx() {
        return shopperCtx;
    }

    public void setShopperCtx(String shopperCtx) {
        this.shopperCtx = shopperCtx;
    }
}
