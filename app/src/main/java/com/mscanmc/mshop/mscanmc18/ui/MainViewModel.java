package com.mscanmc.mshop.mscanmc18.ui;

import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;

import com.mscanmc.mshop.mscanmc18.data.request.FinishSessionRequest;
import com.mscanmc.mshop.mscanmc18.data.request.ShopperScanRequest;
import com.mscanmc.mshop.mscanmc18.data.request.StoreDetailRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoRequest;
import com.mscanmc.mshop.mscanmc18.data.request.UpdateConfigRequest;
import com.mscanmc.mshop.mscanmc18.data.response.DeskListGeneric;
import com.mscanmc.mshop.mscanmc18.data.response.FinishSessionResponse;
import com.mscanmc.mshop.mscanmc18.data.response.GenericMessageResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShopperScanResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingList;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListDetail;
import com.mscanmc.mshop.mscanmc18.data.response.StoreDetailResponse;
import com.mscanmc.mshop.mscanmc18.domain.repository.MScanRepository;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Observable;

public class MainViewModel extends BaseViewModel {
    MScanRepository repository;
    private final MediatorLiveData<StoreDetailResponse> shopperDetailLiveData =
            new MediatorLiveData<>();
    private final MediatorLiveData<DeskListGeneric> deskListGenericMediatorLiveData =
            new MediatorLiveData<>();
    private final MediatorLiveData<ShopperScanResponse> shopperScanResponseMediatorLiveData =
            new MediatorLiveData<>();
    private MediatorLiveData<TripDtoRequest> tripDtoRequestMediatorLiveData =
            new MediatorLiveData<>();
    private MediatorLiveData<ShoppingList> shoppingListMediatorLiveData =
            new MediatorLiveData<>();
    private MediatorLiveData<ShoppingListDetail> entityListResponseMediatorLiveData =
            new MediatorLiveData<>();
    private MediatorLiveData<ShoppingList> userListMediatorLiveData =
            new MediatorLiveData<>();
    private final MediatorLiveData<GenericMessageResponse> updateConfigResponseMediatorLiveData =
            new MediatorLiveData<>();
    private final MediatorLiveData<FinishSessionResponse> finishSessionResponseMediatorLiveData =
            new MediatorLiveData<>();

    @Inject
    public MainViewModel(MScanRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<StoreDetailResponse> getshopperDetailLiveData() {
        return shopperDetailLiveData;
    }

    public Observable<StoreDetailResponse> getStoreDetail(
            StoreDetailRequest updateConfigRequest) {
        return repository.getStoreDetail(updateConfigRequest);
    }


    public Observable<GenericMessageResponse> setUpdateConfig(
            UpdateConfigRequest updateConfigRequest) {
        return repository.getUpdateConfig(updateConfigRequest);
    }


    public void setTripDtoRequestMediatorLiveData(TripDtoRequest tripDtoRequest) {
        this.tripDtoRequestMediatorLiveData.postValue(tripDtoRequest);
    }

    public void unSuscribehopperDetailLiveData() {
        shopperDetailLiveData.postValue(null);
    }

    public void unSuscribeScanResponse() {
        shopperScanResponseMediatorLiveData.postValue(null);
    }

    public void setUserList(ShoppingList userListMediatorLiveData) {
        this.userListMediatorLiveData.postValue(userListMediatorLiveData);
    }

    public Observable<FinishSessionResponse> setEndOfTrip(FinishSessionRequest finishSessionRequest) {
        return repository.setEnOfTrip(finishSessionRequest);
    }

    public Observable<ShopperScanResponse> getShopperScan(
            ShopperScanRequest shopperScanRequest) {

        return repository.getShopperScan(shopperScanRequest);
    }
}
