package com.mscanmc.mshop.mscanmc18.ui.splash;

import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;

import com.mscanmc.mshop.mscanmc18.data.request.ShopperScanRequest;
import com.mscanmc.mshop.mscanmc18.data.request.UpdateConfigRequest;
import com.mscanmc.mshop.mscanmc18.data.response.GenericMessageResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShopperScanResponse;
import com.mscanmc.mshop.mscanmc18.domain.repository.MScanRepository;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class SplashViewModel extends BaseViewModel {

    MScanRepository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MediatorLiveData<GenericMessageResponse> updateConfigResponseMediatorLiveData =
            new MediatorLiveData<>();
    private final MediatorLiveData<ShopperScanResponse> shopperScanResponseMediatorLiveData =
            new MediatorLiveData<>();

    @Inject
    public SplashViewModel(MScanRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<GenericMessageResponse> getUpdateConfig() {
        return updateConfigResponseMediatorLiveData;
    }

    public Observable<GenericMessageResponse> setUpdateConfig(
            UpdateConfigRequest updateConfigRequest) {
        return repository.getUpdateConfig(updateConfigRequest);
    }

    public Observable<ShopperScanResponse> getShopperScan(
            ShopperScanRequest shopperScanRequest) {
        return repository.getShopperScan(shopperScanRequest);
    }

    public void unSuscribeMediator() {
        updateConfigResponseMediatorLiveData.postValue(null);
    }
}
