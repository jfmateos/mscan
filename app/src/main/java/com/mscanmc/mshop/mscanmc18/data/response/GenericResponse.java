package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericResponse {

    private static String MESSAGE_OK = "OK";
    private static String MESSAGE_FAV_EXIST = "Fav entity link already exist";

    @SerializedName("message")
    @Expose
    private String message;

    /**
     * No args constructor for use in serialization
     *
     */
    public GenericResponse() {
    }

    /**
     *
     * @param message
     */
    public GenericResponse(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isOK(String s){
        return s.equalsIgnoreCase(MESSAGE_OK);
    }
    public boolean isFav(String s){
        return s.equalsIgnoreCase(MESSAGE_FAV_EXIST);
    }

}

