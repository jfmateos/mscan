package com.mscanmc.mshop.mscanmc18.ui.cart;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.Tab;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.data.request.GetEntityListRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoItemInitialRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoItemRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoRequest;
import com.mscanmc.mshop.mscanmc18.data.response.Basket;
import com.mscanmc.mshop.mscanmc18.data.response.BasketItemResponse;
import com.mscanmc.mshop.mscanmc18.data.response.Code;
import com.mscanmc.mshop.mscanmc18.data.response.Item;
import com.mscanmc.mshop.mscanmc18.data.response.Product;
import com.mscanmc.mshop.mscanmc18.data.response.ProductInfoFull;
import com.mscanmc.mshop.mscanmc18.data.response.ProductQuantityResponse;
import com.mscanmc.mshop.mscanmc18.data.response.Promo;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListLastShopping;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentProductitemListBinding;
import com.mscanmc.mshop.mscanmc18.di.Injectable;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseFragment;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseNavigation;
import com.mscanmc.mshop.mscanmc18.ui.base.GlideApp;
import com.mscanmc.mshop.mscanmc18.ui.base.ViewModelFactory;
import com.mscanmc.mshop.mscanmc18.utils.Constants;
import com.mscanmc.mshop.mscanmc18.utils.DataUtil;
import com.mscanmc.mshop.mscanmc18.utils.SnackBarUtil;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.mscanmc.mshop.mscanmc18.utils.Constants.DISCOUNT_PROMO;

public class ProductFragment extends BaseFragment
        implements Injectable,
        HasSupportFragmentInjector,
        CartRecyclerViewAdapter.onItemAdapterIteractionListener,
        MySavedListItemRecyclerViewAdapter.UpdateSavedList, CurrentCartRecyclerViewAdapter.UpdateCartList {
    @Inject
    protected BaseNavigation navigation;
    @Inject
    protected ViewModelFactory viewModelFactory;
    @Inject
    protected BaseNavigation baseNavigation;
    private CartViewModel mViewModel;
    @Inject
    protected PreferencesHelper preferencesHelper;
    private AndroidInjector<Fragment> androidInjector;
    private static final String ID_SHOPPING = "id";
    private static final String TAB_CART = "cart";
    private static final String TAB_SHOPPING = "shopping";
    private static final String SHOPPING_LIST = "shopping_list";
    private int idShopping = -1;
    boolean updateList = false;
    private onUpdateCartListener mListener;
    private CartRecyclerViewAdapter.onItemAdapterIteractionListener mItemListener;
    private MySavedListItemRecyclerViewAdapter.UpdateSavedList mUpdateSavedList;
    private MySavedListItemRecyclerViewAdapter mySavedListItemRecyclerViewAdapter;
    private FragmentProductitemListBinding mBinding;
    private ProductQuantityResponse productQuantityResponse = new ProductQuantityResponse();
    private List<BasketItemResponse> shoppingList = new ArrayList<>();
    private List<BasketItemResponse> currentCartList = new ArrayList<>();
    private CartRecyclerViewAdapter cartRecyclerViewAdapter;
    private HashMap<String, Boolean> deleteDuplicates = new HashMap<>();
    private AlertDialog mDialogUnitPromo;
    private AlertDialog mDialogDiscount;
    private CurrentCartRecyclerViewAdapter.UpdateCartList mUpdateCartList;
    private CurrentCartRecyclerViewAdapter currentCartRecyclerViewAdapter;
    private List<Promo> promoList = new ArrayList<>();


    public static ProductFragment newInstance(int idShopping) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putInt(ID_SHOPPING, idShopping);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idShopping = getArguments().getInt(ID_SHOPPING);
        }
        initBasketItems();
        currentCartRecyclerViewAdapter = new CurrentCartRecyclerViewAdapter(currentCartList, mUpdateCartList);
        mySavedListItemRecyclerViewAdapter = new MySavedListItemRecyclerViewAdapter(shoppingList, mUpdateSavedList);
    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mItemListener = this;
        mUpdateSavedList = this;
        mUpdateCartList = this;
        mBinding =
                DataBindingUtil.inflate(
                        inflater, R.layout.fragment_productitem_list, container, false);
        showLoading();
        mBinding.srlCartItems.setColorSchemeResources(
                R.color.primary_dark,
                R.color.green,
                R.color.primary);
        mBinding.srlCartItems.setOnRefreshListener(() -> {
            getInitialShopping();
            mBinding.srlCartItems.setRefreshing(false);
        });
        Objects.requireNonNull(getActivity()).onWindowFocusChanged(false);
        mViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(CartViewModel.class);
        mDialogUnitPromo =
                new AlertDialog.Builder(mBinding.getRoot().getContext()).create();
        mDialogDiscount =
                new AlertDialog.Builder(mBinding.getRoot().getContext()).create();
        mBinding.tabLayout.setTabMode(TabLayout.MODE_FIXED);
        mBinding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        LinearLayoutManager llm = new LinearLayoutManager(mBinding.getRoot().getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.rvCartView.setLayoutManager(llm);
        mBinding.rvCartView.getItemAnimator().setChangeDuration(0);
        cartRecyclerViewAdapter = new CartRecyclerViewAdapter(mBinding.getRoot().getContext(), productQuantityResponse, mItemListener);
        Objects.requireNonNull(mBinding.tabLayout.getTabAt(1)).setTag(TAB_SHOPPING);
        Objects.requireNonNull(mBinding.tabLayout.getTabAt(0)).setTag(TAB_CART);
        mBinding.setIsShoppingList(true);
        mBinding.setShowTab(false);
        getInitialShopping();
        Objects.requireNonNull(mBinding.tabLayout.getTabAt(0)).select();
        if (idShopping != -1) {
            mBinding.setIsShoppingList(false);
            mBinding.setIsCartEmpty(false);
            mBinding.setIsShoppingList(false);
            mBinding.tabLayout.getTabAt(1).select();
            getSavedCart();
        }
        mBinding.tabLayout.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(Tab tab) {
                        mBinding.setShowTab(true);
                        if (tab.getPosition() == 0) {
                            mBinding.setIsShoppingList(true);
                            getInitialShopping();
                        } else if (tab.getPosition() == 1) {
                            showLoading();
                            mBinding.setIsShoppingList(false);
                            mBinding.setIsCartEmpty(false);
                            if (!updateList) {
                                getSavedCart();
                            }

                            hideLoading();
                        }
                    }

                    @Override
                    public void onTabUnselected(Tab tab) {
                    }

                    @Override
                    public void onTabReselected(Tab tab) {

                    }
                });

        mViewModel.getTriDto().observe(this, new Observer<TripDtoItemRequest>() {
            @Override
            public void onChanged(@Nullable TripDtoItemRequest tripDtoItemRequest) {
                if (tripDtoItemRequest != null) {
                    ProductFragment.this.setProductQuantity(tripDtoItemRequest);
                }
            }
        });
        hideLoading();
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        if (context instanceof onUpdateCartListener) {
            mListener = (onUpdateCartListener) context;
        } else if (context instanceof MySavedListItemRecyclerViewAdapter.UpdateSavedList) {
            mUpdateSavedList = (MySavedListItemRecyclerViewAdapter.UpdateSavedList) context;
        } else if (context instanceof CurrentCartRecyclerViewAdapter.UpdateCartList) {
            mUpdateCartList = (CurrentCartRecyclerViewAdapter.UpdateCartList) context;
        } else {
            throw new RuntimeException(
                    context.toString() + " must implement onItemAdapterIteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mViewModel.unsuscribeAll();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void initBasketItems() {
        productQuantityResponse = null;
        productQuantityResponse = new ProductQuantityResponse();
        productQuantityResponse.setBasket(new Basket());
        productQuantityResponse.getBasket().setBasket(new ArrayList<>());

    }

    private void getInitialShopping() {
        //showLoading();
        TripDtoItemInitialRequest tripDtoItemRequest = new TripDtoItemInitialRequest(preferencesHelper.getShopperCtxt(getActivity()));
        mViewModel.getInitialCart(tripDtoItemRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((productQuantityResponse) -> {
                            getInitCartList(productQuantityResponse);
                            hideLoading();
                        },
                        e -> {
                            e.printStackTrace();
                            hideLoading();

                        });

    }

    private void getSavedCart() {
        //showLoading();
        mBinding.setIsCartEmpty(false);
        TripDtoItemInitialRequest tripDtoItemRequest = new TripDtoItemInitialRequest(preferencesHelper.getShopperCtxt(getActivity()));
        mViewModel.getInitialCart(tripDtoItemRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((productQuantityResponse) -> {
                            for (BasketItemResponse basketItemResponse : productQuantityResponse.getBasket().getBasket()) {
                                currentCartList.add(basketItemResponse);
                            }
                            hideLoading();
                            currentCartRecyclerViewAdapter.updateList(productQuantityResponse.getBasket().getBasket());
                            mBinding.rvShoppingAddedView.setAdapter(currentCartRecyclerViewAdapter);
                            showSavedList();
                        },
                        e -> {
                            e.printStackTrace();
                            hideLoading();

                        });

    }

    private void getInitCartList(ProductQuantityResponse productQuantityResponse) {
        int cartQUantity = ProductFragment.this.getCartQuantity(productQuantityResponse);
        mBinding.setCartQuantity(String.valueOf(cartQUantity));
        Objects.requireNonNull(mBinding.tabLayout.getTabAt(0)).setText(String.format(ProductFragment.this.getResources().getString(R.string.your_cart), String.valueOf(cartQUantity)));
        mBinding.tvYourCartItems.setText(String.format(ProductFragment.this.getResources().getString(R.string.your_cart), String.valueOf(cartQUantity)));
        mListener.updateCartItems(cartQUantity);
        if (!productQuantityResponse.getBasket().getBasket().isEmpty()) {
            mBinding.setTotalPrice(String.valueOf(DataUtil.formatFinalPrice(productQuantityResponse) / 100).replace(".", ",").concat(" €"));
            mBinding.setIsCartEmpty(false);
            initBasketItems();
            this.productQuantityResponse = productQuantityResponse;
            cartRecyclerViewAdapter.updateList(productQuantityResponse);
            mBinding.rvCartView.setAdapter(cartRecyclerViewAdapter);

        } else {
            mBinding.setIsCartEmpty(true);
        }
    }

    private void setProductQuantity(TripDtoItemRequest tripDtoItemRequest) {
        showLoading();
        List<TripDtoItemRequest> arrayList = new ArrayList<>();
        if (tripDtoItemRequest != null) {
            tripDtoItemRequest.setCodeType("EAN13");
            arrayList.add(tripDtoItemRequest);
        }
        TripDtoRequest productQuantityRequest =
                new TripDtoRequest(arrayList, preferencesHelper.getShopperCtxt(getContext()));
        mViewModel.setProductQuantity(productQuantityRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((productQuantityResponse) -> {
                            hideLoading();
                            shoppingList.addAll(productQuantityResponse.getBasket().getBasket());
                            Set<BasketItemResponse> hashSet = new HashSet<>(shoppingList);
                            shoppingList.clear();
                            shoppingList.addAll(hashSet);
                            updateCartList(productQuantityResponse, tripDtoItemRequest);
                            getSavedCart();
                        },
                        e -> e.printStackTrace());
    }


    private void updateCartList(ProductQuantityResponse productQuantityResponse, TripDtoItemRequest tripDtoItemRequest) {
        hideLoading();
        if (productQuantityResponse != null) {
            int cartQUantity = 0;
            mDialogUnitPromo.dismiss();
            initBasketItems();
            mDialogDiscount.dismiss();
            cartQUantity = getCartQuantity(productQuantityResponse);
            mBinding.setCartQuantity(String.valueOf(cartQUantity));
            mBinding.tabLayout.getTabAt(0).setText(String.format(getResources().getString(R.string.your_cart), String.valueOf(cartQUantity)));
            mBinding.tvYourCartItems.setText(String.format(getResources().getString(R.string.your_cart), String.valueOf(cartQUantity)));
            mListener.updateCartItems(cartQUantity);
            if (!productQuantityResponse.getBasket().getErrorProducts().isEmpty()) {
                mBinding.setIsCartEmpty(false);
                Code code = new Code("codeType", productQuantityResponse.getBasket().getErrorProducts().get(0));
                Product product = new Product(code, Constants.UNKNOWN_PRODUCT);
                productQuantityResponse.getBasket().getBasket().add(0, new BasketItemResponse("-1", product, 1));
            } else if (productQuantityResponse.getBasket().getBasket().size() != 0) {
                mBinding.setIsCartEmpty(false);
                for (Promo promo : productQuantityResponse.getBasket().getPromo()) {
                    if (promo != null) {
                        promoList.add(promo);
                        if (promo.getProductCodeValue().equalsIgnoreCase(tripDtoItemRequest.getCodeValue())) {
                            checkPromo(productQuantityResponse);
                        }
                    }
                }
            }
            for (BasketItemResponse basketItemResponse : productQuantityResponse.getBasket().getBasket()) {
                if (basketItemResponse.getQuantity() != 0) {
                    this.productQuantityResponse.getBasket().getBasket().add(basketItemResponse);
                    currentCartList.add(basketItemResponse);
                }
            }

            List<BasketItemResponse> tmpList = new ArrayList<>();
            for (BasketItemResponse basketItemResponse : productQuantityResponse.getBasket().getBasket()) {
                for (BasketItemResponse itemResponse : shoppingList) {
                    if (itemResponse.getProductInfoFull().getCodeValue() != null) {
                        if (!basketItemResponse.getProductInfoFull().getCodeValue().equalsIgnoreCase(itemResponse.getProductInfoFull().getCodeValue())) {
                            tmpList.add(itemResponse);
                        }else{
                            tmpList.add(itemResponse);
                        }
                    }
                }
            }
            mBinding.setTotalPrice(String.valueOf(DataUtil.formatFinalPrice(productQuantityResponse) / 100).replace(".", ",").concat(" €"));
            //currentCartRecyclerViewAdapter.updateList(productQuantityResponse.getBasket().getBasket());
            ProductQuantityResponse productQuantityResponse1 = new ProductQuantityResponse();
            Set<BasketItemResponse> hashSet = new HashSet<>(tmpList);
            tmpList.clear();
            tmpList.addAll(hashSet);
            cartRecyclerViewAdapter.updateList(productQuantityResponse);
            mBinding.rvCartView.setAdapter(cartRecyclerViewAdapter);
            //mBinding.rvShoppingAddedView.setAdapter(cartRecyclerViewAdapter);
            //mySavedListItemRecyclerViewAdapter.updateList(tmpList);
            //mBinding.rvShoppingListView.setAdapter(mySavedListItemRecyclerViewAdapter);

        }
    }

    private void updateCartAfterDelete(ProductQuantityResponse productQuantityResponse, TripDtoItemRequest tripDtoItemRequest) {
        if (productQuantityResponse != null) {
            int cartQUantity = 0;
            mDialogUnitPromo.dismiss();
            initBasketItems();
            mDialogDiscount.dismiss();
            cartQUantity = getCartQuantity(productQuantityResponse);
            mBinding.setCartQuantity(String.valueOf(cartQUantity));
            mBinding.tabLayout.getTabAt(0).setText(String.format(getResources().getString(R.string.your_cart), String.valueOf(cartQUantity)));
            mBinding.tvYourCartItems.setText(String.format(getResources().getString(R.string.your_cart), String.valueOf(cartQUantity)));
            mListener.updateCartItems(cartQUantity);
            if (!productQuantityResponse.getBasket().getErrorProducts().isEmpty()) {
                mBinding.setIsCartEmpty(false);
                Code code = new Code("codeType", productQuantityResponse.getBasket().getErrorProducts().get(0));
                Product product = new Product(code, Constants.UNKNOWN_PRODUCT);
                productQuantityResponse.getBasket().getBasket().add(0, new BasketItemResponse("-1", product, 1));
            }
            for (BasketItemResponse basketItemResponse : productQuantityResponse.getBasket().getBasket()) {
                if (basketItemResponse.getQuantity() != 0) {
                    this.productQuantityResponse.getBasket().getBasket().add(basketItemResponse);
                }
            }

            mBinding.setTotalPrice(String.valueOf(DataUtil.formatFinalPrice(productQuantityResponse) / 100).replace(".", ",").concat(" €"));
            cartRecyclerViewAdapter.updateList(productQuantityResponse);
            currentCartRecyclerViewAdapter.updateList(productQuantityResponse.getBasket().getBasket());
            mBinding.rvCartView.setAdapter(cartRecyclerViewAdapter);
            mBinding.rvShoppingAddedView.setAdapter(currentCartRecyclerViewAdapter);
        }
    }

    private void deleteItem(TripDtoItemRequest tripDtoItemRequest) {
        showLoading();
        List<TripDtoItemRequest> arrayList = new ArrayList<>();
        if (tripDtoItemRequest != null) {
            tripDtoItemRequest.setCodeType("EAN13");
            arrayList.add(tripDtoItemRequest);
        }
        TripDtoRequest productQuantityRequest =
                new TripDtoRequest(arrayList, preferencesHelper.getShopperCtxt(getContext()));
        mViewModel.removeItem(productQuantityRequest).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((productQuantityResponse) -> {
                            hideLoading();
                            updateCartAfterDelete(productQuantityResponse, tripDtoItemRequest);
                        },
                        e -> {
                            e.printStackTrace();
                            hideLoading();
                        });
    }


    private int getCartQuantity(ProductQuantityResponse basketResponse) {
        int items = 0;
        for (BasketItemResponse basketItemResponse : basketResponse.getBasket().getBasket()) {
            items = items + basketItemResponse.getQuantity();
        }
        return items;
    }


    private void checkPromo(ProductQuantityResponse basketResponse) {
        for (Promo promo : basketResponse.getBasket().getPromo()) {
            if (promo != null) {
                if (deleteDuplicates.size() == 0) {
                    deleteDuplicates.put(promo.getProductCodeValue(), true);
                    if (promo.getUnitMode().equalsIgnoreCase(DISCOUNT_PROMO)) {
                        showDialogDiscountPromo(promo);
                        break;
                    } else if (promo.getUnitMode().equalsIgnoreCase(Constants.DISCOUNT_PRODUCT_PROMO)) {
                        showDialogUnitPromo(promo);
                        break;
                    }
                } else {
                    if (!deleteDuplicates.containsKey(promo.getProductCodeValue())) {
                        deleteDuplicates.put(promo.getProductCodeValue(), true);
                        if (promo.getUnitMode().equalsIgnoreCase(DISCOUNT_PROMO)) {
                            showDialogDiscountPromo(promo);
                            break;
                        } else if (promo.getUnitMode().equalsIgnoreCase(Constants.DISCOUNT_PRODUCT_PROMO)) {
                            showDialogUnitPromo(promo);
                            break;
                        }
                    }

                }
            }
        }
    }

    private void showDialogDiscountPromo(Promo promo) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.dialog_promo, null);
        AppCompatImageView ivClose = (AppCompatImageView) promptView.findViewById(R.id.iv_discount_close);
        AppCompatImageView ivPhoto = (AppCompatImageView) promptView.findViewById(R.id.iv_discount_photo);
        AppCompatTextView tvPreviousPrice = (AppCompatTextView) promptView.findViewById(R.id.tv_reduced_price);
        AppCompatTextView tvQuantityDiscount = (AppCompatTextView) promptView.findViewById(R.id.tv_quantity_discount);
        AppCompatTextView tvFinalPrice = (AppCompatTextView) promptView.findViewById(R.id.tv_promo_final_price);
        AppCompatTextView tvDiscountDescription = (AppCompatTextView) promptView.findViewById(R.id.tv_discount_description);
        AppCompatTextView tvValidFrom = (AppCompatTextView) promptView.findViewById(R.id.tv_valid_from);
        ivClose.setOnClickListener(v -> mDialogDiscount.dismiss());
        GlideApp.with(this).load(promo.getUrlImagenPromo()).error(R.drawable.placeholder).into(ivPhoto);
        double previosPrice = promo.getPreviousPricePromo() / 10000.00;
        tvPreviousPrice.setText(String.valueOf(promo.getPreviousPricePromo() / 10000.00).replace(".", ",") + " €");
        tvPreviousPrice.setBackground(getResources().getDrawable(R.drawable.strike_line));
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.ITALY);
        try {
            Date dateFrom = format.parse(promo.getPromoFromDate());
            DateFormat newformat = new SimpleDateFormat("dd.MM");
            String fromDate = newformat.format(dateFrom);
            Date dateTo = format.parse(promo.getPromoToDate());
            String toDate = newformat.format(dateTo);
            tvValidFrom.setText(getString(R.string.valid_from) + " " + fromDate + " to " + toDate + "");
        } catch (ParseException e) {
            e.printStackTrace();
            tvValidFrom.setText("");
        }
        tvQuantityDiscount.setText(String.valueOf((int) promo.getUnitValuePromo() / 100).concat(" %"));
        double finalPrice = previosPrice - (previosPrice * promo.getUnitValuePromo() / 10000.00);
        tvFinalPrice.setText(String.valueOf(finalPrice).replace(".", ",") + " €");
        tvDiscountDescription.setText(promo.getDescriptionTextPromo());
        mDialogDiscount.setView(promptView);
        mDialogDiscount.show();
        hideLoading();
    }

    private void showDialogUnitPromo(Promo promo) {
        //showLoading();
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.dialog_unit_promo, null);
        AppCompatImageView ivClose = (AppCompatImageView) promptView.findViewById(R.id.iv_unit_discount_close);
        AppCompatImageView ivPhoto = (AppCompatImageView) promptView.findViewById(R.id.iv_unit_discount_photo);
        AppCompatTextView tvFinalPrice = (AppCompatTextView) promptView.findViewById(R.id.tv_unit_final_price);
        AppCompatTextView tvDiscountDescription = (AppCompatTextView) promptView.findViewById(R.id.tv_unit_discount_description);
        AppCompatTextView tvValidFrom = (AppCompatTextView) promptView.findViewById(R.id.tv_unit_valid_from);
        ivClose.setOnClickListener(v -> mDialogUnitPromo.dismiss());
        GlideApp.with(this).load(promo.getUrlImagenPromo()).error(R.drawable.placeholder).into(ivPhoto);
        tvFinalPrice.setText(String.valueOf(promo.getUnitPriceStore() / 100.0).concat(" €").replace(".", ","));
        tvDiscountDescription.setText(promo.getDescriptionTextPromo());
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        try {
            Date dateFrom = format.parse(promo.getPromoFromDate());
            DateFormat newformat = new SimpleDateFormat("MMM d");
            String fromDate = newformat.format(dateFrom);
            Date dateTo = format.parse(promo.getPromoToDate());
            String toDate = newformat.format(dateTo);
            tvValidFrom.setText(getString(R.string.valid_from) + " " + fromDate + " to " + toDate + "");
        } catch (ParseException e) {
            e.printStackTrace();
            tvValidFrom.setText("");
        }
        mDialogUnitPromo.setView(promptView);
        mDialogUnitPromo.show();
        hideLoading();
    }

    private void showSavedList() {
        //showLoading();
        //mBinding.setIsCartEmpty(false);
        //mBinding.setIsShoppingList(false);
        //mBinding.tabLayout.getTabAt(1).select();
        final List<BasketItemResponse> list = new ArrayList<>();
        mViewModel
                .getShoppingListDetail(
                        new GetEntityListRequest(
                                preferencesHelper.getShopperCtxt(getContext()), DataUtil.recoverStoreIdValue(), idShopping))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ShoppingListLastShopping>() {
                    @Override
                    public void accept(ShoppingListLastShopping shoppingListLastShopping) throws Exception {
                        ProductFragment.this.getSavedList(shoppingListLastShopping);
                        mBinding.setShowTab(true);
                        hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable e) throws Exception {
                        e.printStackTrace();
                    }
                });
    }

    public void getSavedList(ShoppingListLastShopping shoppingListDetail) {
        //showLoading();
        shoppingList.clear();
        List<BasketItemResponse> list = new ArrayList<>();
        list.clear();

        for (Item item : shoppingListDetail.getShoppingList().getItems()) {
            if (item.getQuantity() > 0) {
                BasketItemResponse basketItemResponse = new BasketItemResponse();
                basketItemResponse.setProductInfoFull(new ProductInfoFull());
                if (item.getProductInfoFull() != null) {
                    basketItemResponse.getProductInfoFull().setProductName(item.getProductInfoFull().getProductName() != null ? item.getProductInfoFull().getProductName() : "");
                    basketItemResponse.getProductInfoFull().setTradename(item.getProductInfoFull().getTradename());
                    basketItemResponse.getProductInfoFull().setAisleNb(item.getProductInfoFull().getAisleNb());
                    basketItemResponse.getProductInfoFull().setUnitPrice(item.getProductInfoFull().getUnitPrice());
                    basketItemResponse.getProductInfoFull().setImageSmall(item.getProductInfoFull().getImageSmall());
                    basketItemResponse.getProductInfoFull().setCodeType(item.getProductInfoFull().getCodeType());
                    basketItemResponse.getProductInfoFull().setCodeValue(item.getProductInfoFull().getCodeValue());
                    basketItemResponse.setQuantity(item.getQuantity());
                    list.add(basketItemResponse);
                    shoppingList.add(basketItemResponse);
                } else {
                    basketItemResponse.getProductInfoFull().setProductName(item.getFreeform());
                    list.add(basketItemResponse);
                    shoppingList.add(basketItemResponse);
                }
            }
        }

        HashSet<BasketItemResponse> hashSet = new HashSet<>();
        hashSet.addAll(list);
        list.clear();
        list.addAll(hashSet);
        hashSet.clear();
        hashSet.addAll(shoppingList);
        shoppingList.clear();
        shoppingList.addAll(hashSet);
        for (BasketItemResponse basketItemResponse : currentCartList) {
            for (BasketItemResponse itemResponse : shoppingList) {
                if (list.size() > 0) {
                    if (basketItemResponse.getProductInfoFull().getCodeValue().equalsIgnoreCase(itemResponse.getProductInfoFull().getCodeValue())) {
                        list.remove(itemResponse);
                    }
                }
            }

        }
        Objects.requireNonNull(mBinding.tabLayout.getTabAt(1)).select();
        Objects.requireNonNull(mBinding.tabLayout.getTabAt(1)).setText(preferencesHelper.getSavedList(getActivity()));
        updateList = false;
        currentCartRecyclerViewAdapter.updateList(productQuantityResponse.getBasket().getBasket());
        mBinding.rvShoppingAddedView.setAdapter(currentCartRecyclerViewAdapter);
        mySavedListItemRecyclerViewAdapter.updateList(list);
        mBinding.rvShoppingListView.setAdapter(mySavedListItemRecyclerViewAdapter);
        hideLoading();

    }

    private void hideLoading() {
        mBinding.setIsLoading(false);
    }

    private void showLoading() {
        mBinding.setIsLoading(true);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return androidInjector;
    }

    @Override
    public void showDialogQuantity(BasketItemResponse basketItemResponse) {
        //showLoading();
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.dialog_edit_product, null);

        final AlertDialog mAlertDialog =
                new AlertDialog.Builder(mBinding.getRoot().getContext()).create();
        AppCompatTextView tvQuantity =
                (AppCompatTextView) promptView.findViewById(R.id.tv_quantity_modify);
        AppCompatButton bAccept = (AppCompatButton) promptView.findViewById(R.id.b_accept);
        AppCompatImageView ivAddItem =
                (AppCompatImageView) promptView.findViewById(R.id.iv_add_quantity);
        AppCompatImageView ivRestItem =
                (AppCompatImageView) promptView.findViewById(R.id.iv_rest_quantity);
        AppCompatButton bCancel = (AppCompatButton) promptView.findViewById(R.id.b_cancel);
        tvQuantity.setText(String.valueOf(basketItemResponse.getQuantity()));
        ivAddItem.setOnClickListener(
                v -> {
                    String add = tvQuantity.getText().toString();
                    int result = Integer.parseInt(add) + 1;
                    tvQuantity.setText(String.valueOf(result));
                    // btnAdd1 has been clicked

                });
        ivRestItem.setOnClickListener(
                v -> {
                    if (Integer.parseInt(tvQuantity.getText().toString()) > 1) {
                        String add = tvQuantity.getText().toString();
                        int result = Integer.parseInt(add) - 1;
                        tvQuantity.setText(String.valueOf(result));
                    }
                });
        bAccept.setOnClickListener(
                v -> {
                    mBinding.rvCartView.getAdapter().notifyDataSetChanged();
                    String add = tvQuantity.getText().toString();
                    int tmp = Integer.parseInt(add);
                    if (tmp == 0) {
                        SnackBarUtil snackBarUtil = new SnackBarUtil();
                        snackBarUtil.styleSnackbar(
                                mBinding.getRoot(), getString(R.string.error_adding_zero), R.color.black);
                    } else {
                        // TODO harcodeo quitar esto
                        TripDtoItemRequest tripDtoItemRequest =
                                new TripDtoItemRequest("EAN13", basketItemResponse.getProductInfoFull().getCodeValue(), tmp, basketItemResponse.getId());
                        setProductQuantity(tripDtoItemRequest);
                        mAlertDialog.dismiss();
                    }
                });
        bCancel.setOnClickListener(
                v -> {
                    mAlertDialog.dismiss();
                });
        mAlertDialog.setView(promptView);
        mAlertDialog.show();
        hideLoading();
    }

    @Override
    public void deleteItem(BasketItemResponse basketItemResponse) {
        TripDtoItemRequest tripDtoItemRequest =
                new TripDtoItemRequest("EAN13", basketItemResponse.getProductInfoFull().getCodeValue(), 0, basketItemResponse.getId());
        deleteItem(tripDtoItemRequest);
        mBinding.tabLayout.getTabAt(0).select();
    }

    @Override
    public void showDialogPromo(Promo promo) {
        if (promo.getUnitMode().equalsIgnoreCase(DISCOUNT_PROMO)) {
            showDialogDiscountPromo(promo);
        } else if (promo.getUnitMode().equalsIgnoreCase(Constants.DISCOUNT_PRODUCT_PROMO)) {
            showDialogUnitPromo(promo);
        }
    }


    @Override
    public void updateSavedList(BasketItemResponse basketItemResponse) {
        TripDtoItemRequest tripDtoItemRequest = new TripDtoItemRequest(basketItemResponse.getProductInfoFull().getCodeType(), basketItemResponse.getProductInfoFull().getCodeValue(), 1, "-1");
        updateSaveList(tripDtoItemRequest);

    }

    private void updateSaveList(TripDtoItemRequest tripDtoItemRequest) {
        List<BasketItemResponse> list = new ArrayList<>();
        List<TripDtoItemRequest> arrayList = new ArrayList<>();
        if (tripDtoItemRequest != null) {
            tripDtoItemRequest.setCodeType("EAN13");
            arrayList.add(tripDtoItemRequest);

            HashSet<BasketItemResponse> hashSet = new HashSet<>();
            hashSet.addAll(shoppingList);
            shoppingList.clear();
            shoppingList.addAll(hashSet);
            for (int i = 0; i < shoppingList.size(); i++) {
                if (shoppingList.get(i).getProductInfoFull().getCodeValue() != tripDtoItemRequest.getCodeValue()) {
                    list.add(shoppingList.get(i));
                }
            }
        }
        TripDtoRequest productQuantityRequest =
                new TripDtoRequest(arrayList, preferencesHelper.getShopperCtxt(getContext()));

        mViewModel.setProductQuantity(productQuantityRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((productQuantityResponse) -> {
                            Timber.wtf("subscribe");
                            updateSavedList(productQuantityResponse);
                            hideLoading();

                        },
                        e -> {
                            e.printStackTrace();
                            hideLoading();

                        });
    }

    private void updateSavedList(ProductQuantityResponse productQuantityResponse) {
        mDialogDiscount.dismiss();
        mDialogUnitPromo.dismiss();
        initBasketItems();
        int cartQUantity = 0;
        cartQUantity = getCartQuantity(productQuantityResponse);
        mBinding.setCartQuantity(String.valueOf(cartQUantity));
        mBinding.tabLayout.getTabAt(0).setText(String.format(getResources().getString(R.string.your_cart), String.valueOf(cartQUantity)));
        mBinding.tvYourCartItems.setText(String.format(getResources().getString(R.string.your_cart), String.valueOf(cartQUantity)));
        mBinding.setTotalPrice(String.valueOf(DataUtil.formatFinalPrice(productQuantityResponse)).replace(".", ",").concat(" €"));
        showSavedList();
    }

    @Override
    public void updateCartList(BasketItemResponse basketItemResponse) {
        Timber.wtf("updateCartList " + basketItemResponse.getProductInfoFull().getCodeValue());

    }

    public interface onUpdateCartListener {
        // TODO: Update argument type and name
        public void updateCartItems(int quantity);
    }
}
