package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateConfigBasicResponse {

    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    /**
     * No args constructor for use in serialization
     */
    public UpdateConfigBasicResponse() {
    }

    /**
     * @param storeId
     * @param deviceId
     */
    public UpdateConfigBasicResponse(String storeId, String deviceId) {
        super();
        this.storeId = storeId;
        this.deviceId = deviceId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
