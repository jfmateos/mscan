package com.mscanmc.mshop.mscanmc18.ui.cart;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.stetho.common.ArrayListAccumulator;
import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.response.BasketItemResponse;
import com.mscanmc.mshop.mscanmc18.data.response.Item;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingList;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListDetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SavedListFragment extends Fragment implements MySavedListItemRecyclerViewAdapter.UpdateSavedList {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private MySavedListItemRecyclerViewAdapter.UpdateSavedList mListener;
    ShoppingList shoppingList;

    public SavedListFragment() {
    }

    public static SavedListFragment newInstance(ShoppingList shoppingList) {
        SavedListFragment fragment = new SavedListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_COLUMN_COUNT, (Serializable) shoppingList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            shoppingList = new ShoppingList();
            shoppingList = (ShoppingList) getArguments().getSerializable(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user__list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            List<BasketItemResponse> list = new ArrayList<>();
            for (ShoppingListDetail shoppingListDetail : shoppingList.getShoppingList()) {
                for (Item item : shoppingListDetail.getItems()) {
                    BasketItemResponse basketItemResponse = new BasketItemResponse();
                    basketItemResponse.setProductInfoFull(item.getProductInfoFull());
                    list.add(basketItemResponse);
                }
            }
            recyclerView.setAdapter(new MySavedListItemRecyclerViewAdapter(list, mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MySavedListItemRecyclerViewAdapter.UpdateSavedList) {
            mListener = (MySavedListItemRecyclerViewAdapter.UpdateSavedList) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onListItemClick");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void updateSavedList(BasketItemResponse basketItemResponse) {

    }
}
