package com.mscanmc.mshop.mscanmc18.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import me.pushy.sdk.util.PushyServiceManager;
import timber.log.Timber;

public class PushyUpdateReceiver extends BroadcastReceiver {
    public PushyUpdateReceiver() {
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getData() != null && intent.getData().getSchemeSpecificPart() != null && intent.getData().getSchemeSpecificPart().equals(context.getPackageName())) {
            Timber.wtf("App updated");
            PushyServiceManager.start(context);
        }

    }
}
