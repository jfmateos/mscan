package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Basket {
    @SerializedName("errorProducts")
    @Expose
    private List<String> errorProducts = null;
    @SerializedName("promo")
    @Expose
    private List<Promo> promo = null;
    @SerializedName("basket")
    @Expose
    private List<BasketItemResponse> basket = null;

    /**
     * No args constructor for use in serialization
     */
    public Basket() {
    }

    /**
     * @param promo
     * @param basket
     * @param errorProducts
     */
    public Basket(List<String> errorProducts, List<Promo> promo, List<BasketItemResponse> basket) {
        super();
        this.errorProducts = errorProducts;
        this.promo = promo;
        this.basket = basket;
    }

    public List<String> getErrorProducts() {
        return errorProducts;
    }

    public void setErrorProducts(List<String> errorProducts) {
        this.errorProducts = errorProducts;
    }

    public List<Promo> getPromo() {
        return promo;
    }

    public void setPromo(List<Promo> promo) {
        this.promo = promo;
    }

    public List<BasketItemResponse> getBasket() {
        return basket;
    }

    public void setBasket(List<BasketItemResponse> basket) {
        this.basket = basket;
    }


}
