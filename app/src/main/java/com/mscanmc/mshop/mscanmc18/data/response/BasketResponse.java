package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class BasketResponse {

    @SerializedName("basket")
    @Expose
    private Basket basket;
    @SerializedName("errors")
    @Expose
    private List<String> errors = null;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     */
    public BasketResponse() {
    }

    /**
     * @param errors
     * @param basket
     * @param executeTime
     */
    public BasketResponse(Basket basket, List<String> errors, double executeTime) {
        super();
        this.basket = basket;
        this.errors = errors;
        this.executeTime = executeTime;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

}