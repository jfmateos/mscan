package com.mscanmc.mshop.mscanmc18.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mscanmc.mshop.mscanmc18.utils.Constants;

import timber.log.Timber;

public class NewPushBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String pushContent = intent.getStringExtra(Constants.BROADCAST_PUSH_MESSAGE);
        String encodedId = intent.getStringExtra(Constants.BROADCAST_PUSH_ENCODED_ID);

        if (pushContent != null && pushContent.contentEquals(Constants.RELEASE_SCANNER)) {
            //scannerPresenter.unlockDeviceCradle();
            Timber.wtf("NewPushBroadcastReceiver");
        }

        if (encodedId != null) {
            //dataManager.setEncodedId(encodedId);
            Timber.wtf("release encode != null");
        }
    }


}
