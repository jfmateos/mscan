package com.mscanmc.mshop.mscanmc18.di.module;

import android.content.Context;
import android.content.SharedPreferences;

import com.mscanmc.mshop.mscanmc18.di.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class SharedPreferencesModule {

    private Context context;

    public SharedPreferencesModule(Context context) {
        this.context = context;
    }

    @Provides
    @ApplicationScope
    SharedPreferences provideSharedPreferences() {
        return context.getSharedPreferences("PrefName", Context.MODE_PRIVATE);
    }
}
