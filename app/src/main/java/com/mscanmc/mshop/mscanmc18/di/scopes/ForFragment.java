package com.mscanmc.mshop.mscanmc18.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface ForFragment {
}
