package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntityDeskList {

    @SerializedName("idDesk")
    @Expose
    private int idDesk;

    @SerializedName("deskName")
    @Expose
    private String deskName;

    @SerializedName("currentTurn")
    @Expose
    private String currentTurn;

    @SerializedName("distanceTurns")
    @Expose
    private int distanceTurns;

    @SerializedName("predictedNextTurn")
    @Expose
    private String predictedNextTurn;

    @SerializedName("thumbImage")
    @Expose
    private String thumbImage;

    @SerializedName("myTurn")
    @Expose
    private String myTurn;

    @SerializedName("flicButtonName")
    @Expose
    private String flicButtonName;

    @SerializedName("showOnTV")
    @Expose
    private String showOnTV;

    /** No args constructor for use in serialization */
    public EntityDeskList() {}

    /**
     * @param thumbImage
     * @param predictedNextTurn
     * @param showOnTV
     * @param flicButtonName
     * @param distanceTurns
     * @param currentTurn
     * @param idDesk
     * @param myTurn
     * @param deskName
     */
    public EntityDeskList(
            int idDesk,
            String deskName,
            String currentTurn,
            int distanceTurns,
            String predictedNextTurn,
            String thumbImage,
            String myTurn,
            String flicButtonName,
            String showOnTV) {
        super();
        this.idDesk = idDesk;
        this.deskName = deskName;
        this.currentTurn = currentTurn;
        this.distanceTurns = distanceTurns;
        this.predictedNextTurn = predictedNextTurn;
        this.thumbImage = thumbImage;
        this.myTurn = myTurn;
        this.flicButtonName = flicButtonName;
        this.showOnTV = showOnTV;
    }

    public int getIdDesk() {
        return idDesk;
    }

    public void setIdDesk(int idDesk) {
        this.idDesk = idDesk;
    }

    public String getDeskName() {
        return deskName;
    }

    public void setDeskName(String deskName) {
        this.deskName = deskName;
    }

    public String getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(String currentTurn) {
        this.currentTurn = currentTurn;
    }

    public int getDistanceTurns() {
        return distanceTurns;
    }

    public void setDistanceTurns(int distanceTurns) {
        this.distanceTurns = distanceTurns;
    }

    public String getPredictedNextTurn() {
        return predictedNextTurn;
    }

    public void setPredictedNextTurn(String predictedNextTurn) {
        this.predictedNextTurn = predictedNextTurn;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getMyTurn() {
        return myTurn;
    }

    public void setMyTurn(String myTurn) {
        this.myTurn = myTurn;
    }

    public String getFlicButtonName() {
        return flicButtonName;
    }

    public void setFlicButtonName(String flicButtonName) {
        this.flicButtonName = flicButtonName;
    }

    public String getShowOnTV() {
        return showOnTV;
    }

    public void setShowOnTV(String showOnTV) {
        this.showOnTV = showOnTV;
    }
}

