package com.mscanmc.mshop.mscanmc18.ui.notifications;

import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;

import com.mscanmc.mshop.mscanmc18.data.request.NotificationRequest;
import com.mscanmc.mshop.mscanmc18.data.response.NotificationResponse;
import com.mscanmc.mshop.mscanmc18.domain.repository.MScanRepository;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Observable;

public class NotificationViewModel extends BaseViewModel {
    MScanRepository repository;
    private final MediatorLiveData<NotificationResponse> shopperDetailLiveData =
            new MediatorLiveData<>();

    @Inject
    public NotificationViewModel(MScanRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<NotificationResponse> getshopperDetailLiveData() {
        return shopperDetailLiveData;
    }

    public Observable<NotificationResponse> getStoreDetail(
            NotificationRequest updateConfigRequest) {
        return repository.getNotifications(updateConfigRequest);
    }


}
