package com.mscanmc.mshop.mscanmc18.ui.mylist;

import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;

import com.mscanmc.mshop.mscanmc18.data.request.StoreDetailRequest;
import com.mscanmc.mshop.mscanmc18.data.response.HistoricGenericResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingList;
import com.mscanmc.mshop.mscanmc18.domain.repository.MScanRepository;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ShoppingListViewModel extends BaseViewModel {
    MScanRepository repository;
    private final MediatorLiveData<ShoppingList> shoppingListMediatorLiveData =
            new MediatorLiveData<>();
    private final MediatorLiveData<HistoricGenericResponse> historicListMediatorLiveData =
            new MediatorLiveData<>();

    @Inject
    public ShoppingListViewModel(MScanRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<ShoppingList> getShoppingListMutableLiveData() {
        return shoppingListMediatorLiveData;
    }

    public Observable<ShoppingList> getMyShoppingList(
            StoreDetailRequest updateConfigRequest) {
        return repository.getListOfShoppingList(updateConfigRequest);
    }

    public Observable<HistoricGenericResponse> getHistoricList(StoreDetailRequest storeDetailRequest) {
        return repository.getListOfHistoricList(storeDetailRequest);
    }
}
