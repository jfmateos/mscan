package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mscanmc.mshop.mscanmc18.network.BaseResponse;

public class GetCustomerTerminalStatusResponse extends BaseResponse {

    public String getTokenSession() {
        return tokenSession;
    }

    public void setTokenSession(String tokenSession) {
        this.tokenSession = tokenSession;
    }

    @SerializedName("tokenSession")
    @Expose
    private String tokenSession;

    /** No args constructor for use in serialization */
    public GetCustomerTerminalStatusResponse() {}

    /** @param tokenSession */
    public GetCustomerTerminalStatusResponse(String tokenSession) {
        super();
        this.tokenSession = tokenSession;
    }
}
