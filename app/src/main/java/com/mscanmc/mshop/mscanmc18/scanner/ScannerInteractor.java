package com.mscanmc.mshop.mscanmc18.scanner;

import com.symbol.emdk.barcode.ScanDataCollection;

/**
 * Created by victor on 7/6/17.
 * Mshop Spain.
 */

public interface ScannerInteractor {
    void processScannedData(ScanDataCollection.LabelType labelType, String data);
}
