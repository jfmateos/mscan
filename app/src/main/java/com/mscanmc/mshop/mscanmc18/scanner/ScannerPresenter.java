package com.mscanmc.mshop.mscanmc18.scanner;

/**
 * Created by victor on 6/6/17.
 * Mshop Spain.
 */

public interface ScannerPresenter {
    void setView(ScannerView scannerView);
    void setActivityView(ActivityScannerView scannerView);
    void initDeviceScanner();
    void unlockDeviceCradle();
    void releaseDeviceScanner();
    void enableNewDeliveryNotification(boolean enabled);
    void enableUrgentDeliveryNotification(boolean enabled);
    void playSoundNotification();
    void pauseScanner();
    void startScanner();
    void onDestroy(ScannerView scannerView);
}
