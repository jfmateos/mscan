package com.mscanmc.mshop.mscanmc18.di.qualifier;

import javax.inject.Qualifier;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Qualifier
public @interface ApplicationContext {
}
