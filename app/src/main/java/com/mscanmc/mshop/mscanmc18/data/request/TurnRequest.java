package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TurnRequest {
    @SerializedName("shopperCtx")
    @Expose
    private String shopperCtx;
    @SerializedName("storeId")
    @Expose
    private int storeId;
    @SerializedName("deskCode")
    @Expose
    private String deskCode;


    @SerializedName("notifyBefore")
    @Expose
    private String notifyBefore;

    /**
     * No args constructor for use in serialization
     */
    public TurnRequest() {
    }

    public TurnRequest(String shopperCtx, int storeId) {
        this.shopperCtx = shopperCtx;
        this.storeId = storeId;
    }
    public TurnRequest(String shopperCtx, int storeId, String deskCode, String notifyBefore) {
        this.shopperCtx = shopperCtx;
        this.storeId = storeId;
        this.deskCode = deskCode;
        this.notifyBefore = notifyBefore;
    }



    public String getShopperCtx() {
        return shopperCtx;
    }

    public void setShopperCtx(String shopperCtx) {
        this.shopperCtx = shopperCtx;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getDeskCode() {
        return deskCode;
    }

    public void setDeskCode(String deskCode) {
        this.deskCode = deskCode;
    }

    public String getNotifyBefore() {
        return notifyBefore;
    }

    public void setNotifyBefore(String notifyBefore) {
        this.notifyBefore = notifyBefore;
    }
}
