package com.mscanmc.mshop.mscanmc18.data.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntityDetailRequest {

    @SerializedName("tokenSession")
    @Expose
    private String tokenSession;

    @SerializedName("source")
    @Expose
    private String source;

    @SerializedName("idEntity")
    @Expose
    private int idEntity;

    @SerializedName("lang")
    @Expose
    private String lang;

    @SerializedName("idDesk")
    @Expose
    private int idDesk;

    public EntityDetailRequest() {}

    public EntityDetailRequest(
            String tokenSession, String source, int idEntity, String lang, int idDesk) {
        super();
        this.tokenSession = tokenSession;
        this.source = source;
        this.idEntity = idEntity;
        this.lang = lang;
        this.idDesk = idDesk;
    }

    public String getTokenSession() {
        return tokenSession;
    }

    public void setTokenSession(String tokenSession) {
        this.tokenSession = tokenSession;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(int idEntity) {
        this.idEntity = idEntity;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getIdDesk() {
        return idDesk;
    }

    public void setIdDesk(int idDesk) {
        this.idDesk = idDesk;
    }
}
