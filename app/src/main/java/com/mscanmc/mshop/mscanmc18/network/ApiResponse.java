package com.mscanmc.mshop.mscanmc18.network;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

import static com.mscanmc.mshop.mscanmc18.network.Status.ERROR;
import static com.mscanmc.mshop.mscanmc18.network.Status.LOADING;
import static com.mscanmc.mshop.mscanmc18.network.Status.SUCCESS;

public class ApiResponse<T extends Class<T>> {

    public final Status status;

    public final Class<T> data;

    @Nullable
    public final Throwable error;

    public ApiResponse(Status status, @Nullable Class<T> data, @Nullable Throwable error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static ApiResponse loading() {
        return new ApiResponse(LOADING, null, null);
    }

    public ApiResponse success(@NonNull T data) {
        return new ApiResponse(SUCCESS, data, null);
    }
}
