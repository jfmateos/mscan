package com.mscanmc.mshop.mscanmc18.ui;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.broadcast.CradleBroadCastReceiver;
import com.mscanmc.mshop.mscanmc18.cradle.CradleListener;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.data.request.EntityDetailRequest;
import com.mscanmc.mshop.mscanmc18.data.request.FinishSessionRequest;
import com.mscanmc.mshop.mscanmc18.data.request.GetCustomerTerminalStatusRequest;
import com.mscanmc.mshop.mscanmc18.data.request.GetEntityRequest;
import com.mscanmc.mshop.mscanmc18.data.request.ShopperScanRequest;
import com.mscanmc.mshop.mscanmc18.data.request.StoreDetailRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoItemRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoRequest;
import com.mscanmc.mshop.mscanmc18.data.request.UpdateConfigRequest;
import com.mscanmc.mshop.mscanmc18.data.response.EntityDeskList;
import com.mscanmc.mshop.mscanmc18.data.response.EntityDetailResponse;
import com.mscanmc.mshop.mscanmc18.data.response.GetCustomerTerminalStatusResponse;
import com.mscanmc.mshop.mscanmc18.data.response.GetEntityResponse;
import com.mscanmc.mshop.mscanmc18.data.response.Historic;
import com.mscanmc.mshop.mscanmc18.data.response.ShopperEvent;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListDetail;
import com.mscanmc.mshop.mscanmc18.databinding.ActivityMainBinding;
import com.mscanmc.mshop.mscanmc18.domain.service.AzureInstance;
import com.mscanmc.mshop.mscanmc18.domain.service.AzureService;
import com.mscanmc.mshop.mscanmc18.scanner.BarcodeScanner;
import com.mscanmc.mshop.mscanmc18.scanner.IOnScannerEvent;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseActivity;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseNavigation;
import com.mscanmc.mshop.mscanmc18.ui.base.ViewModelFactory;
import com.mscanmc.mshop.mscanmc18.ui.cart.CartViewModel;
import com.mscanmc.mshop.mscanmc18.ui.cart.ProductFragment;
import com.mscanmc.mshop.mscanmc18.ui.mylist.MyshoppingListRecyclerViewAdapter;
import com.mscanmc.mshop.mscanmc18.ui.mylist.ShoppingListFragment;
import com.mscanmc.mshop.mscanmc18.ui.mylist.ShoppingListViewModel;
import com.mscanmc.mshop.mscanmc18.ui.notifications.NotificationFragment;
import com.mscanmc.mshop.mscanmc18.ui.notifications.NotificationViewModel;
import com.mscanmc.mshop.mscanmc18.ui.turn.TurnFragment;
import com.mscanmc.mshop.mscanmc18.ui.turn.TurnViewModel;
import com.mscanmc.mshop.mscanmc18.utils.Constants;
import com.mscanmc.mshop.mscanmc18.utils.DataUtil;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.personalshopper.CradleException;
import com.symbol.emdk.personalshopper.CradleLedFlashInfo;
import com.symbol.emdk.personalshopper.CradleResults;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.pushy.sdk.Pushy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

import static com.mscanmc.mshop.mscanmc18.data.PreferencesHelper.PREF_FILE_NAME;
import static com.mscanmc.mshop.mscanmc18.data.PreferencesHelper.SAVED_LIST;
import static com.mscanmc.mshop.mscanmc18.data.PreferencesHelper.SELECTED_LIST;
import static com.mscanmc.mshop.mscanmc18.utils.Constants.AUDIT_REQUIRED;
import static com.mscanmc.mshop.mscanmc18.utils.Constants.END_OF_TRIP_CODE;
import static com.mscanmc.mshop.mscanmc18.utils.Constants.WAITING_ASSISTANCE;

public class MainActivity extends BaseActivity
        implements HasActivityInjector,
        HasSupportFragmentInjector,
        ShoppingListFragment.onListItemClick,
        BarcodeManager.ScannerConnectionListener,
        CradleListener,
        ProductFragment.onUpdateCartListener,
        TurnFragment.RequestTurn, IOnScannerEvent, NotificationFragment.OnListFragmentInteractionListener {

    @Inject
    protected DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;
    @Inject
    protected DispatchingAndroidInjector<Fragment> dispatchingFragmnetInjector;
    @Inject
    protected BaseNavigation navigation;
    @Inject
    protected ViewModelFactory viewModelFactory;
    @Inject
    protected BaseNavigation baseNavigation;
    private MainViewModel mViewModel;
    @Inject
    protected PreferencesHelper preferencesHelper;
    private ActivityMainBinding mBinding;
    private CartViewModel cartViewModel;
    private ShoppingListViewModel shoppingListViewModel;
    private TurnViewModel turnViewModel;
    MyshoppingListRecyclerViewAdapter.OnMyListItemClick myListItemClick;
    List<EntityDeskList> turnList = new ArrayList<>();
    private CradleBroadCastReceiver cradleBroadcastReceiver;
    private UpdatedTokenBroadcastReceiver updatedTokenBroadcastReceiver;
    private NewPushBroadcastReceiver newPushBroadcastReceiver;
    Observable<String> registerForPushy;
    private BaseActivity activity;
    private android.support.v7.app.AlertDialog mAlertDialog;
    AHBottomNavigationAdapter navigationAdapter;
    //TODO meter el idshopping en Preferences Helper
    int idShopping = -1;
    private NotificationViewModel notificationViewModel;

    Retrofit retrofit;
    private boolean showList = false;

    public static void start(BaseActivity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onWindowFocusChanged(false);
        updatedTokenBroadcastReceiver = new UpdatedTokenBroadcastReceiver();
        registerReceiver(
                updatedTokenBroadcastReceiver, new IntentFilter(Constants.BROADCAST_UPDATED_TOKEN));

        newPushBroadcastReceiver = new NewPushBroadcastReceiver();
        registerReceiver(newPushBroadcastReceiver, new IntentFilter(Constants.BROADCAST_PUSH));
        initCradleListener();
        BarcodeScanner.getInstance(this);
        BarcodeScanner.registerUIobject(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        onWindowFocusChanged(false);
        activity = this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //mBinding.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.navigation);
        setNavigationBottom();
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);
        cartViewModel = ViewModelProviders.of(this, viewModelFactory).get(CartViewModel.class);
        shoppingListViewModel = ViewModelProviders.of(this, viewModelFactory).get(ShoppingListViewModel.class);
        turnViewModel = ViewModelProviders.of(this, viewModelFactory).get(TurnViewModel.class);
        notificationViewModel = ViewModelProviders.of(this, viewModelFactory).get(NotificationViewModel.class);
        mBinding.setShowSplash(true);
        mAlertDialog = new AlertDialog.Builder(this).create();
        preferencesHelper.generateConfigFile(getApplicationContext(), "604");
        registerForPushy = obsRegisterForPushNotifications();
        getTurnList();
        if (isStoragePermissionGranted()) {
            registerForPushy
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(
                            new Observer<String>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onNext(String token) {
                                    Intent intent = new Intent(Constants.BROADCAST_UPDATED_TOKEN);
                                    intent.putExtra(Constants.TOKEN_SENT, token);
                                    sendBroadcast(intent);
                                    preferencesHelper.setPushyToken(getApplicationContext(), token);

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            e.getMessage(),
                                            Toast.LENGTH_SHORT)
                                            .show();
                                }

                                @Override
                                public void onComplete() {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            getString(R.string.pushy_registered),
                                            Toast.LENGTH_SHORT).show();
                                    try {
                                        updateCradle(true);
                                    } catch (Settings.SettingNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
        }
    }

    private void showLoading() {
        mBinding.setIsLoading(true);
    }

    private void hideLoading() {
        mBinding.setIsLoading(false);
    }

    private void setNavigationBottom() {
        AHBottomNavigationItem cart = new AHBottomNavigationItem(R.string.cart, R.drawable.ic_carro_nav, R.color.nav_bottom);
        AHBottomNavigationItem lastShopping = new AHBottomNavigationItem(R.string.my_lists, R.drawable.ic_listas_light, R.color.nav_bottom);
        AHBottomNavigationItem turns = new AHBottomNavigationItem(R.string.turns, R.drawable.ic_turnos_navbar, R.color.nav_bottom);
        AHBottomNavigationItem notifications = new AHBottomNavigationItem(R.string.title_notifications, R.drawable.ic_alert_menu, R.color.nav_bottom);
        mBinding.navigation.addItem(cart);
        mBinding.navigation.addItem(lastShopping);
        mBinding.navigation.addItem(turns);
        mBinding.navigation.addItem(notifications);
        mBinding.navigation.setBehaviorTranslationEnabled(false);
        mBinding.navigation.setInactiveColor(R.color.nav_bottom);
        mBinding.navigation.setForceTint(true);
        mBinding.navigation.setAccentColor(R.color.nav_bottom);
        mBinding.navigation.setTranslucentNavigationEnabled(false);
        mBinding.navigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        mBinding.navigation.setColored(true);
        mBinding.navigation.setCurrentItem(0);
        mBinding.navigation.setNotificationBackgroundColor(getResources().getColor(R.color.red));
        mBinding.navigation.setNotificationTextColor(getResources().getColor(R.color.white));
        mBinding.navigation.setOnTabSelectedListener((position, wasSelected) -> {
            showLoading();
            switch (position) {
                case 0:
                    if (preferencesHelper.isSelectedList(activity)) {
                        SharedPreferences sharedPreferences = mBinding.getRoot().getContext().getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(SELECTED_LIST,false);
                        editor.apply();
                        baseNavigation.showProductFragmentList(activity, idShopping);
                        hideLoading();
                    } else {
                        baseNavigation.showProductFragmentList(activity, -1);
                        hideLoading();
                    }

                    return true;
                case 1:
                    baseNavigation.showMyShoppingList(activity, R.id.main_contain, myListItemClick);
                    hideLoading();
                    return true;

                case 2:
                    getCustomerTurns();
                    hideLoading();
                    return true;

                case 3:
                    baseNavigation.showNotifications(activity);
                    hideLoading();
                    return true;

            }
            return true;
        });

    }

    /**
     * Cradle
     */
    @Override
    public void onCradleEmpty() throws Settings.SettingNotFoundException {
        mBinding.setShowSplash(false);
        updateCradle(false);
    }

    @Override
    public void onCradleBusy() throws CradleException, Settings.SettingNotFoundException {
        mAlertDialog.dismiss();
        mBinding.setShowSplash(true);
        mBinding.setIsLoading(false);
        try {
            Thread.sleep(300);
            updateCradle(true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    private void initCradleListener() {
        cradleBroadcastReceiver = new CradleBroadCastReceiver(this);
        registerReceiver(cradleBroadcastReceiver, new IntentFilter(Intent.ACTION_POWER_CONNECTED));
        registerReceiver(cradleBroadcastReceiver, new IntentFilter(Intent.ACTION_POWER_DISCONNECTED));
    }

    public void unlockDevice() throws Settings.SettingNotFoundException {
        if (BarcodeScanner.getPsObject() != null && BarcodeScanner.getPsObject().cradle != null) {
            int onDuration = 2000;
            int offDuration = 300;
            int unlockDuration = 15;
            try {
                CradleLedFlashInfo ledFlashInfo =
                        new CradleLedFlashInfo(onDuration, offDuration, true);
                CradleResults result = BarcodeScanner.getPsObject().cradle.unlock(unlockDuration, ledFlashInfo);
            } catch (CradleException e) {
                e.printStackTrace();
            }
        }

    }

    private void updateCradle(boolean isOnCradle) throws Settings.SettingNotFoundException {
        String data = isOnCradle ? getCradleData() : null;
        mViewModel
                .setUpdateConfig(
                        new UpdateConfigRequest(
                                0,
                                preferencesHelper.getUniqueId(getApplicationContext()),
                                "status",
                                String.valueOf(DataUtil.getBatteryLevel(getApplication())),
                                DataUtil.recoverStoreIdValue(),
                                DataUtil.DEVICE_TYPE,
                                DataUtil.DEVICE_MODEL,
                                data,
                                preferencesHelper.getPushyToken(getApplicationContext()),
                                DataUtil.getApplicationName(getApplicationContext()),
                                DataUtil.getBrand(),
                                DataUtil.getDeviceModel(),
                                DataUtil.getAndroidVersion(),
                                DataUtil.getAppVersion(getApplicationContext()),
                                DataUtil.getDeviceType(),
                                DataUtil.getConnection(getApplicationContext()),
                                DataUtil.getSsid(getApplicationContext()),
                                DataUtil.getMac(getApplication()),
                                preferencesHelper.getUniqueId(getApplicationContext()),
                                DataUtil.getIpaddress(getApplication()),
                                DataUtil.getSignal(getApplicationContext()),
                                DataUtil.getMajor(),
                                DataUtil.getMinor(),
                                DataUtil.getBrightness(this),
                                DataUtil.getPapperOut()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(genericMessageResponse -> {
                    Timber.wtf(String.valueOf(genericMessageResponse.getExecuteTime()));
                }, error -> error.printStackTrace());
    }

    public String getCradleData() {
        String params = "";
        try {
            if (!BarcodeScanner.getPsObject().cradle.isEnabled()) {
                BarcodeScanner.getPsObject().cradle.enable();
            }
            if (BarcodeScanner.getPsObject() != null
                    && BarcodeScanner.getPsObject().cradle != null
                    && BarcodeScanner.getPsObject().cradle.config.getLocation() != null) {
                int row = BarcodeScanner.getPsObject().cradle.config.getLocation().row;
                int column = BarcodeScanner.getPsObject().cradle.config.getLocation().column;
                int wall = BarcodeScanner.getPsObject().cradle.config.getLocation().wall;

                params = row + "-" + column + "-" + wall;
                Timber.i("Cradle updated");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (params.isEmpty()) {
            return null;
        } else {
            return params;
        }
    }


    private Observable<String> obsRegisterForPushNotifications() {
        return Observable.create(emitter -> {
            String deviceToken = Pushy.register(getApplicationContext());
            emitter.onNext(deviceToken);
            emitter.onComplete();
        });
    }

    @Override
    public void onConnectionChange(
            ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
    }

    @Override
    public void requestTurn(String deskCode, int item) {

    }


    @Override
    public void onPause() {
        super.onPause();
        hideLoading();
        BarcodeScanner.unregisterUIobject();
    }

    @Override
    public void onListFragmentInteraction(ShopperEvent item) {

    }


    private class AsyncDataUpdate extends AsyncTask<ScanDataCollection, Void, TripDtoItemRequest> {
        TripDtoRequest tripDtoRequest;
        TripDtoItemRequest itemRequest;

        @Override
        protected TripDtoItemRequest doInBackground(ScanDataCollection... params) {
            tripDtoRequest = new TripDtoRequest();
            tripDtoRequest.setItemList(new ArrayList<>());
            itemRequest = new TripDtoItemRequest();
            ScanDataCollection scanDataCollection = params[0];
            if (scanDataCollection != null
                    && scanDataCollection.getResult() == ScannerResults.SUCCESS) {
                List<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
                for (ScanDataCollection.ScanData data : scanData) {
                    String a = data.getData();
                    // TODO hay que meter el id del prodcuto
                    itemRequest.setCodeValue(data.getData());
                    itemRequest.setCodeType(data.getLabelType().name());
                    itemRequest.setQuantity(1);
                    itemRequest.setItemId("-1");
                    tripDtoRequest.setShopperCtx(preferencesHelper.getShopperCtxt(getApplicationContext()));
                    tripDtoRequest.getItemList().add(itemRequest);
                }
            }

            return itemRequest;
        }

        @Override
        protected void onPostExecute(TripDtoItemRequest result) {
            hideLoading();
            Timber.wtf(result.getCodeValue());
            if (result.getCodeValue().startsWith(AUDIT_REQUIRED)) {
                endOfTrip(result, AUDIT_REQUIRED);
            } else if (result.getCodeValue().startsWith(END_OF_TRIP_CODE)) {
                endOfTrip(result, END_OF_TRIP_CODE);
            } else if (result.getCodeValue().startsWith(WAITING_ASSISTANCE)) {
                endOfTrip(result, WAITING_ASSISTANCE);
            } else {
                cartViewModel.setTripDtoRequest(result);
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    /**
     * Turns
     */
    private void getTurnList() {

        AzureService azureService = AzureInstance.getRetrofitInstance().create(AzureService.class);
        azureService.getCustomerTerminalStatus(new GetCustomerTerminalStatusRequest(preferencesHelper.getUniqueId(this), "en-GB", "android")).enqueue(new Callback<GetCustomerTerminalStatusResponse>() {
            @Override
            public void onResponse(
                    Call<GetCustomerTerminalStatusResponse> call,
                    Response<GetCustomerTerminalStatusResponse> response) {
                if (response.isSuccessful()) {
                    //getReceipesListDataResponseMutableLiveData.setValue(response.body());
                    preferencesHelper.putAccessToken(response.body().getTokenSession());


                } else {
                    Timber.e("code: %s", response.code());
                }
            }

            @Override
            public void onFailure(
                    Call<GetCustomerTerminalStatusResponse> call, Throwable t) {
                Timber.wtf(t.getMessage());
                //getReceipesListDataResponseMutableLiveData.setValue(null);
            }
        });
    }

    private void getEntities() {
        GetEntityRequest getEntityRequest =
                new GetEntityRequest(preferencesHelper.getAccessToken());
        AzureService azureService = AzureInstance.getRetrofitInstance().create(AzureService.class);
        azureService.getEntities(getEntityRequest).enqueue(new Callback<GetEntityResponse>() {
            @Override
            public void onResponse(
                    Call<GetEntityResponse> call,
                    Response<GetEntityResponse> response) {
                if (response.isSuccessful()) {
                    //getReceipesListDataResponseMutableLiveData.setValue(response.body());
                    getCustomerTurns();

                } else {
                    Timber.e("code: %s", response.code());
                }
            }

            @Override
            public void onFailure(
                    Call<GetEntityResponse> call, Throwable t) {
                Timber.wtf(t.getMessage());
                //getReceipesListDataResponseMutableLiveData.setValue(null);
            }
        });
    }

    //TODO llamada harcodeada para la demo Octubre 2018
    public void getCustomerTurns() {
        showLoading();
        EntityDetailRequest entityDetailRequest =
                new EntityDetailRequest(preferencesHelper.getAccessToken(), preferencesHelper.getSource(), 1, "it_IT", 0);
        AzureService azureService = AzureInstance.getRetrofitInstance().create(AzureService.class);
        azureService.getEntityDetail(entityDetailRequest).enqueue(new Callback<EntityDetailResponse>() {
            @Override
            public void onResponse(
                    Call<EntityDetailResponse> call,
                    Response<EntityDetailResponse> response) {
                if (response.isSuccessful()) {
                    //getReceipesListDataResponseMutableLiveData.setValue(response.body());
                    // getCustomerTurns();
                    hideLoading();
                    turnList.clear();
                    turnList.addAll(response.body().getEntityDeskList());
                    baseNavigation.showTurn(activity, turnList);

                } else {
                    Timber.e("code: %s", response.code());
                }
            }

            @Override
            public void onFailure(
                    Call<EntityDetailResponse> call, Throwable t) {
                Timber.wtf(t.getMessage());
                //getReceipesListDataResponseMutableLiveData.setValue(null);
            }
        });
    }

    /**
     * List
     */
    private void getStoreList() {
        mViewModel
                .getStoreDetail(
                        new StoreDetailRequest(
                                preferencesHelper.getShopperCtxt(getApplicationContext()), DataUtil.recoverStoreIdValue()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeDetailResponse -> {
                    preferencesHelper.setStoreResponse(
                            getApplicationContext(), storeDetailResponse);
                }, error -> error.printStackTrace());
    }


    @Override
    public void onListItemClick(ShoppingListDetail item) {
        // mBinding.navigation.setSelectedItemId(R.id.ic_bottom_cart);
        idShopping = item.getId();
        showList = true;
        mBinding.navigation.setCurrentItem(0);

    }

    @Override
    public void onHistoricListListener(Historic historic) {
        Timber.wtf("historic");
        idShopping = Integer.parseInt(historic.getId());
        mBinding.navigation.setCurrentItem(0);
    }


    /**
     * @param result
     * @param option 1-> End of trip
     *               2->Audit required
     *               3->Waitting
     */
    private void endOfTrip(TripDtoItemRequest result, String option) {
        //TODO mirar el artículo desconocido
        boolean unknownItems = false;
        showLoading();
        LayoutInflater layoutInflater = LayoutInflater.from(mBinding.getRoot().getContext());
        View promptView = layoutInflater.inflate(R.layout.dialog_finish_scan_you_session, null);
        AppCompatTextView tvTitle = (AppCompatTextView) promptView.findViewById(R.id.tv_finish_session_title);
        AppCompatTextView tvSubTitle = (AppCompatTextView) promptView.findViewById(R.id.tv_finish_session_subtitle);
        int showDialogType = DataUtil.convertCode(option);

        tvTitle.setText(getString(R.string.shopping_ended));
        tvSubTitle.setText(getString(R.string.shopping_ended_message));
        mViewModel.setEndOfTrip(
                new FinishSessionRequest(
                        preferencesHelper.getShopperCtxt(getApplicationContext()),
                        Constants.EAN13, result.getCodeValue(),
                        unknownItems)
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(finishSessionResponse -> {
                    hideLoading();
                    if (finishSessionResponse.getStatus().getCode().equalsIgnoreCase(AUDIT_REQUIRED)) {
                        tvTitle.setText(getString(R.string.audit_required));
                        tvSubTitle.setText(getString(R.string.audit_required_subtitle));
                    } else if (finishSessionResponse.getStatus().getCode().equalsIgnoreCase(WAITING_ASSISTANCE)) {
                        tvTitle.setText(getString(R.string.waitting_assistance));
                        tvSubTitle.setText(getString(R.string.waitting_assistance_subtitle));
                    }
                }, e -> {
                    e.printStackTrace();
                    hideLoading();
                });

        mAlertDialog.setView(promptView);
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();
    }


    @Override
    public void updateCartItems(int quantity) {
        mBinding.navigation.setNotification(String.valueOf(quantity), 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            registerForPushy
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(
                            new Observer<String>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onNext(String token) {
                                    Intent intent = new Intent(Constants.BROADCAST_UPDATED_TOKEN);
                                    intent.putExtra(Constants.TOKEN_SENT, token);
                                    sendBroadcast(intent);
                                    preferencesHelper.setPushyToken(getApplicationContext(), token);

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            e.getMessage(),
                                            Toast.LENGTH_SHORT)
                                            .show();
                                }

                                @Override
                                public void onComplete() {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            getString(R.string.pushy_registered),
                                            Toast.LENGTH_SHORT).show();
                                    try {
                                        updateCradle(true);
                                    } catch (Settings.SettingNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }


    /**
     * Broadcasting
     */
    public class UpdatedTokenBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String token = intent.getStringExtra(Constants.TOKEN_SENT);
            if (token != null) {
                Timber.i("token updated ");
                preferencesHelper.setPushyToken(token);
            }
        }
    }


    public class NewPushBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String pushContent = intent.getStringExtra(Constants.BROADCAST_PUSH_MESSAGE);
            String encodedId = intent.getStringExtra(Constants.BROADCAST_PUSH_ENCODED_ID);

            if (encodedId != null) {

                mViewModel.getShopperScan(new ShopperScanRequest(encodedId, preferencesHelper.getStoreId(), preferencesHelper.getUniqueId(getApplicationContext())))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(shopperScanResponse -> {
                            preferencesHelper.setShopperCtx(getApplicationContext(), shopperScanResponse.getShopperCtx());
                            getStoreList();
                            baseNavigation.showProductFragmentList(activity);
                            try {
                                unlockDevice();
                            } catch (Settings.SettingNotFoundException e) {
                                e.printStackTrace();
                            }
                        });
            }

        }

    }


    @Override
    public void onDataScanned(ScanDataCollection scanData) {
        showLoading();
        new AsyncDataUpdate().execute(scanData);
    }

    @Override
    public void onStatusUpdate(String scanStatus) {
        Timber.d(scanStatus);
    }

    @Override
    public void onError() {
    }

    @Override
    public void onStop() {
        super.onStop();
        BarcodeScanner.deInitScanner();
        //resuming from a screen suspend event the onDestroy method doesn't trigger, therefore releasing EMDK here
        BarcodeScanner.releaseEmdk();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Given EMDK is already released from the onStop method, this isn't necessary
        BarcodeScanner.releaseEmdk();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingFragmnetInjector;
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }


}
