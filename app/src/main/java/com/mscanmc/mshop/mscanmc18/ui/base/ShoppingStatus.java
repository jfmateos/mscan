package com.mscanmc.mshop.mscanmc18.ui.base;

import android.os.Parcel;
import android.os.Parcelable;

public class ShoppingStatus implements Parcelable {
    public final static String STATUS_NOT_YET_ACCEPTED = "SHOPPER_NOT_YET_ACCEPTED";
    public final static String STATUS_OUT_OF_TRIP = "OUT_OF_TRIP";
    public final static String STATUS_FAKE_OUT_OF_TRIP = "STATUS_FAKE_OUT_OF_TRIP";
    public final static String STATUS_TRIP_STARTED = "TRIP_STARTED";
    public final static String STATUS_TRIP_STARTED_BEACONS = "TRIP_STARTED_BEACONS";
    public final static String STATUS_TRIP_STARTED_DATAMATRIX = "TRIP_STARTED_DATAMATRIX";
    public final static String STATUS_AUDIT_REQUIRED = "AUDIT_REQUIRED";
    public final static String STATUS_WAITING_ASSISTANCE = "WAITING_ASSISTANCE";
    public final static String STATUS_WAITING_TO_PAY = "WAITING_TO_PAY";
    public final static String STATUS_WAITING_ACTIVATION = "WAITING_ACTIVATION";

    private String code;
    private String message;

    public ShoppingStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }

    protected ShoppingStatus(Parcel in) {
        code = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShoppingStatus> CREATOR = new Creator<ShoppingStatus>() {
        @Override
        public ShoppingStatus createFromParcel(Parcel in) {
            return new ShoppingStatus(in);
        }

        @Override
        public ShoppingStatus[] newArray(int size) {
            return new ShoppingStatus[size];
        }
    };

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
