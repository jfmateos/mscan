package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeskList {

    @SerializedName("deskCode")
    @Expose
    private String deskCode;
    @SerializedName("deskDescription")
    @Expose
    private String deskDescription;
    @SerializedName("deskType")
    @Expose
    private String deskType;
    @SerializedName("currentTurn")
    @Expose
    private int currentTurn;
    @SerializedName("predictedNextTurn")
    @Expose
    private String predictedNextTurn;
    @SerializedName("myTurn")
    @Expose
    private String myTurn;

    /**
     * No args constructor for use in serialization
     *
     */
    public DeskList() {
    }

    /**
     *
     * @param deskType
     * @param predictedNextTurn
     * @param deskDescription
     * @param currentTurn
     * @param deskCode
     * @param myTurn
     */
    public DeskList(String deskCode, String deskDescription, String deskType, int currentTurn, String predictedNextTurn, String myTurn) {
        super();
        this.deskCode = deskCode;
        this.deskDescription = deskDescription;
        this.deskType = deskType;
        this.currentTurn = currentTurn;
        this.predictedNextTurn = predictedNextTurn;
        this.myTurn = myTurn;
    }

    public String getDeskCode() {
        return deskCode;
    }

    public void setDeskCode(String deskCode) {
        this.deskCode = deskCode;
    }

    public String getDeskDescription() {
        return deskDescription;
    }

    public void setDeskDescription(String deskDescription) {
        this.deskDescription = deskDescription;
    }

    public String getDeskType() {
        return deskType;
    }

    public void setDeskType(String deskType) {
        this.deskType = deskType;
    }

    public int getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(int currentTurn) {
        this.currentTurn = currentTurn;
    }

    public String getPredictedNextTurn() {
        return predictedNextTurn;
    }

    public void setPredictedNextTurn(String predictedNextTurn) {
        this.predictedNextTurn = predictedNextTurn;
    }

    public String getMyTurn() {
        return myTurn;
    }

    public void setMyTurn(String myTurn) {
        this.myTurn = myTurn;
    }

}
