package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripDtoRequest {

    @SerializedName("itemList")
    @Expose
    private List<TripDtoItemRequest> itemList = null;

    @SerializedName("shopperCtx")
    @Expose
    private String shopperCtx;

    /** No args constructor for use in serialization */
    public TripDtoRequest() {}

    /**
     * @param itemList
     * @param shopperCtx
     */
    public TripDtoRequest(List<TripDtoItemRequest> itemList, String shopperCtx) {
        super();
        this.itemList = itemList;
        this.shopperCtx = shopperCtx;
    }

    public List<TripDtoItemRequest> getItemList() {
        return itemList;
    }

    public void setItemList(List<TripDtoItemRequest> itemList) {
        this.itemList = itemList;
    }

    public String getShopperCtx() {
        return shopperCtx;
    }

    public void setShopperCtx(String shopperCtx) {
        this.shopperCtx = shopperCtx;
    }
}
