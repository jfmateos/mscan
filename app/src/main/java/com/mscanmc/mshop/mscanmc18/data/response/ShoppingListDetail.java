package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShoppingListDetail {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("version")
    @Expose
    private double version;
    @SerializedName("sentBy")
    @Expose
    private String sentBy;
    @SerializedName("entriesCount")
    @Expose
    private String entriesCount;
    @SerializedName("shared")
    @Expose
    private String shared;
    @SerializedName("colorCode")
    @Expose
    private String colorCode;
    @SerializedName("colorBorderCode")
    @Expose
    private String colorBorderCode;
    @SerializedName("reminderDate")
    @Expose
    private String reminderDate;
    @SerializedName("userCreationDate")
    @Expose
    private String userCreationDate;
    @SerializedName("userModificationDate")
    @Expose
    private String userModificationDate;
    @SerializedName("favourite")
    @Expose
    private boolean favourite;
    @SerializedName("editable")
    @Expose
    private boolean editable;
    @SerializedName("ownerId")
    @Expose
    private String ownerId;
    @SerializedName("urlShare")
    @Expose
    private String urlShare;
    @SerializedName("sharedShoppers")
    @Expose
    private List<SharedShopper> sharedShoppers;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("locatedItems")
    @Expose
    private List<LocatedItem> locatedItems = null;

    /**
     * No args constructor for use in serialization
     */
    public ShoppingListDetail() {
    }

    /**
     * @param id
     * @param code
     * @param label
     * @param version
     * @param sentBy
     * @param entriesCount
     * @param shared
     * @param colorCode
     * @param colorBorderCode
     * @param reminderDate
     * @param userCreationDate
     * @param userModificationDate
     * @param favourite
     * @param editable
     * @param ownerId
     * @param urlShare
     * @param sharedShoppers
     * @param items
     * @param locatedItems
     */
    public ShoppingListDetail(int id, String code, String label, double version, String sentBy, String entriesCount, String shared, String colorCode, String colorBorderCode, String reminderDate, String userCreationDate, String userModificationDate, boolean favourite, boolean editable, String ownerId, String urlShare,List<SharedShopper>  sharedShoppers, List<Item> items, List<LocatedItem> locatedItems) {
        super();
        this.id = id;
        this.code = code;
        this.label = label;
        this.version = version;
        this.sentBy = sentBy;
        this.entriesCount = entriesCount;
        this.shared = shared;
        this.colorCode = colorCode;
        this.colorBorderCode = colorBorderCode;
        this.reminderDate = reminderDate;
        this.userCreationDate = userCreationDate;
        this.userModificationDate = userModificationDate;
        this.favourite = favourite;
        this.editable = editable;
        this.ownerId = ownerId;
        this.urlShare = urlShare;
        this.sharedShoppers = sharedShoppers;
        this.items = items;
        this.locatedItems = locatedItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public String getSentBy() {
        return sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public String getEntriesCount() {
        return entriesCount;
    }

    public void setEntriesCount(String entriesCount) {
        this.entriesCount = entriesCount;
    }

    public String getShared() {
        return shared;
    }

    public void setShared(String shared) {
        this.shared = shared;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getColorBorderCode() {
        return colorBorderCode;
    }

    public void setColorBorderCode(String colorBorderCode) {
        this.colorBorderCode = colorBorderCode;
    }

    public String getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(String reminderDate) {
        this.reminderDate = reminderDate;
    }

    public String getUserCreationDate() {
        return userCreationDate;
    }

    public void setUserCreationDate(String userCreationDate) {
        this.userCreationDate = userCreationDate;
    }

    public String getUserModificationDate() {
        return userModificationDate;
    }

    public void setUserModificationDate(String userModificationDate) {
        this.userModificationDate = userModificationDate;
    }

    public boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public boolean getEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getUrlShare() {
        return urlShare;
    }

    public void setUrlShare(String urlShare) {
        this.urlShare = urlShare;
    }

    public List<SharedShopper>  getSharedShoppers() {
        return sharedShoppers;
    }

    public void setSharedShoppers(List<SharedShopper>  sharedShoppers) {
        this.sharedShoppers = sharedShoppers;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<LocatedItem> getLocatedItems() {
        return locatedItems;
    }

    public void setLocatedItems(List<LocatedItem> locatedItems) {
        this.locatedItems = locatedItems;
    }
}
