package com.mscanmc.mshop.mscanmc18.di.module;


import com.mscanmc.mshop.mscanmc18.ui.cart.ProductFragment;
import com.mscanmc.mshop.mscanmc18.ui.mylist.ShoppingListFragment;
import com.mscanmc.mshop.mscanmc18.ui.notifications.NotificationFragment;
import com.mscanmc.mshop.mscanmc18.ui.offer.OfferFragment;
import com.mscanmc.mshop.mscanmc18.ui.turn.TurnFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Module
public abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    public abstract ProductFragment contributeProductFragment();

    @ContributesAndroidInjector
    public abstract ShoppingListFragment contributeShoppingListFragment();

    @ContributesAndroidInjector
    public abstract TurnFragment contributeTurnFragment();

    @ContributesAndroidInjector
    public abstract NotificationFragment contributeNotificationFragment();

    @ContributesAndroidInjector
    public abstract OfferFragment contributeOfferFragment();

}
