package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerTurnsResponse {

    @SerializedName("ticketsList")
    @Expose
    private List<CustomerTurnsList> ticketsList = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public CustomerTurnsResponse() {
    }

    /**
     *
     * @param ticketsList
     */
    public CustomerTurnsResponse(List<CustomerTurnsList> ticketsList) {
        super();
        this.ticketsList = ticketsList;
    }

    public List<CustomerTurnsList> getTicketsList() {
        return ticketsList;
    }

    public void setTicketsList(List<CustomerTurnsList> ticketsList) {
        this.ticketsList = ticketsList;
    }

}



