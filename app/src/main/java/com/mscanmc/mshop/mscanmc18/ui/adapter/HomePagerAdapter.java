package com.mscanmc.mshop.mscanmc18.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mscanmc.mshop.mscanmc18.data.response.GetEntityListResponse;

import java.util.ArrayList;

/**
 * Created by victor on 17/5/18.
 * Mshop Spain.
 */
public class HomePagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragmentList = new ArrayList<>();


    public HomePagerAdapter(FragmentManager fm, GetEntityListResponse shoppingList) {
        super(fm);
       /* fragmentList.add(FragmentBasketList.newInstance());
        fragmentList.add(FragmentMyShoppingList.newInstance(shoppingList));*/
    }


    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
