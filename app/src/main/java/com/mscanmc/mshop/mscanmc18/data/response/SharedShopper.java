package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SharedShopper {

    @SerializedName("shoppingList")
    @Expose
    private ShoppingListDetail shoppingList;
    @SerializedName("guestShopper")
    @Expose
    private GuestShopper guestShopper;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * No args constructor for use in serialization
     */
    public SharedShopper() {
    }

    /**
     * @param guestShopper
     * @param shoppingList
     * @param status
     */
    public SharedShopper(ShoppingListDetail shoppingList, GuestShopper guestShopper, String status) {
        super();
        this.shoppingList = shoppingList;
        this.guestShopper = guestShopper;
        this.status = status;
    }

    public ShoppingListDetail getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(ShoppingListDetail shoppingList) {
        this.shoppingList = shoppingList;
    }

    public GuestShopper getGuestShopper() {
        return guestShopper;
    }

    public void setGuestShopper(GuestShopper guestShopper) {
        this.guestShopper = guestShopper;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

