package com.mscanmc.mshop.mscanmc18.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import com.mscanmc.mshop.mscanmc18.cradle.CradleListener;
import com.symbol.emdk.personalshopper.CradleException;

public class CradleBroadCastReceiver extends BroadcastReceiver {
    private CradleListener cradleListener;


    public CradleBroadCastReceiver(CradleListener cradleListener) {
        this.cradleListener = cradleListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().contentEquals(Intent.ACTION_POWER_CONNECTED)) {
            try {
                cradleListener.onCradleBusy();
            } catch (CradleException e) {
                e.printStackTrace();
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        } else if (intent.getAction().contentEquals(Intent.ACTION_POWER_DISCONNECTED)) {
            try {
                cradleListener.onCradleEmpty();
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
