package com.mscanmc.mshop.mscanmc18.ui.base;

import android.arch.lifecycle.ViewModel;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Juan Francisco Mateos Redondo
 */

public class BaseViewModel extends ViewModel {

    protected CompositeDisposable compositeDisposable;

    public BaseViewModel() {
        compositeDisposable = new CompositeDisposable();
    }

    public void clearSubscriptions() {
        compositeDisposable.clear();
    }


    //TODO loading in viewmodel
  /*  public ObservableBoolean progressVisibile = new ObservableBoolean();

    public BaseViewModel() {
        progressVisibile.set(false);
    }

    public ObservableBoolean getProgressVisibile() {
        return progressVisibile;
    }

    public void setProgressVisibile(ObservableBoolean progressVisibile) {
        this.progressVisibile = progressVisibile;
    }

    public void showLoading() {
        this.progressVisibile.set(true);
    }

    public void hideLoading() {
        this.progressVisibile.set(false);
    }*/
}
