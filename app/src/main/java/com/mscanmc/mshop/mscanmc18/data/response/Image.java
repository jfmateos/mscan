package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("id")
    @Expose
    private double id;
    @SerializedName("imageName")
    @Expose
    private String imageName;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("htmlLink")
    @Expose
    private String htmlLink;
    @SerializedName("isTemp")
    @Expose
    private String isTemp;
    @SerializedName("isCopy")
    @Expose
    private String isCopy;

    /**
     * No args constructor for use in serialization
     */
    public Image() {
    }

    /**
     * @param id
     * @param isTemp
     * @param imageUrl
     * @param imageName
     * @param isCopy
     * @param htmlLink
     */
    public Image(double id, String imageName, String imageUrl, String htmlLink, String isTemp, String isCopy) {
        super();
        this.id = id;
        this.imageName = imageName;
        this.imageUrl = imageUrl;
        this.htmlLink = htmlLink;
        this.isTemp = isTemp;
        this.isCopy = isCopy;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getHtmlLink() {
        return htmlLink;
    }

    public void setHtmlLink(String htmlLink) {
        this.htmlLink = htmlLink;
    }

    public String getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(String isTemp) {
        this.isTemp = isTemp;
    }

    public String getIsCopy() {
        return isCopy;
    }

    public void setIsCopy(String isCopy) {
        this.isCopy = isCopy;
    }
}
