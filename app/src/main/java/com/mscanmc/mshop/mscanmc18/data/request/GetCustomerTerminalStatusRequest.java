package com.mscanmc.mshop.mscanmc18.data.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCustomerTerminalStatusRequest {

    @SerializedName("terminalUniqueId")
    @Expose
    private String terminalUniqueId;

    @SerializedName("lang")
    @Expose
    private String lang;

    @SerializedName("platform")
    @Expose
    private String platform;

    /** No args constructor for use in serialization */
    public GetCustomerTerminalStatusRequest() {}

    /**
     * @param platform
     * @param lang
     * @param terminalUniqueId
     */
    public GetCustomerTerminalStatusRequest(String terminalUniqueId, String lang, String platform) {
        super();
        this.terminalUniqueId = terminalUniqueId;
        this.lang = lang;
        this.platform = platform;
    }

    public String getTerminalUniqueId() {
        return terminalUniqueId;
    }

    public void setTerminalUniqueId(String terminalUniqueId) {
        this.terminalUniqueId = terminalUniqueId;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}

