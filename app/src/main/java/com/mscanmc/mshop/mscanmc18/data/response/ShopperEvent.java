package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShopperEvent {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("shortcut")
    @Expose
    private String shortcut;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("eventTypeCode")
    @Expose
    private String eventTypeCode;
    @SerializedName("messageModelCode")
    @Expose
    private String messageModelCode;
    @SerializedName("paramValue")
    @Expose
    private String paramValue;
    @SerializedName("display")
    @Expose
    private boolean display;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("read")
    @Expose
    private String read;
    @SerializedName("sharedStatus")
    @Expose
    private String sharedStatus;
    @SerializedName("openExtendedText")
    @Expose
    private String openExtendedText;
    @SerializedName("extendedText")
    @Expose
    private String extendedText;
    @SerializedName("boxType")
    @Expose
    private double boxType;
    @SerializedName("iconImageUrl")
    @Expose
    private String iconImageUrl;
    @SerializedName("elementTitle")
    @Expose
    private String elementTitle;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("elementBody")
    @Expose
    private String elementBody;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("otherShopperId")
    @Expose
    private String otherShopperId;
    @SerializedName("urlShare")
    @Expose
    private String urlShare;

    /**
     * No args constructor for use in serialization
     */
    public ShopperEvent() {
    }

    /**
     * @param extendedText
     * @param boxType
     * @param urlShare
     * @param display
     * @param paramValue
     * @param deleted
     * @param elementBody
     * @param shortcut
     * @param id
     * @param message
     * @param creationDate
     * @param elementTitle
     * @param price
     * @param sharedStatus
     * @param iconImageUrl
     * @param otherShopperId
     * @param subtitle
     * @param eventTypeCode
     * @param images
     * @param read
     * @param openExtendedText
     * @param messageModelCode
     */
    public ShopperEvent(int id, String shortcut, String creationDate, String message, List<Image> images, String eventTypeCode, String messageModelCode, String paramValue, boolean display, String deleted, String read, String sharedStatus, String openExtendedText, String extendedText, double boxType, String iconImageUrl, String elementTitle, String subtitle, String elementBody, String price, String otherShopperId, String urlShare) {
        super();
        this.id = id;
        this.shortcut = shortcut;
        this.creationDate = creationDate;
        this.message = message;
        this.images = images;
        this.eventTypeCode = eventTypeCode;
        this.messageModelCode = messageModelCode;
        this.paramValue = paramValue;
        this.display = display;
        this.deleted = deleted;
        this.read = read;
        this.sharedStatus = sharedStatus;
        this.openExtendedText = openExtendedText;
        this.extendedText = extendedText;
        this.boxType = boxType;
        this.iconImageUrl = iconImageUrl;
        this.elementTitle = elementTitle;
        this.subtitle = subtitle;
        this.elementBody = elementBody;
        this.price = price;
        this.otherShopperId = otherShopperId;
        this.urlShare = urlShare;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShortcut() {
        return shortcut;
    }

    public void setShortcut(String shortcut) {
        this.shortcut = shortcut;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getEventTypeCode() {
        return eventTypeCode;
    }

    public void setEventTypeCode(String eventTypeCode) {
        this.eventTypeCode = eventTypeCode;
    }

    public String getMessageModelCode() {
        return messageModelCode;
    }

    public void setMessageModelCode(String messageModelCode) {
        this.messageModelCode = messageModelCode;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getSharedStatus() {
        return sharedStatus;
    }

    public void setSharedStatus(String sharedStatus) {
        this.sharedStatus = sharedStatus;
    }

    public String getOpenExtendedText() {
        return openExtendedText;
    }

    public void setOpenExtendedText(String openExtendedText) {
        this.openExtendedText = openExtendedText;
    }

    public String getExtendedText() {
        return extendedText;
    }

    public void setExtendedText(String extendedText) {
        this.extendedText = extendedText;
    }

    public double getBoxType() {
        return boxType;
    }

    public void setBoxType(double boxType) {
        this.boxType = boxType;
    }

    public String getIconImageUrl() {
        return iconImageUrl;
    }

    public void setIconImageUrl(String iconImageUrl) {
        this.iconImageUrl = iconImageUrl;
    }

    public String getElementTitle() {
        return elementTitle;
    }

    public void setElementTitle(String elementTitle) {
        this.elementTitle = elementTitle;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getElementBody() {
        return elementBody;
    }

    public void setElementBody(String elementBody) {
        this.elementBody = elementBody;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOtherShopperId() {
        return otherShopperId;
    }

    public void setOtherShopperId(String otherShopperId) {
        this.otherShopperId = otherShopperId;
    }

    public String getUrlShare() {
        return urlShare;
    }

    public void setUrlShare(String urlShare) {
        this.urlShare = urlShare;
    }

}
