package com.mscanmc.mshop.mscanmc18.ui.base;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mscanmc.mshop.mscanmc18.R;

import io.reactivex.annotations.NonNull;
import timber.log.Timber;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@GlideModule
public class BindingAdapter extends AppGlideModule {
    String imageUrl;

    @android.databinding.BindingAdapter(value = {"visibleGone"})
    public static void setVisibility(View view, Boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @android.databinding.BindingAdapter(value = {"visibleGone"})
    public static void setVisibility(View view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @android.databinding.BindingAdapter(value = {"imageUrl"})
    public static void loadImage(AppCompatImageView view, String imageUrl) {
        // TODO on error poner un placeholder
        RequestOptions options =
                new RequestOptions()
                        .fallback(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.NONE);
        if (imageUrl == null) {
            view.setImageDrawable(view.getResources().getDrawable(R.drawable.placeholder));
        } else {
            if (imageUrl.startsWith("https://www.alcampo.eshttps://")) ;
            {
                imageUrl = imageUrl.replace("https://www.alcampo.eshttps://", "https://");
            }
            if (imageUrl.endsWith(".gif")) {
                Glide.with(view.getContext())
                        .load(imageUrl)
                        .apply(options)
                        .into(view);
            } else {
                //Picasso.get().load(imageUrl).error(R.drawable.a).into(view);

                Glide.with(view.getContext())
                        .load(imageUrl)
                        .apply(new RequestOptions()
                                .dontAnimate()
                                .skipMemoryCache(false))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@android.support.annotation.Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                Timber.wtf("falla "+model);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .into(view);
            }
        }
    }


    @android.databinding.BindingAdapter(value = {"price"})
    public static void printEur(AppCompatTextView textView, @NonNull String price) {
        if (price != null) {
            if (!price.equalsIgnoreCase("")) {
                double result = Double.parseDouble(price) / 100.00;
                String tmp = String.valueOf(result).replace(".", ",");
                RelativeSizeSpan smallSizeText = new RelativeSizeSpan(.7f);
                SpannableStringBuilder builder = new SpannableStringBuilder(tmp + " €");
                if (!builder.toString().equalsIgnoreCase("")) {
                    builder.setSpan(
                            smallSizeText,
                            String.valueOf(tmp).indexOf(","),
                            String.valueOf(tmp).length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    textView.setText(builder);
                }
            }
        }
    }

    @android.databinding.BindingAdapter(value = {"quantity"})
    public static void printQuantity(AppCompatTextView textView, @NonNull int price) {
        textView.setText(String.valueOf(price));
    }
}
