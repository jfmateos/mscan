package com.mscanmc.mshop.mscanmc18.di.module;

import android.app.Application;
import android.content.Context;

import com.mscanmc.mshop.mscanmc18.App;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.di.module.network.RetrofitModule;
import com.mscanmc.mshop.mscanmc18.di.module.viewmodel.ViewModelModule;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseNavigation;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Module(includes = {ViewModelModule.class, RetrofitModule.class})
public class AppModule {
    Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplication(Application application) {
        return application;
    }

    @Provides
    @Singleton
    public App provideApp(App application) {
        return application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    BaseNavigation provideNavigator() {
        return new BaseNavigation();
    }

    @Provides
    @Singleton
    PreferencesHelper provideHelper() {
        return new PreferencesHelper(application.getApplicationContext());
    }
}
