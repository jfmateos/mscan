package com.mscanmc.mshop.mscanmc18.domain.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.data.request.FinishSessionRequest;
import com.mscanmc.mshop.mscanmc18.data.request.GetEntityListRequest;
import com.mscanmc.mshop.mscanmc18.data.request.NotificationRequest;
import com.mscanmc.mshop.mscanmc18.data.request.ShopperScanRequest;
import com.mscanmc.mshop.mscanmc18.data.request.StoreDetailRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoItemInitialRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TurnRequest;
import com.mscanmc.mshop.mscanmc18.data.request.UpdateConfigRequest;
import com.mscanmc.mshop.mscanmc18.data.response.Basket;
import com.mscanmc.mshop.mscanmc18.data.response.DeskListGeneric;
import com.mscanmc.mshop.mscanmc18.data.response.FinishSessionResponse;
import com.mscanmc.mshop.mscanmc18.data.response.GenericMessageResponse;
import com.mscanmc.mshop.mscanmc18.data.response.HistoricGenericResponse;
import com.mscanmc.mshop.mscanmc18.data.response.NotificationResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ProductQuantityResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShopperScanResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingList;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListLastShopping;
import com.mscanmc.mshop.mscanmc18.data.response.StoreDetailResponse;
import com.mscanmc.mshop.mscanmc18.domain.service.ScanService;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Juan Francisco Mateos Redondo
 */
public class MScanRepository {
    ScanService scanService;

    Basket basket = new Basket();
    @Inject
    PreferencesHelper preferencesHelper;

    @Inject
    public MScanRepository(ScanService scanService) {
        this.scanService = scanService;
    }

    public Observable<GenericMessageResponse> getUpdateConfig(
            UpdateConfigRequest updateConfigRequest) {

        return scanService.updateConfig(updateConfigRequest);
    }

    public Observable<StoreDetailResponse> getStoreDetail(StoreDetailRequest storeDetailRequest) {
        HashMap<String, String> params = new HashMap<>();
        params.put("shopperCtx", storeDetailRequest.getShopperCtx());
        params.put("idStore", String.valueOf(storeDetailRequest.getStoreId()));
        return scanService.getStoreDetail(params);
    }

    public Observable<ShopperScanResponse> getShopperScan(ShopperScanRequest shopperScanRequest) {

        return scanService.connectShopperScan(shopperScanRequest);
    }

    public Observable<ProductQuantityResponse> setCart(TripDtoRequest tripDtoRequest) {

        return scanService.SetProductQuantity(tripDtoRequest);
    }

    public Observable<ShoppingList> getListOfShoppingList(StoreDetailRequest storeDetailRequest) {
        HashMap<String, String> params = new HashMap<>();
        params.put("shopperCtx", storeDetailRequest.getShopperCtx());
        params.put("storeId", String.valueOf(storeDetailRequest.getStoreId()));
        return scanService.getListOfShoppingList(params);
    }

    public Observable<ShoppingListLastShopping> getShoppingListDetail(GetEntityListRequest entityListRequest) {

        return scanService.getShoppingListDetails(entityListRequest);
    }

    public Observable<HistoricGenericResponse> getListOfHistoricList(StoreDetailRequest storeDetailRequest) {
        HashMap<String, String> params = new HashMap<>();
        params.put("shopperCtx", storeDetailRequest.getShopperCtx());
        params.put("limit", String.valueOf(10));
        return scanService.getHistoricList(params);
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }


    public Observable<ProductQuantityResponse> getInitCart(TripDtoItemInitialRequest tripDtoItemInitialRequest) {
        return scanService.SetProductQuantity(tripDtoItemInitialRequest);
    }


    public Observable<DeskListGeneric> getTurn(TurnRequest turnRequest) {
        return scanService.getDeskTurn(turnRequest);

    }

    public LiveData<DeskListGeneric> getTurnRequest(TurnRequest turnRequest) {
        final MutableLiveData<DeskListGeneric> deskListGenericMutableLiveData =
                new MutableLiveData<>();
        Call<DeskListGeneric> cryptoPOJOCall = scanService.getTurnRequest(turnRequest);
        cryptoPOJOCall.enqueue(
                new Callback<DeskListGeneric>() {
                    @Override
                    public void onResponse(
                            Call<DeskListGeneric> call, Response<DeskListGeneric> response) {
                        if (response.isSuccessful()) {
                            deskListGenericMutableLiveData.setValue(response.body());
                            Timber.d("posts loaded from API");
                        } else {
                            int statusCode = response.code();
                            Timber.e("code: %s", response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<DeskListGeneric> call, Throwable t) {
                        Timber.wtf(t.getMessage());
                        deskListGenericMutableLiveData.setValue(null);
                    }
                });
        return deskListGenericMutableLiveData;
    }

    public Observable<FinishSessionResponse> setEnOfTrip(FinishSessionRequest enOfTrip) {
        return scanService.endOfTrip(enOfTrip.toParams());
    }


    public Observable<NotificationResponse> getNotifications(NotificationRequest notificationRequest) {
        return scanService.getNotifications(notificationRequest.toHashMap());
    }

    public Observable<ProductQuantityResponse> removeItem(TripDtoRequest tripSetProductListDto) {
        return scanService.removeItem(tripSetProductListDto);
    }

}
