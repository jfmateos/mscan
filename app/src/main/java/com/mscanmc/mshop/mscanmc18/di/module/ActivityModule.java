package com.mscanmc.mshop.mscanmc18.di.module;


import com.mscanmc.mshop.mscanmc18.di.scopes.ForActivity;
import com.mscanmc.mshop.mscanmc18.ui.MainActivity;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseActivity;
import com.mscanmc.mshop.mscanmc18.ui.splash.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Module
public abstract class ActivityModule {

    @ForActivity
    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract MainActivity provideMain();

    @ForActivity
    @ContributesAndroidInjector
    abstract BaseActivity provideBase();

    @ForActivity
    @ContributesAndroidInjector
    abstract SplashActivity provideSplash();
}
