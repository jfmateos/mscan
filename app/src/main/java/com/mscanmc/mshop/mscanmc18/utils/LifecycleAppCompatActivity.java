package com.mscanmc.mshop.mscanmc18.utils;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.support.v7.app.AppCompatActivity;

import com.mscanmc.mshop.mscanmc18.ui.base.BaseActivity;

/**
 * Created by Juan Francisco Mateos Redondo
 */
public abstract class LifecycleAppCompatActivity extends AppCompatActivity
        implements LifecycleRegistryOwner {

    LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

}
