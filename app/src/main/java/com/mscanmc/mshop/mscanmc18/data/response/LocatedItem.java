package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocatedItem {

    @SerializedName("familyCode")
    @Expose
    private String familyCode;
    @SerializedName("x")
    @Expose
    private double x;
    @SerializedName("y")
    @Expose
    private double y;
    @SerializedName("aisle")
    @Expose
    private double aisle;
    @SerializedName("nbItems")
    @Expose
    private double nbItems;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("items")
    @Expose
    private List<Item_> items = null;

    /**
     * No args constructor for use in serialization
     */
    public LocatedItem() {
    }

    /**
     * @param floor
     * @param items
     * @param aisle
     * @param familyCode
     * @param nbItems
     * @param y
     * @param x
     */
    public LocatedItem(String familyCode, double x, double y, double aisle, double nbItems, String floor, List<Item_> items) {
        super();
        this.familyCode = familyCode;
        this.x = x;
        this.y = y;
        this.aisle = aisle;
        this.nbItems = nbItems;
        this.floor = floor;
        this.items = items;
    }

    public String getFamilyCode() {
        return familyCode;
    }

    public void setFamilyCode(String familyCode) {
        this.familyCode = familyCode;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getAisle() {
        return aisle;
    }

    public void setAisle(double aisle) {
        this.aisle = aisle;
    }

    public double getNbItems() {
        return nbItems;
    }

    public void setNbItems(double nbItems) {
        this.nbItems = nbItems;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public List<Item_> getItems() {
        return items;
    }

    public void setItems(List<Item_> items) {
        this.items = items;
    }

}