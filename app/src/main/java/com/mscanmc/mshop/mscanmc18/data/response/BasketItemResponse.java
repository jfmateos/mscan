package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BasketItemResponse {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("requiresAssistance")
    @Expose
    private String requiresAssistance;
    @SerializedName("itemDiscount")
    @Expose
    private String itemDiscount;
    @SerializedName("itemPointsOffer")
    @Expose
    private String itemPointsOffer;
    @SerializedName("discountPercent")
    @Expose
    private String discountPercent;
    @SerializedName("grammes")
    @Expose
    private String grammes;
    @SerializedName("unitOriginalPrice")
    @Expose
    private String unitOriginalPrice;
    @SerializedName("unitScannedPrice")
    @Expose
    private double unitScannedPrice;
    @SerializedName("totalScannedPrice")
    @Expose
    private double totalScannedPrice;
    @SerializedName("unitDiscountedPrice")
    @Expose
    private String unitDiscountedPrice;
    @SerializedName("totalDiscountedPrice")
    @Expose
    private String totalDiscountedPrice;
    @SerializedName("finalPrice")
    @Expose
    private double finalPrice;
    @SerializedName("productInfoFull")
    @Expose
    private ProductInfoFull productInfoFull;

    /**
     * No args constructor for use in serialization
     *
     */
    public BasketItemResponse() {
    }

    public BasketItemResponse(String id, Product product, int quantity) {
        this.id = id;
        this.product = product;
        this.quantity = quantity;
    }

    /**
     *
     * @param unitDiscountedPrice
     * @param itemPointsOffer
     * @param requiresAssistance
     * @param unitScannedPrice
     * @param grammes
     * @param finalPrice
     * @param totalDiscountedPrice
     * @param totalScannedPrice
     * @param discount
     * @param productInfoFull
     * @param id
     * @param product
     * @param price
     * @param unitOriginalPrice
     * @param quantity
     * @param itemDiscount
     * @param discountPercent
     */
    public BasketItemResponse(String id, Product product, int quantity, String price, String discount, String requiresAssistance, String itemDiscount, String itemPointsOffer, String discountPercent, String grammes, String unitOriginalPrice, double unitScannedPrice, double totalScannedPrice, String unitDiscountedPrice, String totalDiscountedPrice, double finalPrice, ProductInfoFull productInfoFull) {
        super();
        this.id = id;
        this.product = product;
        this.quantity = quantity;
        this.price = price;
        this.discount = discount;
        this.requiresAssistance = requiresAssistance;
        this.itemDiscount = itemDiscount;
        this.itemPointsOffer = itemPointsOffer;
        this.discountPercent = discountPercent;
        this.grammes = grammes;
        this.unitOriginalPrice = unitOriginalPrice;
        this.unitScannedPrice = unitScannedPrice;
        this.totalScannedPrice = totalScannedPrice;
        this.unitDiscountedPrice = unitDiscountedPrice;
        this.totalDiscountedPrice = totalDiscountedPrice;
        this.finalPrice = finalPrice;
        this.productInfoFull = productInfoFull;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getRequiresAssistance() {
        return requiresAssistance;
    }

    public void setRequiresAssistance(String requiresAssistance) {
        this.requiresAssistance = requiresAssistance;
    }

    public String getItemDiscount() {
        return itemDiscount;
    }

    public void setItemDiscount(String itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    public String getItemPointsOffer() {
        return itemPointsOffer;
    }

    public void setItemPointsOffer(String itemPointsOffer) {
        this.itemPointsOffer = itemPointsOffer;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getGrammes() {
        return grammes;
    }

    public void setGrammes(String grammes) {
        this.grammes = grammes;
    }

    public String getUnitOriginalPrice() {
        return unitOriginalPrice;
    }

    public void setUnitOriginalPrice(String unitOriginalPrice) {
        this.unitOriginalPrice = unitOriginalPrice;
    }

    public double getUnitScannedPrice() {
        return unitScannedPrice;
    }

    public void setUnitScannedPrice(double unitScannedPrice) {
        this.unitScannedPrice = unitScannedPrice;
    }

    public double getTotalScannedPrice() {
        return totalScannedPrice;
    }

    public void setTotalScannedPrice(double totalScannedPrice) {
        this.totalScannedPrice = totalScannedPrice;
    }

    public String getUnitDiscountedPrice() {
        return unitDiscountedPrice;
    }

    public void setUnitDiscountedPrice(String unitDiscountedPrice) {
        this.unitDiscountedPrice = unitDiscountedPrice;
    }

    public String getTotalDiscountedPrice() {
        return totalDiscountedPrice;
    }

    public void setTotalDiscountedPrice(String totalDiscountedPrice) {
        this.totalDiscountedPrice = totalDiscountedPrice;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public ProductInfoFull getProductInfoFull() {
        return productInfoFull;
    }

    public void setProductInfoFull(ProductInfoFull productInfoFull) {
        this.productInfoFull = productInfoFull;
    }
}
