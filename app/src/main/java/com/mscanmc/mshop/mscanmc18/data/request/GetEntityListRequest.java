package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetEntityListRequest {

    @SerializedName("shopperCtx")
    @Expose
    private String shopperCtx;
    @SerializedName("storeId")
    @Expose
    private int storeId;
    @SerializedName("shoppingListId")
    @Expose
    private int shoppingListId;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetEntityListRequest() {
    }

    /**
     *
     * @param shopperCtx
     * @param shoppingListId
     * @param storeId
     */
    public GetEntityListRequest(String shopperCtx, int storeId, int shoppingListId) {
        super();
        this.shopperCtx = shopperCtx;
        this.storeId = storeId;
        this.shoppingListId = shoppingListId;
    }

    /**
     *
     * @param shopperCtx
     * @param storeId
     */
    public GetEntityListRequest(String shopperCtx, int storeId) {
        super();
        this.shopperCtx = shopperCtx;
        this.storeId = storeId;
    }

    public String getShopperCtx() {
        return shopperCtx;
    }

    public void setShopperCtx(String shopperCtx) {
        this.shopperCtx = shopperCtx;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(int shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

}