package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetEntityResponse {

    @SerializedName("entitiesList")
    @Expose
    private List<EntityResponse> entitiesList = null;

    /**
     * No args constructor for use in serialization
     */
    public GetEntityResponse() {
    }

    /**
     * @param entitiesList
     */
    public GetEntityResponse(List<EntityResponse> entitiesList) {
        super();
        this.entitiesList = entitiesList;
    }

    public List<EntityResponse> getEntitiesList() {
        return entitiesList;
    }

    public void setEntitiesList(List<EntityResponse> entitiesList) {
        this.entitiesList = entitiesList;
    }
}

