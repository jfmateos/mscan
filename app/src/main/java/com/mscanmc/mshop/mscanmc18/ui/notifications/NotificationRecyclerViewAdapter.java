package com.mscanmc.mshop.mscanmc18.ui.notifications;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.response.ShopperEvent;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentNotificationItemBinding;
import com.mscanmc.mshop.mscanmc18.ui.notifications.NotificationFragment.OnListFragmentInteractionListener;
import com.mscanmc.mshop.mscanmc18.utils.Constants;

import java.util.List;

public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<NotificationRecyclerViewAdapter.ViewHolder> {

    private final List<ShopperEvent> mValues;
    private final OnListFragmentInteractionListener mListener;
    FragmentNotificationItemBinding mBinding;

    public NotificationRecyclerViewAdapter(List<ShopperEvent> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        mBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_notification_item, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.binding.executePendingBindings();
        if (mValues.get(position).getShortcut().equalsIgnoreCase(Constants.OFFERS)) {
            mBinding.setIsOffer(true);
            mBinding.setOffer(mValues.get(position));

        } else {
            String titleComponents[] = mValues.get(position).getElementTitle().split("\n");
            mBinding.setIsOffer(false);
            mBinding.setShopperEvent(mValues.get(position));
        }
    }

    public void updateList(List<ShopperEvent> items) {
        mValues.clear();
        mValues.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        FragmentNotificationItemBinding binding;

        public ViewHolder(FragmentNotificationItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
