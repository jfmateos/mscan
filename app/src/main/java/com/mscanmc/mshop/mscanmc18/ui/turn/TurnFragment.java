package com.mscanmc.mshop.mscanmc18.ui.turn;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.data.request.EntityDetailRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TurnRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TurnRequestAzure;
import com.mscanmc.mshop.mscanmc18.data.request.TurnResponse;
import com.mscanmc.mshop.mscanmc18.data.response.EntityDeskList;
import com.mscanmc.mshop.mscanmc18.data.response.EntityDetailResponse;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentTurnListBinding;
import com.mscanmc.mshop.mscanmc18.domain.service.AzureInstance;
import com.mscanmc.mshop.mscanmc18.domain.service.AzureService;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseActivity;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseFragment;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseNavigation;
import com.mscanmc.mshop.mscanmc18.ui.base.ViewModelFactory;
import com.mscanmc.mshop.mscanmc18.utils.DataUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class TurnFragment extends BaseFragment implements HasSupportFragmentInjector, MyTurnRecyclerViewAdapter.RequestTurn {
    private AndroidInjector<Fragment> androidInjector;
    private static final String DESK_LIST = "desk_list";
    private int mColumnCount = 1;
    private RequestTurn mListener;
    MyTurnRecyclerViewAdapter.RequestTurn mRequestTurn;
    @Inject
    protected BaseNavigation navigation;
    @Inject
    protected ViewModelFactory viewModelFactory;
    @Inject
    protected BaseNavigation baseNavigation;
    private TurnViewModel mViewModel;
    @Inject
    protected PreferencesHelper preferencesHelper;
    FragmentTurnListBinding mBinding;
    List<EntityDeskList> turnList = new ArrayList<>();
    BaseActivity activity;

    public TurnFragment() {
    }

    //TODO cuando se quite el harcodeo del Main Activity que lanza esta actividad, ver cuál de los dos newInstance vale
  /*  public static TurnFragment newInstance(List<DeskList> deskLists) {
        TurnFragment fragment = new TurnFragment();
        Bundle args = new Bundle();
        args.putSerializable(DESK_LIST, (Serializable) deskLists);
        fragment.setArguments(args);
        return fragment;
    }*/
    public static TurnFragment newInstance(List<EntityDeskList> deskLists) {
        TurnFragment fragment = new TurnFragment();
        Bundle args = new Bundle();
        args.putSerializable(DESK_LIST, (Serializable) deskLists);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = (BaseActivity) getActivity();
        if (getArguments() != null) {
            turnList = (List<EntityDeskList>) getArguments().getSerializable(DESK_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_turn_list, container, false);
        mViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(TurnViewModel.class);
        mBinding.turnList.setLayoutManager(new LinearLayoutManager(mBinding.getRoot().getContext()));
        mRequestTurn = this;
        mBinding.turnList.setAdapter(new MyTurnRecyclerViewAdapter(turnList, mRequestTurn));
        mBinding.setLastUpdating(DataUtil.getCurrentTime());
        mBinding.setStoreName(preferencesHelper.getStoreResponse(getContext()).getStore().getLabel());
        mBinding.nestedTurn.setColorSchemeResources(
                R.color.primary_dark,
                R.color.green,
                R.color.primary);
        mBinding.nestedTurn.setOnRefreshListener(() -> {
            getCustomerTurns();
            mBinding.nestedTurn.setRefreshing(false);
        });
        return mBinding.getRoot();
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        if (context instanceof MyTurnRecyclerViewAdapter.RequestTurn) {
            mRequestTurn = (MyTurnRecyclerViewAdapter.RequestTurn) context;

        } else if (context instanceof RequestTurn) {
            mListener = (RequestTurn) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return androidInjector;
    }


    @Override
    public void requestTurn(String deskCode, int item) {
        mBinding.setIsLoading(true);
        mViewModel.getTurnRequest(new TurnRequest(
                preferencesHelper.getShopperCtxt(getContext()),
                preferencesHelper.getStoreId(),
                deskCode,
                String.valueOf(item)))
                .observe(this, deskListGeneric -> {
                    if (deskListGeneric != null) {
                        mBinding.setIsLoading(true);
                        // mBinding.turnList.setAdapter(new MyTurnRecyclerViewAdapter(turnList, mRequestTurn));
                    }
                });
    }


    //TODO llamada harcodeada para la demo Octubre 2018
    @Override
    public void getTurn(EntityDeskList entityDeskList, int selectedItemPosition) {
        mBinding.setIsLoading(true);
        TurnRequestAzure turnRequestAzure =
                new TurnRequestAzure(preferencesHelper.getAccessToken(), preferencesHelper.getSource(), 1, entityDeskList.getIdDesk(), preferencesHelper.getUniqueId(getActivity()), selectedItemPosition, "");
        AzureService azureService = AzureInstance.getRetrofitInstance().create(AzureService.class);
        azureService.getTurn(turnRequestAzure).enqueue(new Callback<TurnResponse>() {
            @Override
            public void onResponse(
                    Call<TurnResponse> call,
                    Response<TurnResponse> response) {
                mBinding.setIsLoading(false);
                if (response.isSuccessful()) {
                    entityDeskList.setMyTurn(response.body().getTurn());
                    //getReceipesListDataResponseMutableLiveData.setValue(response.body());
                    showYourTurn(entityDeskList);

                } else {
                    Timber.e("code: %s", response.code());
                }
            }

            @Override
            public void onFailure(
                    Call<TurnResponse> call, Throwable t) {
                Timber.wtf(t.getMessage());
                //getReceipesListDataResponseMutableLiveData.setValue(null);
            }
        });
    }

    //TODO llamada harcodeada para la demo Octubre 2018
    public void getCustomerTurns() {
        EntityDetailRequest entityDetailRequest =
                new EntityDetailRequest(preferencesHelper.getAccessToken(), preferencesHelper.getSource(), 1, "es_ES", 0);
        AzureService azureService = AzureInstance.getRetrofitInstance().create(AzureService.class);
        azureService.getEntityDetail(entityDetailRequest).enqueue(new Callback<EntityDetailResponse>() {
            @Override
            public void onResponse(
                    Call<EntityDetailResponse> call,
                    Response<EntityDetailResponse> response) {
                if (response.isSuccessful()) {
                    //getReceipesListDataResponseMutableLiveData.setValue(response.body());
                    // getCustomerTurns();
                    turnList.clear();
                    turnList.addAll(response.body().getEntityDeskList());

                    baseNavigation.showTurn(activity, turnList);

                } else {
                    Timber.e("code: %s", response.code());
                }
            }

            @Override
            public void onFailure(
                    Call<EntityDetailResponse> call, Throwable t) {
                Timber.wtf(t.getMessage());
                //getReceipesListDataResponseMutableLiveData.setValue(null);
            }
        });
    }

    private void showYourTurn(EntityDeskList deskList) {
        LayoutInflater layoutInflater = LayoutInflater.from(mBinding.getRoot().getContext());

        View promptView = layoutInflater.inflate(R.layout.dialog_taken_turn, null);
        final AlertDialog mAlertDialog =
                new AlertDialog.Builder(mBinding.getRoot().getContext()).create();
        AppCompatImageView imageView = (AppCompatImageView) promptView.findViewById(R.id.ic_close_taken_turn);
        AppCompatTextView title = (AppCompatTextView) promptView.findViewById(R.id.tv_take_turn_desk);
        AppCompatTextView turn = (AppCompatTextView) promptView.findViewById(R.id.tv_take_turn_number);
        AppCompatTextView date = (AppCompatTextView) promptView.findViewById(R.id.tv_date);
        AppCompatTextView hour = (AppCompatTextView) promptView.findViewById(R.id.tv_hour);
        title.setText(deskList.getDeskName());
        turn.setText(deskList.getMyTurn().replaceFirst("^0+(?!$)", ""));
        date.setText(DataUtil.getCurrentDate());
        hour.setText(DataUtil.getCurrentTime());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
                getCustomerTurns();
            }
        });
        mAlertDialog.setView(promptView);
        mAlertDialog.show();
    }

    public interface RequestTurn {
        void requestTurn(String deskCode, int item);
    }
}
