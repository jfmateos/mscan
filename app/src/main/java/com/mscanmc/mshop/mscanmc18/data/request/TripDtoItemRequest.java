package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripDtoItemRequest {

    @SerializedName("codeType")
    @Expose
    private String codeType;

    @SerializedName("codeValue")
    @Expose
    private String codeValue;

    @SerializedName("quantity")
    @Expose
    private int quantity;

    @SerializedName("itemId")
    @Expose
    private String itemId;

    /** No args constructor for use in serialization */
    public TripDtoItemRequest() {}

    /**
     * @param codeValue
     * @param quantity
     * @param codeType
     * @param itemId
     */
    public TripDtoItemRequest(String codeType, String codeValue, int quantity, String itemId) {
        super();
        this.codeType = codeType;
        this.codeValue = codeValue;
        this.quantity = quantity;
        this.itemId = itemId;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
