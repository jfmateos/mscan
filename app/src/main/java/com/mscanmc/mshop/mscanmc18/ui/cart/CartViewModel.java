package com.mscanmc.mshop.mscanmc18.ui.cart;

import android.arch.lifecycle.MutableLiveData;

import com.mscanmc.mshop.mscanmc18.data.request.GetEntityListRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoItemInitialRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoItemRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoRequest;
import com.mscanmc.mshop.mscanmc18.data.response.ProductQuantityResponse;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListLastShopping;
import com.mscanmc.mshop.mscanmc18.domain.repository.MScanRepository;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CartViewModel extends BaseViewModel {
    MScanRepository repository;
    private MutableLiveData<TripDtoItemRequest> tripDtoRequest = new MutableLiveData<>();

    @Inject
    public CartViewModel(MScanRepository repository) {

        this.repository = repository;
    }


    public Observable<ProductQuantityResponse> setProductQuantity(
            TripDtoRequest tripSetProductListDto) {
        return repository.setCart(tripSetProductListDto);

    }

    public Observable<ProductQuantityResponse> removeItem(
            TripDtoRequest tripSetProductListDto) {
        return repository.removeItem(tripSetProductListDto);

    }


    public void unsuscribeAll() {
        tripDtoRequest.setValue(null);
    }

    public void setTripDtoRequest(TripDtoItemRequest tripDtoRequest) {
        this.tripDtoRequest.setValue(tripDtoRequest);

    }


    public MutableLiveData<TripDtoItemRequest> getTriDto() {
        return this.tripDtoRequest;
    }

    public Observable<ProductQuantityResponse> getInitialCart(TripDtoItemInitialRequest tripDtoItemInitialRequest) {
        return repository.getInitCart(tripDtoItemInitialRequest);
    }


    public Observable<ShoppingListLastShopping> getShoppingListDetail(
            GetEntityListRequest entityListRequest) {
        return repository.getShoppingListDetail(entityListRequest);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        tripDtoRequest.setValue(null);
    }
}
