package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShopperScanRequest {

    @SerializedName("encodedId")
    @Expose
    private String encodedId;
    @SerializedName("storeId")
    @Expose
    private int storeId;
    @SerializedName("terminalUniqueId")
    @Expose
    private String terminalUniqueId;

    /**
     * No args constructor for use in serialization
     *
     */
    public ShopperScanRequest() {
    }

    /**
     *
     * @param encodedId
     * @param terminalUniqueId
     * @param storeId
     */
    public ShopperScanRequest(String encodedId, int storeId, String terminalUniqueId) {
        super();
        this.encodedId = encodedId;
        this.storeId = storeId;
        this.terminalUniqueId = terminalUniqueId;
    }

    public String getEncodedId() {
        return encodedId;
    }

    public void setEncodedId(String encodedId) {
        this.encodedId = encodedId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getTerminalUniqueId() {
        return terminalUniqueId;
    }

    public void setTerminalUniqueId(String terminalUniqueId) {
        this.terminalUniqueId = terminalUniqueId;
    }

}