package com.mscanmc.mshop.mscanmc18.di.module;

import android.app.Application;
import android.view.LayoutInflater;

import com.mscanmc.mshop.mscanmc18.di.qualifier.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Module
public class AndroidModule {
    @Provides
    public LayoutInflater provideLayoutInflater(@ActivityScope Application application) {
        return LayoutInflater.from(application);
    }
}
