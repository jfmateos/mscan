package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("code")
    @Expose
    private Code code;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("needUnit")
    @Expose
    private String needUnit;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("requiresAssistance")
    @Expose
    private String requiresAssistance;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;

    /**
     * No args constructor for use in serialization
     */
    public Product() {
    }

    public Product(Code code, String label) {
        this.code = code;
        this.label = label;
    }

    /**
     * @param needUnit
     * @param price
     * @param imageUrl
     * @param requiresAssistance
     * @param department
     * @param label
     * @param code
     */
    public Product(Code code, String price, String needUnit, String department, String requiresAssistance, String label, String imageUrl) {
        super();
        this.code = code;
        this.price = price;
        this.needUnit = needUnit;
        this.department = department;
        this.requiresAssistance = requiresAssistance;
        this.label = label;
        this.imageUrl = imageUrl;
    }

    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNeedUnit() {
        return needUnit;
    }

    public void setNeedUnit(String needUnit) {
        this.needUnit = needUnit;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getRequiresAssistance() {
        return requiresAssistance;
    }

    public void setRequiresAssistance(String requiresAssistance) {
        this.requiresAssistance = requiresAssistance;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
