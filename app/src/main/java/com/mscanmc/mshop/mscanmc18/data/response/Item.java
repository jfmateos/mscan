package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("listId")
    @Expose
    private String listId;
    @SerializedName("itemType")
    @Expose
    private String itemType;
    @SerializedName("barcode")
    @Expose
    private Barcode barcode;
    @SerializedName("freeform")
    @Expose
    private String freeform;
    @SerializedName("tradename")
    @Expose
    private String tradename;
    @SerializedName("needsUnit")
    @Expose
    private String needsUnit;
    @SerializedName("checked")
    @Expose
    private String checked;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("productInfoFull")
    @Expose
    private ProductInfoFull productInfoFull;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("quantity")
    @Expose
    private int quantity;

    /**
     * No args constructor for use in serialization
     */
    public Item() {
    }

    /**
     * @param productInfoFull
     * @param freeform
     * @param category
     * @param needsUnit
     * @param tradename
     * @param listId
     * @param department
     * @param itemType
     * @param barcode
     * @param quantity
     * @param checked
     */
    public Item(String listId, String itemType, Barcode barcode, String freeform, String tradename, String needsUnit, String checked, String department, ProductInfoFull productInfoFull, String category, int quantity) {
        super();
        this.listId = listId;
        this.itemType = itemType;
        this.barcode = barcode;
        this.freeform = freeform;
        this.tradename = tradename;
        this.needsUnit = needsUnit;
        this.checked = checked;
        this.department = department;
        this.productInfoFull = productInfoFull;
        this.category = category;
        this.quantity = quantity;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    public void setBarcode(Barcode barcode) {
        this.barcode = barcode;
    }

    public String getFreeform() {
        return freeform;
    }

    public void setFreeform(String freeform) {
        this.freeform = freeform;
    }

    public String getTradename() {
        return tradename;
    }

    public void setTradename(String tradename) {
        this.tradename = tradename;
    }

    public String getNeedsUnit() {
        return needsUnit;
    }

    public void setNeedsUnit(String needsUnit) {
        this.needsUnit = needsUnit;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public ProductInfoFull getProductInfoFull() {
        return productInfoFull;
    }

    public void setProductInfoFull(ProductInfoFull productInfoFull) {
        this.productInfoFull = productInfoFull;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
