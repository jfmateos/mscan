package com.mscanmc.mshop.mscanmc18.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mscanmc.mshop.mscanmc18.utils.Constants;

import java.util.Date;

import timber.log.Timber;

public class PushReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String notificationTitle = "mScan";
        String notificationText = "New notification";
        String messageType = "";
        String encodedId = "";
        if (intent.getStringExtra("message") != null) {

        }
        notificationText = intent.getStringExtra("message");
        messageType = intent.getStringExtra("messageType");
        encodedId = intent.getStringExtra("encodedId");
        final Intent i = new Intent(Constants.BROADCAST_PUSH);
        i.putExtra(Constants.BROADCAST_PUSH_MESSAGE, messageType);
        i.putExtra(Constants.BROADCAST_PUSH_ID, new Date());
        i.putExtra(Constants.BROADCAST_PUSH_ENCODED_ID, encodedId);
        context.sendBroadcast(i);
    }
}