package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductInfoFull_ {

    @SerializedName("codeValue")
    @Expose
    private String codeValue;
    @SerializedName("codeType")
    @Expose
    private String codeType;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("unitPrice")
    @Expose
    private double unitPrice;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("unitType")
    @Expose
    private String unitType;
    @SerializedName("productReference")
    @Expose
    private String productReference;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("ingredient")
    @Expose
    private String ingredient;
    @SerializedName("tradename")
    @Expose
    private String tradename;
    @SerializedName("descriptionShort")
    @Expose
    private String descriptionShort;
    @SerializedName("infoNutricional")
    @Expose
    private String infoNutricional;
    @SerializedName("family")
    @Expose
    private Family_ family;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("imageCode")
    @Expose
    private String imageCode;
    @SerializedName("imageInfo1")
    @Expose
    private String imageInfo1;
    @SerializedName("imageInfo2")
    @Expose
    private String imageInfo2;
    @SerializedName("imageInfo3")
    @Expose
    private String imageInfo3;
    @SerializedName("imageInfo4")
    @Expose
    private String imageInfo4;
    @SerializedName("imageInfo5")
    @Expose
    private String imageInfo5;
    @SerializedName("imageInfo6")
    @Expose
    private String imageInfo6;
    @SerializedName("imageSmall")
    @Expose
    private String imageSmall;
    @SerializedName("imageList")
    @Expose
    private List<String> imageList = null;
    @SerializedName("storeId")
    @Expose
    private double storeId;
    @SerializedName("storeLabel")
    @Expose
    private String storeLabel;
    @SerializedName("aisleNb")
    @Expose
    private double aisleNb;
    @SerializedName("x")
    @Expose
    private double x;
    @SerializedName("y")
    @Expose
    private double y;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("familyCode")
    @Expose
    private String familyCode;
    @SerializedName("familyDescription")
    @Expose
    private String familyDescription;
    @SerializedName("categoryCode")
    @Expose
    private String categoryCode;
    @SerializedName("categoryDescription")
    @Expose
    private String categoryDescription;
    @SerializedName("marketCode")
    @Expose
    private String marketCode;
    @SerializedName("marketDescription")
    @Expose
    private String marketDescription;
    @SerializedName("catalogGroupCode")
    @Expose
    private String catalogGroupCode;
    @SerializedName("catalogGroupDescription")
    @Expose
    private String catalogGroupDescription;
    @SerializedName("mapGroupCode")
    @Expose
    private String mapGroupCode;
    @SerializedName("mapGroupDescription")
    @Expose
    private String mapGroupDescription;
    @SerializedName("urlShare")
    @Expose
    private String urlShare;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("frozen")
    @Expose
    private String frozen;
    @SerializedName("ecological")
    @Expose
    private String ecological;
    @SerializedName("health")
    @Expose
    private String health;
    @SerializedName("favouriteProduct")
    @Expose
    private String favouriteProduct;
    @SerializedName("controlledProduction")
    @Expose
    private String controlledProduction;
    @SerializedName("self")
    @Expose
    private String self;
    @SerializedName("white")
    @Expose
    private String white;
    @SerializedName("refrigerated")
    @Expose
    private String refrigerated;
    @SerializedName("variableWeight")
    @Expose
    private String variableWeight;
    @SerializedName("alcohol")
    @Expose
    private String alcohol;
    @SerializedName("first")
    @Expose
    private String first;

    /**
     * No args constructor for use in serialization
     */
    public ProductInfoFull_() {
    }

    /**
     * @param refrigerated
     * @param urlShare
     * @param unitPrice
     * @param marketDescription
     * @param imageSmall
     * @param catalogGroupCode
     * @param tradename
     * @param favouriteProduct
     * @param family
     * @param description
     * @param ingredient
     * @param catalogGroupDescription
     * @param marketCode
     * @param ecological
     * @param unitType
     * @param white
     * @param categoryCode
     * @param label
     * @param storeLabel
     * @param health
     * @param discount
     * @param imageInfo1
     * @param imageInfo2
     * @param imageInfo3
     * @param imageList
     * @param imageInfo4
     * @param price
     * @param imageInfo5
     * @param floor
     * @param imageInfo6
     * @param alcohol
     * @param active
     * @param productReference
     * @param storeId
     * @param controlledProduction
     * @param self
     * @param imageCode
     * @param frozen
     * @param mapGroupCode
     * @param codeValue
     * @param variableWeight
     * @param descriptionShort
     * @param aisleNb
     * @param infoNutricional
     * @param familyDescription
     * @param categoryDescription
     * @param familyCode
     * @param codeType
     * @param mapGroupDescription
     * @param first
     * @param productName
     * @param y
     * @param x
     */
    public ProductInfoFull_(String codeValue, String codeType, String label, double unitPrice, double price, String unitType, String productReference, String productName, String description, String ingredient, String tradename, String descriptionShort, String infoNutricional, Family_ family, String discount, String imageCode, String imageInfo1, String imageInfo2, String imageInfo3, String imageInfo4, String imageInfo5, String imageInfo6, String imageSmall, List<String> imageList, double storeId, String storeLabel, double aisleNb, double x, double y, String floor, String familyCode, String familyDescription, String categoryCode, String categoryDescription, String marketCode, String marketDescription, String catalogGroupCode, String catalogGroupDescription, String mapGroupCode, String mapGroupDescription, String urlShare, String active, String frozen, String ecological, String health, String favouriteProduct, String controlledProduction, String self, String white, String refrigerated, String variableWeight, String alcohol, String first) {
        super();
        this.codeValue = codeValue;
        this.codeType = codeType;
        this.label = label;
        this.unitPrice = unitPrice;
        this.price = price;
        this.unitType = unitType;
        this.productReference = productReference;
        this.productName = productName;
        this.description = description;
        this.ingredient = ingredient;
        this.tradename = tradename;
        this.descriptionShort = descriptionShort;
        this.infoNutricional = infoNutricional;
        this.family = family;
        this.discount = discount;
        this.imageCode = imageCode;
        this.imageInfo1 = imageInfo1;
        this.imageInfo2 = imageInfo2;
        this.imageInfo3 = imageInfo3;
        this.imageInfo4 = imageInfo4;
        this.imageInfo5 = imageInfo5;
        this.imageInfo6 = imageInfo6;
        this.imageSmall = imageSmall;
        this.imageList = imageList;
        this.storeId = storeId;
        this.storeLabel = storeLabel;
        this.aisleNb = aisleNb;
        this.x = x;
        this.y = y;
        this.floor = floor;
        this.familyCode = familyCode;
        this.familyDescription = familyDescription;
        this.categoryCode = categoryCode;
        this.categoryDescription = categoryDescription;
        this.marketCode = marketCode;
        this.marketDescription = marketDescription;
        this.catalogGroupCode = catalogGroupCode;
        this.catalogGroupDescription = catalogGroupDescription;
        this.mapGroupCode = mapGroupCode;
        this.mapGroupDescription = mapGroupDescription;
        this.urlShare = urlShare;
        this.active = active;
        this.frozen = frozen;
        this.ecological = ecological;
        this.health = health;
        this.favouriteProduct = favouriteProduct;
        this.controlledProduction = controlledProduction;
        this.self = self;
        this.white = white;
        this.refrigerated = refrigerated;
        this.variableWeight = variableWeight;
        this.alcohol = alcohol;
        this.first = first;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getProductReference() {
        return productReference;
    }

    public void setProductReference(String productReference) {
        this.productReference = productReference;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getTradename() {
        return tradename;
    }

    public void setTradename(String tradename) {
        this.tradename = tradename;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getInfoNutricional() {
        return infoNutricional;
    }

    public void setInfoNutricional(String infoNutricional) {
        this.infoNutricional = infoNutricional;
    }

    public Family_ getFamily() {
        return family;
    }

    public void setFamily(Family_ family) {
        this.family = family;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getImageCode() {
        return imageCode;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }

    public String getImageInfo1() {
        return imageInfo1;
    }

    public void setImageInfo1(String imageInfo1) {
        this.imageInfo1 = imageInfo1;
    }

    public String getImageInfo2() {
        return imageInfo2;
    }

    public void setImageInfo2(String imageInfo2) {
        this.imageInfo2 = imageInfo2;
    }

    public String getImageInfo3() {
        return imageInfo3;
    }

    public void setImageInfo3(String imageInfo3) {
        this.imageInfo3 = imageInfo3;
    }

    public String getImageInfo4() {
        return imageInfo4;
    }

    public void setImageInfo4(String imageInfo4) {
        this.imageInfo4 = imageInfo4;
    }

    public String getImageInfo5() {
        return imageInfo5;
    }

    public void setImageInfo5(String imageInfo5) {
        this.imageInfo5 = imageInfo5;
    }

    public String getImageInfo6() {
        return imageInfo6;
    }

    public void setImageInfo6(String imageInfo6) {
        this.imageInfo6 = imageInfo6;
    }

    public String getImageSmall() {
        return imageSmall;
    }

    public void setImageSmall(String imageSmall) {
        this.imageSmall = imageSmall;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public double getStoreId() {
        return storeId;
    }

    public void setStoreId(double storeId) {
        this.storeId = storeId;
    }

    public String getStoreLabel() {
        return storeLabel;
    }

    public void setStoreLabel(String storeLabel) {
        this.storeLabel = storeLabel;
    }

    public double getAisleNb() {
        return aisleNb;
    }

    public void setAisleNb(double aisleNb) {
        this.aisleNb = aisleNb;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getFamilyCode() {
        return familyCode;
    }

    public void setFamilyCode(String familyCode) {
        this.familyCode = familyCode;
    }

    public String getFamilyDescription() {
        return familyDescription;
    }

    public void setFamilyDescription(String familyDescription) {
        this.familyDescription = familyDescription;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getMarketCode() {
        return marketCode;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public String getMarketDescription() {
        return marketDescription;
    }

    public void setMarketDescription(String marketDescription) {
        this.marketDescription = marketDescription;
    }

    public String getCatalogGroupCode() {
        return catalogGroupCode;
    }

    public void setCatalogGroupCode(String catalogGroupCode) {
        this.catalogGroupCode = catalogGroupCode;
    }

    public String getCatalogGroupDescription() {
        return catalogGroupDescription;
    }

    public void setCatalogGroupDescription(String catalogGroupDescription) {
        this.catalogGroupDescription = catalogGroupDescription;
    }

    public String getMapGroupCode() {
        return mapGroupCode;
    }

    public void setMapGroupCode(String mapGroupCode) {
        this.mapGroupCode = mapGroupCode;
    }

    public String getMapGroupDescription() {
        return mapGroupDescription;
    }

    public void setMapGroupDescription(String mapGroupDescription) {
        this.mapGroupDescription = mapGroupDescription;
    }

    public String getUrlShare() {
        return urlShare;
    }

    public void setUrlShare(String urlShare) {
        this.urlShare = urlShare;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getFrozen() {
        return frozen;
    }

    public void setFrozen(String frozen) {
        this.frozen = frozen;
    }

    public String getEcological() {
        return ecological;
    }

    public void setEcological(String ecological) {
        this.ecological = ecological;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getFavouriteProduct() {
        return favouriteProduct;
    }

    public void setFavouriteProduct(String favouriteProduct) {
        this.favouriteProduct = favouriteProduct;
    }

    public String getControlledProduction() {
        return controlledProduction;
    }

    public void setControlledProduction(String controlledProduction) {
        this.controlledProduction = controlledProduction;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getWhite() {
        return white;
    }

    public void setWhite(String white) {
        this.white = white;
    }

    public String getRefrigerated() {
        return refrigerated;
    }

    public void setRefrigerated(String refrigerated) {
        this.refrigerated = refrigerated;
    }

    public String getVariableWeight() {
        return variableWeight;
    }

    public void setVariableWeight(String variableWeight) {
        this.variableWeight = variableWeight;
    }

    public String getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(String alcohol) {
        this.alcohol = alcohol;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

}