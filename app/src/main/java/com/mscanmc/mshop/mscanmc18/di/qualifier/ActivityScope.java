package com.mscanmc.mshop.mscanmc18.di.qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}
