package com.mscanmc.mshop.mscanmc18.ui.mylist;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.response.Historic;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentUserHistoricListItemBinding;

import java.util.List;

import timber.log.Timber;

public class MyhistoricListRecyclerViewAdapter
        extends RecyclerView.Adapter<MyhistoricListRecyclerViewAdapter.ViewHolder> {

    private final List<Historic> mValues;
    private final ShoppingListFragment.onListItemClick mListener;
    private final MyshoppingListRecyclerViewAdapter.OnMyListItemClick onMyListItemClick;

    FragmentUserHistoricListItemBinding mBinding;

    public MyhistoricListRecyclerViewAdapter(
            List<Historic> items, ShoppingListFragment.onListItemClick listener, MyshoppingListRecyclerViewAdapter.OnMyListItemClick onMyListItemClick) {
        mValues = items;
        mListener = listener;
        this.onMyListItemClick = onMyListItemClick;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        mBinding =
                DataBindingUtil.inflate(
                        layoutInflater, R.layout.fragment_user_historic_list_item, parent, false);
        return new ViewHolder(mBinding);
    }

    private void onClickUserNow(View view) {

        Timber.d("ñldjdñ");

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.binding.executePendingBindings();
        mBinding.setShoppingDetail(mValues.get(position));
        holder.binding.setOnClickUserNow(v -> {
            mListener.onHistoricListListener(mValues.get(position));
        });
        onClickUserNow(holder.binding.bUseNowHistoric);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void refreshData(List<Historic> historics) {
        this.mValues.clear();
        this.mValues.addAll(historics);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        FragmentUserHistoricListItemBinding binding;

        public ViewHolder(FragmentUserHistoricListItemBinding view) {
            super(view.getRoot());
            binding = view;
        }
    }
}
