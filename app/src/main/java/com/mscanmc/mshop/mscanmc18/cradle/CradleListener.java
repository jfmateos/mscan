package com.mscanmc.mshop.mscanmc18.cradle;

import android.provider.Settings;

import com.symbol.emdk.personalshopper.CradleException;

public interface CradleListener {
    void onCradleEmpty() throws Settings.SettingNotFoundException;
    void onCradleBusy() throws CradleException, Settings.SettingNotFoundException;
}