package com.mscanmc.mshop.mscanmc18.network;

public enum Status {
    LOADING,
    SUCCESS,
    ERROR,
    COMPLETED
}
