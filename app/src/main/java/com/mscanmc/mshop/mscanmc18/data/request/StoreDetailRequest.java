package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreDetailRequest {

    @SerializedName("shopperCtx")
    @Expose
    private String shopperCtx;
    @SerializedName("storeId")
    @Expose
    private int storeId;

    /**
     * No args constructor for use in serialization
     *
     */
    public StoreDetailRequest() {
    }

    /**
     *
     * @param shopperCtx
     * @param storeId
     */
    public StoreDetailRequest(String shopperCtx, int storeId) {
        super();
        this.shopperCtx = shopperCtx;
        this.storeId = storeId;
    }

    public String getShopperCtx() {
        return shopperCtx;
    }

    public void setShopperCtx(String shopperCtx) {
        this.shopperCtx = shopperCtx;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }
}
