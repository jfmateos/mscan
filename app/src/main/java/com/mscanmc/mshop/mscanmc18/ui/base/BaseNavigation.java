package com.mscanmc.mshop.mscanmc18.ui.base;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.response.DeskList;
import com.mscanmc.mshop.mscanmc18.data.response.EntityDeskList;
import com.mscanmc.mshop.mscanmc18.data.response.ShopperEvent;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingList;
import com.mscanmc.mshop.mscanmc18.di.scopes.ForActivity;
import com.mscanmc.mshop.mscanmc18.ui.MainActivity;
import com.mscanmc.mshop.mscanmc18.ui.cart.ProductFragment;
import com.mscanmc.mshop.mscanmc18.ui.cart.SavedListFragment;
import com.mscanmc.mshop.mscanmc18.ui.mylist.MyshoppingListRecyclerViewAdapter;
import com.mscanmc.mshop.mscanmc18.ui.mylist.ShoppingListFragment;
import com.mscanmc.mshop.mscanmc18.ui.notifications.NotificationFragment;
import com.mscanmc.mshop.mscanmc18.ui.splash.SplashActivity;
import com.mscanmc.mshop.mscanmc18.ui.turn.TurnFragment;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@ForActivity
public class BaseNavigation extends BaseActivity {
    @Inject
    public BaseNavigation() {
    }

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    public void startMainActivity(BaseActivity baseActivity) {
        MainActivity.start(baseActivity);
    }

    public void startSplashActivity(BaseActivity baseActivity) {
        SplashActivity.start(baseActivity);
    }

    public void showProductFragmentList(BaseActivity activity, int shoppingList) {
        fragmentManager = activity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.add(R.id.main_contain, ProductFragment.newInstance(shoppingList));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void showProductFragmentList(BaseActivity activity) {
        fragmentManager = activity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.add(R.id.main_contain, ProductFragment.newInstance(-1));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void showMyShoppingList(BaseActivity activity, int content, MyshoppingListRecyclerViewAdapter.OnMyListItemClick myListItemClick) {
        fragmentManager = activity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.add(content, ShoppingListFragment.newInstance(2, myListItemClick));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    //TODO esta debería ser la llamada cuando se quite el harcodeo que llama a la tienda desde el MAin
/*
    public void showTurn(BaseActivity activity, List<DeskList> turnList) {
        fragmentManager = activity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.add(R.id.main_contain, TurnFragment.newInstance(turnList));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }*/
 public void showTurn(BaseActivity activity, List<EntityDeskList> turnList) {
        fragmentManager = activity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.add(R.id.main_contain, TurnFragment.newInstance(turnList));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void showNotifications(BaseActivity activity, List<ShopperEvent> shopperEvents) {
        fragmentManager = activity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.add(R.id.main_contain, NotificationFragment.newInstance(shopperEvents));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void showNotifications(BaseActivity activity) {
        fragmentManager = activity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.add(R.id.main_contain, NotificationFragment.newInstance());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
