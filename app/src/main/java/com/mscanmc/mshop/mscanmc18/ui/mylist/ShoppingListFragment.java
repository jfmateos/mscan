package com.mscanmc.mshop.mscanmc18.ui.mylist;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.data.request.StoreDetailRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TripDtoItemRequest;
import com.mscanmc.mshop.mscanmc18.data.response.Historic;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListDetail;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentUserListBinding;
import com.mscanmc.mshop.mscanmc18.di.Injectable;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseFragment;
import com.mscanmc.mshop.mscanmc18.ui.base.ViewModelFactory;
import com.mscanmc.mshop.mscanmc18.ui.cart.ProductFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class ShoppingListFragment extends BaseFragment implements HasSupportFragmentInjector, Injectable, MyshoppingListRecyclerViewAdapter.OnMyListItemClick {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String TAB_MY_LIST = "my_list";
    private static final String TAB_HISTORIC_LIST = "historic_list";
    private AndroidInjector<Fragment> androidInjector;
    private onListItemClick mListener;
    FragmentUserListBinding mBinding;
    @Inject
    protected ViewModelFactory viewModelFactory;
    @Inject
    PreferencesHelper preferencesHelper;
    private ShoppingListViewModel mViewModel;
    MyshoppingListRecyclerViewAdapter.OnMyListItemClick myListItemClick;
    private List<Historic> listHistoric = new ArrayList<>();
    private List<ShoppingListDetail> listDetails = new ArrayList<>();
    private MyhistoricListRecyclerViewAdapter myHitoryAdapter;
    private MyshoppingListRecyclerViewAdapter myshoppinAdapter;


    public ShoppingListFragment() {
    }

    public static ShoppingListFragment newInstance(int columnCount, MyshoppingListRecyclerViewAdapter.OnMyListItemClick myListItemClick) {
        ShoppingListFragment fragment = new ShoppingListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user__list, container, false);
        mViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ShoppingListViewModel.class);
        mBinding.tbShoppingList.setTabMode(TabLayout.MODE_FIXED);
        mBinding.tbShoppingList.setTabGravity(TabLayout.GRAVITY_FILL);
        Objects.requireNonNull(mBinding.tbShoppingList.getTabAt(0)).setTag(TAB_MY_LIST);
        Objects.requireNonNull(mBinding.tbShoppingList.getTabAt(1)).setTag(TAB_HISTORIC_LIST);
        listDetails = new ArrayList<>();
        mBinding.rvUserList.setLayoutManager(new LinearLayoutManager(mBinding.getRoot().getContext()));
        myshoppinAdapter = new MyshoppingListRecyclerViewAdapter(listDetails, mListener, myListItemClick);
        mBinding.rvUserList.setAdapter(myshoppinAdapter);
        mBinding.rvUserHiistoricList.setLayoutManager(new LinearLayoutManager(mBinding.getRoot().getContext()));
        mBinding.setIsMyList(true);
        myHitoryAdapter = new MyhistoricListRecyclerViewAdapter(listHistoric, mListener, myListItemClick);
        mBinding.rvUserHiistoricList.setAdapter(myHitoryAdapter);
        getMyShoppingList();
        mBinding.tbShoppingList.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        if (tab.getTag().toString().equalsIgnoreCase(TAB_MY_LIST)) {
                            mBinding.setIsMyList(true);
                            getMyShoppingList();
                        } else if (tab.getTag().toString().equalsIgnoreCase(TAB_HISTORIC_LIST)) {
                            mBinding.setIsMyList(false);
                            getHistoricList();
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                    }
                });
        return mBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        if (context instanceof onListItemClick) {
            mListener = (onListItemClick) context;
        } else if (context instanceof MyshoppingListRecyclerViewAdapter.OnMyListItemClick) {
            myListItemClick = (MyshoppingListRecyclerViewAdapter.OnMyListItemClick) context;
        } else {

            throw new RuntimeException(
                    context.toString() + " must implement onItemAdapterIteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return androidInjector;
    }

    @Override
    public void onMyListitemClick(int shoppingList) {
        myListItemClick.onMyListitemClick(shoppingList);

    }

    public void getMyShoppingList() {
        //mBinding.setIsLoading(true);
        mViewModel.getMyShoppingList(new StoreDetailRequest(preferencesHelper.getShopperCtxt(getContext()), preferencesHelper.getStoreId()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(shoppingList -> {
                    getHistoricList();
                    mBinding.setIsLoading(false);
                    listDetails.clear();
                    if(shoppingList.getShoppingList().get(0).getLabel().toLowerCase().equalsIgnoreCase("Mis productos Favoritos")){
                        shoppingList.getShoppingList().get(0).setLabel("I miei prodotti preferiti");
                    }
                    listDetails.addAll(shoppingList.getShoppingList());
                    myshoppinAdapter.refreshData(shoppingList.getShoppingList());
                    mBinding.rvUserList.setAdapter(myshoppinAdapter);
                }, error -> {
                    getHistoricList();
                    error.printStackTrace();
                    mBinding.setIsLoading(false);
                });
    }

    public void getHistoricList() {
        //mBinding.setIsLoading(true);
        mViewModel.getHistoricList(new StoreDetailRequest(preferencesHelper.getShopperCtxt(getContext()), preferencesHelper.getStoreId()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        genericResponse -> {
                            mBinding.setIsLoading(false);
                            listHistoric.clear();
                            listHistoric.addAll(genericResponse.getHistorics());
                            myHitoryAdapter.refreshData(genericResponse.getHistorics());
                            mBinding.rvUserHiistoricList.setAdapter(myHitoryAdapter);
                        }, error -> {
                            error.printStackTrace();
                            mBinding.setIsLoading(false);
                        });
    }

    public interface onListItemClick {
        void onListItemClick(ShoppingListDetail item);

        void onHistoricListListener(Historic historic);
    }
}
