package com.mscanmc.mshop.mscanmc18.ui.splash;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.broadcast.CradleBroadCastReceiver;
import com.mscanmc.mshop.mscanmc18.cradle.CradleListener;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.data.request.ShopperScanRequest;
import com.mscanmc.mshop.mscanmc18.data.request.UpdateConfigRequest;
import com.mscanmc.mshop.mscanmc18.databinding.ActivitySplashBinding;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseActivity;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseNavigation;
import com.mscanmc.mshop.mscanmc18.ui.base.ViewModelFactory;
import com.mscanmc.mshop.mscanmc18.utils.Constants;
import com.mscanmc.mshop.mscanmc18.utils.DataUtil;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.personalshopper.CradleException;
import com.symbol.emdk.personalshopper.CradleLedFlashInfo;
import com.symbol.emdk.personalshopper.CradleResults;
import com.symbol.emdk.personalshopper.PersonalShopper;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.DispatchingAndroidInjector;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.pushy.sdk.Pushy;
import timber.log.Timber;

public class SplashActivity extends BaseActivity
        implements CradleListener, EMDKManager.EMDKListener {

    ActivitySplashBinding mBinding;
    private UpdatedTokenBroadcastReceiver updatedTokenBroadcastReceiver;
    private NewPushBroadcastReceiver newPushBroadcastReceiver;
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private CradleBroadCastReceiver cradleBroadcastReceiver;
    private PersonalShopper psObject = null;
    @Inject
    protected DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;
    @Inject
    protected BaseNavigation navigation;
    @Inject
    protected ViewModelFactory viewModelFactory;
    @Inject
    protected BaseNavigation baseNavigation;
    SplashViewModel mViewModel;
    @Inject
    PreferencesHelper preferencesHelper;
    BaseActivity activity;
    private List<ScannerInfo> deviceList;
    //private Scanner scanner = null;
    Observable<String> registerForPushy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel.class);
        activity = this;
        preferencesHelper.generateConfigFile(getApplicationContext(), "604");
        registerForPushy = obsRegisterForPushNotifications();
        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode == EMDKResults.STATUS_CODE.FAILURE) {
            Timber.wtf("initDeviceScanner - el EMDK da errores!");
        }

        if (isStoragePermissionGranted()) {
            registerForPushy
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(
                            new Observer<String>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onNext(String token) {
                                    Intent intent = new Intent(Constants.BROADCAST_UPDATED_TOKEN);
                                    intent.putExtra(Constants.TOKEN_SENT, token);
                                    sendBroadcast(intent);
                                    preferencesHelper.setPushyToken(getApplicationContext(), token);

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            e.getMessage(),
                                            Toast.LENGTH_SHORT)
                                            .show();
                                }

                                @Override
                                public void onComplete() {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            getString(R.string.pushy_registered),
                                            Toast.LENGTH_SHORT).show();
                                    try {
                                        updateCradle(true);
                                    } catch (Settings.SettingNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            registerForPushy
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(
                            new Observer<String>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onNext(String token) {
                                    Intent intent = new Intent(Constants.BROADCAST_UPDATED_TOKEN);
                                    intent.putExtra(Constants.TOKEN_SENT, token);
                                    sendBroadcast(intent);
                                    preferencesHelper.setPushyToken(getApplicationContext(), token);

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            e.getMessage(),
                                            Toast.LENGTH_SHORT)
                                            .show();
                                }

                                @Override
                                public void onComplete() {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            getString(R.string.pushy_registered),
                                            Toast.LENGTH_SHORT).show();
                                    try {
                                        updateCradle(true);
                                    } catch (Settings.SettingNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("ooo", "Permission is granted");

                return true;
            } else {

                Log.v("kk", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("KK", "Permission is granted");
            return true;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        onWindowFocusChanged(false);

        updatedTokenBroadcastReceiver = new UpdatedTokenBroadcastReceiver();
        registerReceiver(
                updatedTokenBroadcastReceiver, new IntentFilter(Constants.BROADCAST_UPDATED_TOKEN));

        newPushBroadcastReceiver = new NewPushBroadcastReceiver();
        registerReceiver(newPushBroadcastReceiver, new IntentFilter(Constants.BROADCAST_PUSH));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseEmdk();
        baseNavigation.startMainActivity(activity);
        unregisterReceiver(updatedTokenBroadcastReceiver);
        unregisterReceiver(newPushBroadcastReceiver);
        unregisterReceiver(cradleBroadcastReceiver);
        mViewModel.unSuscribeMediator();

    }

    @Override
    public void onCradleEmpty() {
        //deInitScanner();
        //releaseEmdk();
        try {
            updateCradle(false);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCradleBusy() throws CradleException {
        Timber.wtf("Splash busy");

        try {
            updateCradle(true);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        //emdkManager.release();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // emdkManager.release();
    }

   /* private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.cancelRead();
                scanner.disable();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
            scanner.removeDataListener(this);
            scanner.removeStatusListener(this);
            try {
                scanner.release();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
            scanner = null;
        }
    }*/

    public void releaseEmdk() {
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    public static void start(BaseActivity activity) {
        Intent intent = new Intent(activity, SplashActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        psObject = (PersonalShopper) this.emdkManager.getInstance(EMDKManager.FEATURE_TYPE.PERSONALSHOPPER);

        try {
            if (!psObject.cradle.isEnabled()) {
                psObject.cradle.enable();
            }
        } catch (CradleException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClosed() {
        //releaseEmdk();

    }


    public class NewPushBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String encodedId = intent.getStringExtra(Constants.BROADCAST_PUSH_ENCODED_ID);
            if (encodedId != null) {
                mViewModel.getShopperScan(new ShopperScanRequest(encodedId, preferencesHelper.getStoreId(), preferencesHelper.getUniqueId(getApplicationContext())))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(shopperScanResponse -> {
                            preferencesHelper.setShopperCtx(getApplicationContext(), shopperScanResponse.getShopperCtx());
                            baseNavigation.showProductFragmentList(activity);
                            try {
                                unlockDevice();
                            } catch (Settings.SettingNotFoundException e) {
                                e.printStackTrace();
                            }
                        });
            }

        }

    }

    public void unlockDevice() throws Settings.SettingNotFoundException {
        if (psObject != null && psObject.cradle != null) {
            int onDuration = 2000;
            int offDuration = 300;
            int unlockDuration = 15;
            try {
                CradleLedFlashInfo ledFlashInfo =
                        new CradleLedFlashInfo(onDuration, offDuration, true);
                CradleResults result = psObject.cradle.unlock(unlockDuration, ledFlashInfo);
            } catch (CradleException e) {
                e.printStackTrace();
            }
        }

    }

    private void updateCradle(boolean isOnCradle) throws Settings.SettingNotFoundException {
        String data = isOnCradle ? getCradleData() : null;
        mViewModel
                .setUpdateConfig(
                        new UpdateConfigRequest(
                                0,
                                preferencesHelper.getUniqueId(getApplicationContext()),
                                "status",
                                String.valueOf(DataUtil.getBatteryLevel(getApplication())),
                                DataUtil.recoverStoreIdValue(),
                                DataUtil.DEVICE_TYPE,
                                DataUtil.DEVICE_MODEL,
                                data,
                                preferencesHelper.getPushyToken(getApplicationContext()),
                                DataUtil.getApplicationName(getApplicationContext()),
                                DataUtil.getBrand(),
                                DataUtil.getDeviceModel(),
                                DataUtil.getAndroidVersion(),
                                DataUtil.getAppVersion(getApplicationContext()),
                                DataUtil.getDeviceType(),
                                DataUtil.getConnection(getApplicationContext()),
                                DataUtil.getSsid(getApplicationContext()),
                                DataUtil.getMac(getApplication()),
                                preferencesHelper.getUniqueId(getApplicationContext()),
                                DataUtil.getIpaddress(getApplication()),
                                DataUtil.getSignal(getApplicationContext()),
                                DataUtil.getMajor(),
                                DataUtil.getMinor(),
                                DataUtil.getBrightness(activity),
                                DataUtil.getPapperOut()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(genericMessageResponse -> {
                    Timber.wtf(String.valueOf(genericMessageResponse.getExecuteTime()));
                    if (!isOnCradle) {
                        finish();
                    }
                }, Throwable::printStackTrace);
    }

    public class UpdatedTokenBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String token = intent.getStringExtra(Constants.TOKEN_SENT);
            if (token != null) {
                Timber.wtf("pushy Token " + token);
                preferencesHelper.setPushyToken(token);
            }
        }
    }

    private Observable<String> obsRegisterForPushNotifications() {
        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) throws Exception {
                String deviceToken = Pushy.register(getApplicationContext());
                emitter.onNext(deviceToken);
                emitter.onComplete();
            }
        });
    }

    public String getCradleData() {
        String params = "";

        try {
            if (psObject != null
                    && psObject.cradle != null
                    && psObject.cradle.config.getLocation() != null) {
                int row = psObject.cradle.config.getLocation().row;
                int column = psObject.cradle.config.getLocation().column;
                int wall = psObject.cradle.config.getLocation().wall;

                params = row + "-" + column + "-" + wall;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (params.isEmpty()) {
            return null;
        } else {
            return params;
        }
    }
}
