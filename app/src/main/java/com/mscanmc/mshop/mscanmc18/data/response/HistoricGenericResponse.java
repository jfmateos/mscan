package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HistoricGenericResponse {

    @SerializedName("historics")
    @Expose
    private List<Historic> historics = null;
    @SerializedName("errors")
    @Expose
    private List<String> errors = null;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     */
    public HistoricGenericResponse() {
    }

    /**
     * @param errors
     * @param executeTime
     * @param historics
     */
    public HistoricGenericResponse(List<Historic> historics, List<String> errors, double executeTime) {
        super();
        this.historics = historics;
        this.errors = errors;
        this.executeTime = executeTime;
    }

    public List<Historic> getHistorics() {
        return historics;
    }

    public void setHistorics(List<Historic> historics) {
        this.historics = historics;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }
}
