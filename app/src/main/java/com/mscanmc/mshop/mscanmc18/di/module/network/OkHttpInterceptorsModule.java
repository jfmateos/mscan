package com.mscanmc.mshop.mscanmc18.di.module.network;

import com.mscanmc.mshop.mscanmc18.network.OkHttpInterceptors;

import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;
import okhttp3.logging.HttpLoggingInterceptor;

import static java.util.Collections.singletonList;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Module
public class OkHttpInterceptorsModule {

    public OkHttpInterceptorsModule() {
    }

    @Provides
    @Singleton
    @NonNull
    public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    @OkHttpInterceptors
    @Singleton
    @NonNull
    public List<HttpLoggingInterceptor> provideOkHttpInterceptors(
            @NonNull HttpLoggingInterceptor httpLoggingInterceptor) {
        return singletonList(httpLoggingInterceptor);
    }
}
