package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PromoResponse {
    @SerializedName("ShoppingListDetail")
    @Expose
    private ShoppingListDetail ShoppingListDetail;
    @SerializedName("errors")
    @Expose
    private List<String> errors = null;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     */
    public PromoResponse() {
    }

    /**
     * @param errors
     * @param ShoppingListDetail
     * @param executeTime
     */
    public PromoResponse(ShoppingListDetail ShoppingListDetail, List<String> errors, double executeTime) {
        super();
        this.ShoppingListDetail = ShoppingListDetail;
        this.errors = errors;
        this.executeTime = executeTime;
    }

    public ShoppingListDetail getShoppingListDetail() {
        return ShoppingListDetail;
    }

    public void setShoppingListDetail(ShoppingListDetail ShoppingListDetail) {
        this.ShoppingListDetail = ShoppingListDetail;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }


}
