package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse {

    @SerializedName("shopperEvents")
    @Expose
    private List<ShopperEvent> shopperEvents = null;
    @SerializedName("errors")
    @Expose
    private List<String> errors = null;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     */
    public NotificationResponse() {
    }

    /**
     * @param errors
     * @param shopperEvents
     * @param executeTime
     */
    public NotificationResponse(List<ShopperEvent> shopperEvents, List<String> errors, double executeTime) {
        super();
        this.shopperEvents = shopperEvents;
        this.errors = errors;
        this.executeTime = executeTime;
    }

    public List<ShopperEvent> getShopperEvents() {
        return shopperEvents;
    }

    public void setShopperEvents(List<ShopperEvent> shopperEvents) {
        this.shopperEvents = shopperEvents;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

}
