package com.mscanmc.mshop.mscanmc18.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mscanmc.mshop.mscanmc18.data.response.Promo;
import com.mscanmc.mshop.mscanmc18.data.response.ShoppingListDetail;
import com.mscanmc.mshop.mscanmc18.data.response.StoreDetailResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

@Singleton
public class PreferencesHelper {

    public static final String PREF_FILE_NAME = "mscan_mc18";

    public static final String UNIQUE_ID = "UNIQUE_ID";
    public static final String BASKET_ITEMS_NUMBER = "BASKET_ITEMS_NUMBER";
    private static final String SHOP_CTXT = "SHOPPER_CTXT";
    private static final String PUSHY_TOKEN = "PUSHY_TOKEN";
    private static final String SHOPPING_LIST = "STORE";
    private static final String ENCODED_ID = "ENCODED_ID";
    private static final String STORE_ID = "STORE_ID";
    private static final String BASKET_SESSION = "BASKET_SESSION";
    private static final String MY_SHOPPING_LIST = "MY SHOPPING LIST";
    public static final String SAVED_LIST = "SAVED_LIST";
    public static final String SELECTED_LIST = "SELECTED_LIST";
    private String uniqueId;
    private final SharedPreferences mPref;
    private final Gson mGson;
    String pushyToken;
    StoreDetailResponse storeResponse = new StoreDetailResponse();
    private HashMap<String, Boolean> basketSession = new HashMap<>();
    private ShoppingListDetail savedList = new ShoppingListDetail();
    private List<Promo> promoList = new ArrayList<>();
    private Promo promo = new Promo();
    private String encodedId;
    private int storeId;
    private String shopperCtx;
    private String tokenSession;
    //TODO saber de dónde viene este valor
    private String source = "customer_session";
    private String label = "MY SHOPPING LIST";
    private boolean isSelectedList = false;
    private String PROMO_SESSION = "PROMO_SESSION";

    @Inject
    public PreferencesHelper(Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        mGson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz").create();
    }

    public String getUniqueId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        uniqueId = pref.getString(UNIQUE_ID, "");
        if (uniqueId.equals("")) {
            uniqueId =
                    Settings.Secure.getString(
                            context.getContentResolver(), Settings.Secure.ANDROID_ID);
            setUniqueId(context, uniqueId);
        }
        return uniqueId;
    }


    public void setUniqueId(Context context, String uniqueId) {
        SharedPreferences pref =
                context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(UNIQUE_ID, uniqueId);
        editor.apply();
    }

    public void setPushyToken(Context context, String pushyToken) {

        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(PUSHY_TOKEN, pushyToken);
        editor.apply();
    }

    public String getPushyToken(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        pushyToken = pref.getString(PUSHY_TOKEN, "");
        return pushyToken;
    }

    public String getEncodedId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        pushyToken = pref.getString(ENCODED_ID, "");
        return pushyToken;
    }

    public void setEncodedId(Context context, String encodedId) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(ENCODED_ID, encodedId);
        editor.apply();
    }

    public String getEncodedId() {
        return encodedId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    // TODO sacar el storeid
    public int getStoreId() {
        return 604;
    }

    public void setBasketItemsNumber(Context context, int basketItemsNumber) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(BASKET_ITEMS_NUMBER, basketItemsNumber);
        editor.apply();
    }

    public void setShopperCtx(Context context, String shopperCtx) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SHOP_CTXT, shopperCtx);
        editor.apply();
    }

    public String getShopperCtxt(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        shopperCtx = pref.getString(SHOP_CTXT, "");
        return shopperCtx;
    }

    public StoreDetailResponse getStoreResponse(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        String json = pref.getString(SHOPPING_LIST, "");
        storeResponse = mGson.fromJson(json, StoreDetailResponse.class);
        return storeResponse;
    }

    public void setStoreResponse(Context context, StoreDetailResponse storeResponse) {
        this.storeResponse = storeResponse;
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        String json = mGson.toJson(storeResponse);
        editor.putString(SHOPPING_LIST, json);
        editor.apply();
    }

    public void generateConfigFile(Context context, String sBody) {
        SharedPreferences pref =
                context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(STORE_ID, sBody);
    }

    public void setPushyToken(String token) {
        this.pushyToken = token;
    }

    public void deleteUserData(Context context) {
        SharedPreferences preferences =
                context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().remove(SHOP_CTXT).apply();
        preferences.edit().remove(SHOPPING_LIST).apply();
        Timber.e("user data deleted");
    }

    public void putAccessToken(String tokenSession) {
        this.tokenSession = tokenSession;
    }

    public String getAccessToken() {
        return tokenSession;
    }

    public String getSource() {
        return source;
    }

    public HashMap<String, Boolean> getBasketSession(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        String json = pref.getString(BASKET_SESSION, "");
        storeResponse = mGson.fromJson(json, StoreDetailResponse.class);
        return basketSession;
    }

    public void setBasketSession(Context context, HashMap<String, Boolean> basketSession) {
        this.basketSession = basketSession;
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        String json = mGson.toJson(basketSession);
        editor.putString(BASKET_SESSION, json);
        editor.apply();
    }

    public String getSavedList(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        label = pref.getString(SAVED_LIST, label);
        return label;
    }

    public Boolean isSelectedList(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        isSelectedList = pref.getBoolean(SELECTED_LIST, false);
        return isSelectedList;
    }

    public void addPromo(Context context, List<Promo> promo) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        String json = mGson.toJson(promo);
        editor.putString(PROMO_SESSION, json);
        editor.apply();

    }

    public void setPromoList(List<Promo> promoList) {
        this.promoList = promoList;
    }
}
