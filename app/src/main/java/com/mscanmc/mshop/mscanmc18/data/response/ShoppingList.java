package com.mscanmc.mshop.mscanmc18.data.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShoppingList {

    @SerializedName("errors")
    @Expose
    private List<String> errors = null;
    @SerializedName("shoppingList")
    @Expose
    private List<ShoppingListDetail> shoppingList = null;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public ShoppingList() {
    }

    /**
     *
     * @param shoppingList
     * @param errors
     * @param executeTime
     */
    public ShoppingList(List<String> errors, List<ShoppingListDetail> shoppingList, double executeTime) {
        super();
        this.errors = errors;
        this.shoppingList = shoppingList;
        this.executeTime = executeTime;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<ShoppingListDetail> getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(List<ShoppingListDetail> shoppingList) {
        this.shoppingList = shoppingList;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

}
