package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Historic {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("store")
    @Expose
    private FavouriteStoreResponse store;
    @SerializedName("dateHeure")
    @Expose
    private String dateHeure;
    @SerializedName("dateValidation")
    @Expose
    private String dateValidation;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("ticket")
    @Expose
    private String ticket;
    @SerializedName("productsCount")
    @Expose
    private String productsCount;
    @SerializedName("itemList")
    @Expose
    private List<BasketItemResponse> BasketItemResponse = null;

    /**
     * No args constructor for use in serialization
     */
    public Historic() {
    }

    /**
     * @param total
     * @param id
     * @param productsCount
     * @param store
     * @param ticket
     * @param BasketItemResponse
     * @param dateHeure
     * @param dateValidation
     */
    public Historic(String id, FavouriteStoreResponse store, String dateHeure, String dateValidation, String total, String ticket, String productsCount, List<BasketItemResponse> BasketItemResponse) {
        super();
        this.id = id;
        this.store = store;
        this.dateHeure = dateHeure;
        this.dateValidation = dateValidation;
        this.total = total;
        this.ticket = ticket;
        this.productsCount = productsCount;
        this.BasketItemResponse = BasketItemResponse;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FavouriteStoreResponse getStore() {
        return store;
    }

    public void setStore(FavouriteStoreResponse store) {
        this.store = store;
    }

    public String getDateHeure() {
        return dateHeure;
    }

    public void setDateHeure(String dateHeure) {
        this.dateHeure = dateHeure;
    }

    public String getDateValidation() {
        return dateValidation;
    }

    public void setDateValidation(String dateValidation) {
        this.dateValidation = dateValidation;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getProductsCount() {
        return productsCount;
    }

    public void setProductsCount(String productsCount) {
        this.productsCount = productsCount;
    }

    public List<BasketItemResponse> getBasketItemResponse() {
        return BasketItemResponse;
    }

    public void setBasketItemResponse(List<BasketItemResponse> BasketItemResponse) {
        this.BasketItemResponse = BasketItemResponse;

    }
}
