package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class FinishSessionRequest {
    @SerializedName("shopperCtx")
    @Expose
    private String shopperCtx;
    @SerializedName("codeType")
    @Expose
    private String codeType;
    @SerializedName("codeValue")
    @Expose
    private String codeValue;
    @SerializedName("unfoundProducts")
    @Expose
    private boolean unfoundProducts;

    public FinishSessionRequest() {

    }

    public FinishSessionRequest(String shoperCtx, String codeType, String codeValue, boolean unfoundProducts) {
        this.shopperCtx = shoperCtx;
        this.codeType = codeType;
        this.codeValue = codeValue;
        this.unfoundProducts = unfoundProducts;
    }

    public String getShopperCtx() {
        return shopperCtx;
    }

    public void setShopperCtx(String shopperCtx) {
        this.shopperCtx = shopperCtx;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    public boolean isUnfoundProducts() {
        return unfoundProducts;
    }

    public void setUnfoundProducts(boolean unfoundProducts) {
        this.unfoundProducts = unfoundProducts;
    }
    public HashMap<String, String> toParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put("shopperCtx", shopperCtx);
        params.put("codeType", codeType);
        params.put("codeValue", codeValue);
        params.put("unfoundProducts", String.valueOf(unfoundProducts));

        return params;
    }
}