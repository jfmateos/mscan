package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShoppingListDetails_ {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("version")
    @Expose
    private double version;
    @SerializedName("sentBy")
    @Expose
    private String sentBy;
    @SerializedName("entriesCount")
    @Expose
    private String entriesCount;
    @SerializedName("shared")
    @Expose
    private String shared;
    @SerializedName("colorCode")
    @Expose
    private String colorCode;
    @SerializedName("colorBorderCode")
    @Expose
    private String colorBorderCode;
    @SerializedName("reminderDate")
    @Expose
    private String reminderDate;
    @SerializedName("userCreationDate")
    @Expose
    private String userCreationDate;
    @SerializedName("userModificationDate")
    @Expose
    private String userModificationDate;
    @SerializedName("favourite")
    @Expose
    private String favourite;
    @SerializedName("editable")
    @Expose
    private String editable;
    @SerializedName("ownerId")
    @Expose
    private String ownerId;
    @SerializedName("urlShare")
    @Expose
    private String urlShare;
    @SerializedName("sharedShoppers")
    @Expose
    private SharedShopper sharedShoppers;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("locatedItems")
    @Expose
    private List<LocatedItem> locatedItems = null;

    /**
     * No args constructor for use in serialization
     */
    public ShoppingListDetails_() {
    }

    /**
     * @param locatedItems
     * @param shared
     * @param userModificationDate
     * @param entriesCount
     * @param reminderDate
     * @param urlShare
     * @param ownerId
     * @param label
     * @param code
     * @param sharedShoppers
     * @param colorBorderCode
     * @param editable
     * @param version
     * @param id
     * @param items
     * @param userCreationDate
     * @param sentBy
     * @param colorCode
     * @param favourite
     */
    public ShoppingListDetails_(String id, String code, String label, double version, String sentBy, String entriesCount, String shared, String colorCode, String colorBorderCode, String reminderDate, String userCreationDate, String userModificationDate, String favourite, String editable, String ownerId, String urlShare, SharedShopper sharedShoppers, List<Item> items, List<LocatedItem> locatedItems) {
        super();
        this.id = id;
        this.code = code;
        this.label = label;
        this.version = version;
        this.sentBy = sentBy;
        this.entriesCount = entriesCount;
        this.shared = shared;
        this.colorCode = colorCode;
        this.colorBorderCode = colorBorderCode;
        this.reminderDate = reminderDate;
        this.userCreationDate = userCreationDate;
        this.userModificationDate = userModificationDate;
        this.favourite = favourite;
        this.editable = editable;
        this.ownerId = ownerId;
        this.urlShare = urlShare;
        this.sharedShoppers = sharedShoppers;
        this.items = items;
        this.locatedItems = locatedItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public String getSentBy() {
        return sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public String getEntriesCount() {
        return entriesCount;
    }

    public void setEntriesCount(String entriesCount) {
        this.entriesCount = entriesCount;
    }

    public String getShared() {
        return shared;
    }

    public void setShared(String shared) {
        this.shared = shared;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getColorBorderCode() {
        return colorBorderCode;
    }

    public void setColorBorderCode(String colorBorderCode) {
        this.colorBorderCode = colorBorderCode;
    }

    public String getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(String reminderDate) {
        this.reminderDate = reminderDate;
    }

    public String getUserCreationDate() {
        return userCreationDate;
    }

    public void setUserCreationDate(String userCreationDate) {
        this.userCreationDate = userCreationDate;
    }

    public String getUserModificationDate() {
        return userModificationDate;
    }

    public void setUserModificationDate(String userModificationDate) {
        this.userModificationDate = userModificationDate;
    }

    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }

    public String getEditable() {
        return editable;
    }

    public void setEditable(String editable) {
        this.editable = editable;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getUrlShare() {
        return urlShare;
    }

    public void setUrlShare(String urlShare) {
        this.urlShare = urlShare;
    }

    public SharedShopper getSharedShoppers() {
        return sharedShoppers;
    }

    public void setSharedShoppers(SharedShopper sharedShoppers) {
        this.sharedShoppers = sharedShoppers;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<LocatedItem> getLocatedItems() {
        return locatedItems;
    }

    public void setLocatedItems(List<LocatedItem> locatedItems) {
        this.locatedItems = locatedItems;
    }

}