package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TripDtoItemInitialRequest {


    @SerializedName("itemList")
    @Expose
    private List<String> itemList = null;
    @SerializedName("shopperCtx")
    @Expose
    private String shopperCtx;

    /**
     * No args constructor for use in serialization
     */
    public TripDtoItemInitialRequest() {
    }

    /**
     * @param itemList
     * @param shopperCtx
     */
    public TripDtoItemInitialRequest(List<String> itemList, String shopperCtx) {
        super();
        this.itemList = itemList;
        this.shopperCtx = shopperCtx;
    }

    public TripDtoItemInitialRequest(String shopperCtxt) {
        this.shopperCtx = shopperCtxt;
        itemList = new ArrayList<>();
    }

    public List<String> getItemList() {
        return itemList;
    }

    public void setItemList(List<String> itemList) {
        this.itemList = itemList;
    }

    public String getShopperCtx() {
        return shopperCtx;
    }

    public void setShopperCtx(String shopperCtx) {
        this.shopperCtx = shopperCtx;
    }
}