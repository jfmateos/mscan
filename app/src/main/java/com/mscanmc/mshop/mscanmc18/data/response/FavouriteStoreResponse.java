package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FavouriteStoreResponse {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("precode")
    @Expose
    private String precode;

    @SerializedName("label")
    @Expose
    private String label;

    @SerializedName("selfScanningAvailable")
    @Expose
    private boolean selfScanningAvailable;

    @SerializedName("hasPromoCatalog")
    @Expose
    private boolean hasPromoCatalog;

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    @SerializedName("displayLoyaltyBarcode")
    @Expose
    private boolean displayLoyaltyBarcode;

    @SerializedName("active")
    @Expose
    private boolean active;

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("zipCode")
    @Expose
    private String zipCode;

    @SerializedName("province")
    @Expose
    private String province;

    @SerializedName("region")
    @Expose
    private String region;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("faxNumber")
    @Expose
    private String faxNumber;

    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("storeImage")
    @Expose
    private String storeImage;

    @SerializedName("businessHours")
    @Expose
    private List<BusinessHourResponse> businessHours;

    @SerializedName("groupedBusinessHours")
    @Expose
    private List<String> groupedBusinessHours;

    @SerializedName("comingHolidays")
    @Expose
    private List<String> comingHolidays = null;

    @SerializedName("services")
    @Expose
    private List<String> services = null;

    @SerializedName("favouriteStore")
    @Expose
    private boolean favouriteStore;

    @SerializedName("ipAddressV4")
    @Expose
    private String ipAddressV4;

    @SerializedName("ipBmc")
    @Expose
    private String ipBmc;

    @SerializedName("ipMasterCashRegister")
    @Expose
    private String ipMasterCashRegister;

    @SerializedName("ipMirrorCashRegister")
    @Expose
    private String ipMirrorCashRegister;

    @SerializedName("userCashRegister")
    @Expose
    private String userCashRegister;

    @SerializedName("passCashRegister")
    @Expose
    private String passCashRegister;

    @SerializedName("portCashRegister")
    @Expose
    private String portCashRegister;

    @SerializedName("minutesUntilClosingTime")
    @Expose
    private double minutesUntilClosingTime;

    @SerializedName("openTodayUntilTime")
    @Expose
    private String openTodayUntilTime;

    @SerializedName("openTodayFromTime")
    @Expose
    private String openTodayFromTime;

    @SerializedName("storeOpen")
    @Expose
    private boolean storeOpen;

    @SerializedName("festiveToday")
    @Expose
    private String festiveToday;

    @SerializedName("totalCommunicationPendingNumber")
    @Expose
    private String totalCommunicationPendingNumber;

    @SerializedName("gasStationList")
    @Expose
    private List<String> gasStationList = null;

    @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("mapDetailFloor0")
    @Expose
    private String mapDetailFloor0;

    @SerializedName("mapDetailFloor1")
    @Expose
    private String mapDetailFloor1;

    @SerializedName("mapDetailFloor2")
    @Expose
    private String mapDetailFloor2;

    @SerializedName("mapMassFloor0")
    @Expose
    private String mapMassFloor0;

    @SerializedName("mapMassFloor1")
    @Expose
    private String mapMassFloor1;

    @SerializedName("mapMassFloor2")
    @Expose
    private String mapMassFloor2;

    @SerializedName("mapDetailFloor0Url")
    @Expose
    private String mapDetailFloor0Url;

    @SerializedName("mapDetailFloor1Url")
    @Expose
    private String mapDetailFloor1Url;

    @SerializedName("mapDetailFloor2Url")
    @Expose
    private String mapDetailFloor2Url;

    @SerializedName("mapMassFloor0Url")
    @Expose
    private String mapMassFloor0Url;

    @SerializedName("mapMassFloor1Url")
    @Expose
    private String mapMassFloor1Url;

    @SerializedName("mapMassFloor2Url")
    @Expose
    private String mapMassFloor2Url;

    @SerializedName("turn")
    @Expose
    private boolean turn;

    @SerializedName("ableOnlineShopping")
    @Expose
    private boolean ableOnlineShopping;

    @SerializedName("urlOnlineShopping")
    @Expose
    private String urlOnlineShopping;

    @SerializedName("myClub")
    @Expose
    private boolean myClub;

    @SerializedName("visible")
    @Expose
    private boolean visible;

    @SerializedName("fixProductName")
    @Expose
    private String fixProductName;

    @SerializedName("ecomCode")
    @Expose
    private String ecomCode;

    /** No args constructor for use in serialization */
    public FavouriteStoreResponse() {}

    /**
     * @param portCashRegister
     * @param visible
     * @param street
     * @param comingHolidays
     * @param selfScanningAvailable
     * @param ipAddressV4
     * @param storeOpen
     * @param mapDetailFloor0
     * @param mapDetailFloor2
     * @param city
     * @param mapDetailFloor1
     * @param mapMassFloor0Url
     * @param phoneNumber
     * @param zipCode
     * @param province
     * @param displayLoyaltyBarcode
     * @param longitude
     * @param minutesUntilClosingTime
     * @param turn
     * @param services
     * @param fixProductName
     * @param openTodayFromTime
     * @param ipMirrorCashRegister
     * @param mapDetailFloor2Url
     * @param label
     * @param passCashRegister
     * @param code
     * @param mapMassFloor2Url
     * @param ecomCode
     * @param country
     * @param thumbnail
     * @param email
     * @param active
     * @param userCashRegister
     * @param storeImage
     * @param latitude
     * @param ipMasterCashRegister
     * @param totalCommunicationPendingNumber
     * @param region
     * @param favouriteStore
     * @param precode
     * @param id
     * @param distance
     * @param faxNumber
     * @param mapMassFloor2
     * @param gasStationList
     * @param businessHours
     * @param mapMassFloor0
     * @param mapMassFloor1Url
     * @param mapMassFloor1
     * @param hasPromoCatalog
     * @param groupedBusinessHours
     * @param mapDetailFloor0Url
     * @param festiveToday
     * @param mapDetailFloor1Url
     * @param ableOnlineShopping
     * @param myClub
     * @param openTodayUntilTime
     * @param ipBmc
     * @param urlOnlineShopping
     */
    public FavouriteStoreResponse(
            int id,
            String code,
            String precode,
            String label,
            boolean selfScanningAvailable,
            boolean hasPromoCatalog,
            String thumbnail,
            boolean displayLoyaltyBarcode,
            boolean active,
            String street,
            String city,
            String zipCode,
            String province,
            String region,
            String country,
            double longitude,
            double latitude,
            String faxNumber,
            String phoneNumber,
            String email,
            String storeImage,
            List<BusinessHourResponse> businessHours,
            List<String> groupedBusinessHours,
            List<String> comingHolidays,
            List<String> services,
            boolean favouriteStore,
            String ipAddressV4,
            String ipBmc,
            String ipMasterCashRegister,
            String ipMirrorCashRegister,
            String userCashRegister,
            String passCashRegister,
            String portCashRegister,
            double minutesUntilClosingTime,
            String openTodayUntilTime,
            String openTodayFromTime,
            boolean storeOpen,
            String festiveToday,
            String totalCommunicationPendingNumber,
            List<String> gasStationList,
            String distance,
            String mapDetailFloor0,
            String mapDetailFloor1,
            String mapDetailFloor2,
            String mapMassFloor0,
            String mapMassFloor1,
            String mapMassFloor2,
            String mapDetailFloor0Url,
            String mapDetailFloor1Url,
            String mapDetailFloor2Url,
            String mapMassFloor0Url,
            String mapMassFloor1Url,
            String mapMassFloor2Url,
            boolean turn,
            boolean ableOnlineShopping,
            String urlOnlineShopping,
            boolean myClub,
            boolean visible,
            String fixProductName,
            String ecomCode) {
        super();
        this.id = id;
        this.code = code;
        this.precode = precode;
        this.label = label;
        this.selfScanningAvailable = selfScanningAvailable;
        this.hasPromoCatalog = hasPromoCatalog;
        this.thumbnail = thumbnail;
        this.displayLoyaltyBarcode = displayLoyaltyBarcode;
        this.active = active;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.province = province;
        this.region = region;
        this.country = country;
        this.longitude = longitude;
        this.latitude = latitude;
        this.faxNumber = faxNumber;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.storeImage = storeImage;
        this.businessHours = businessHours;
        this.groupedBusinessHours = groupedBusinessHours;
        this.comingHolidays = comingHolidays;
        this.services = services;
        this.favouriteStore = favouriteStore;
        this.ipAddressV4 = ipAddressV4;
        this.ipBmc = ipBmc;
        this.ipMasterCashRegister = ipMasterCashRegister;
        this.ipMirrorCashRegister = ipMirrorCashRegister;
        this.userCashRegister = userCashRegister;
        this.passCashRegister = passCashRegister;
        this.portCashRegister = portCashRegister;
        this.minutesUntilClosingTime = minutesUntilClosingTime;
        this.openTodayUntilTime = openTodayUntilTime;
        this.openTodayFromTime = openTodayFromTime;
        this.storeOpen = storeOpen;
        this.festiveToday = festiveToday;
        this.totalCommunicationPendingNumber = totalCommunicationPendingNumber;
        this.gasStationList = gasStationList;
        this.distance = distance;
        this.mapDetailFloor0 = mapDetailFloor0;
        this.mapDetailFloor1 = mapDetailFloor1;
        this.mapDetailFloor2 = mapDetailFloor2;
        this.mapMassFloor0 = mapMassFloor0;
        this.mapMassFloor1 = mapMassFloor1;
        this.mapMassFloor2 = mapMassFloor2;
        this.mapDetailFloor0Url = mapDetailFloor0Url;
        this.mapDetailFloor1Url = mapDetailFloor1Url;
        this.mapDetailFloor2Url = mapDetailFloor2Url;
        this.mapMassFloor0Url = mapMassFloor0Url;
        this.mapMassFloor1Url = mapMassFloor1Url;
        this.mapMassFloor2Url = mapMassFloor2Url;
        this.turn = turn;
        this.ableOnlineShopping = ableOnlineShopping;
        this.urlOnlineShopping = urlOnlineShopping;
        this.myClub = myClub;
        this.visible = visible;
        this.fixProductName = fixProductName;
        this.ecomCode = ecomCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPrecode() {
        return precode;
    }

    public void setPrecode(String precode) {
        this.precode = precode;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isSelfScanningAvailable() {
        return selfScanningAvailable;
    }

    public void setSelfScanningAvailable(boolean selfScanningAvailable) {
        this.selfScanningAvailable = selfScanningAvailable;
    }

    public boolean isHasPromoCatalog() {
        return hasPromoCatalog;
    }

    public void setHasPromoCatalog(boolean hasPromoCatalog) {
        this.hasPromoCatalog = hasPromoCatalog;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean getDisplayLoyaltyBarcode() {
        return displayLoyaltyBarcode;
    }

    public void setDisplayLoyaltyBarcode(boolean displayLoyaltyBarcode) {
        this.displayLoyaltyBarcode = displayLoyaltyBarcode;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(String storeImage) {
        this.storeImage = storeImage;
    }

    public List<BusinessHourResponse> getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(List<BusinessHourResponse> businessHours) {
        this.businessHours = businessHours;
    }

    public List<String> getGroupedBusinessHours() {
        return groupedBusinessHours;
    }

    public void setGroupedBusinessHours(List<String> groupedBusinessHours) {
        this.groupedBusinessHours = groupedBusinessHours;
    }

    public List<String> getComingHolidays() {
        return comingHolidays;
    }

    public void setComingHolidays(List<String> comingHolidays) {
        this.comingHolidays = comingHolidays;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public boolean isFavouriteStore() {
        return favouriteStore;
    }

    public void setFavouriteStore(boolean favouriteStore) {
        this.favouriteStore = favouriteStore;
    }

    public String getIpAddressV4() {
        return ipAddressV4;
    }

    public void setIpAddressV4(String ipAddressV4) {
        this.ipAddressV4 = ipAddressV4;
    }

    public String getIpBmc() {
        return ipBmc;
    }

    public void setIpBmc(String ipBmc) {
        this.ipBmc = ipBmc;
    }

    public String getIpMasterCashRegister() {
        return ipMasterCashRegister;
    }

    public void setIpMasterCashRegister(String ipMasterCashRegister) {
        this.ipMasterCashRegister = ipMasterCashRegister;
    }

    public String getIpMirrorCashRegister() {
        return ipMirrorCashRegister;
    }

    public void setIpMirrorCashRegister(String ipMirrorCashRegister) {
        this.ipMirrorCashRegister = ipMirrorCashRegister;
    }

    public String getUserCashRegister() {
        return userCashRegister;
    }

    public void setUserCashRegister(String userCashRegister) {
        this.userCashRegister = userCashRegister;
    }

    public String getPassCashRegister() {
        return passCashRegister;
    }

    public void setPassCashRegister(String passCashRegister) {
        this.passCashRegister = passCashRegister;
    }

    public String getPortCashRegister() {
        return portCashRegister;
    }

    public void setPortCashRegister(String portCashRegister) {
        this.portCashRegister = portCashRegister;
    }

    public double getMinutesUntilClosingTime() {
        return minutesUntilClosingTime;
    }

    public void setMinutesUntilClosingTime(double minutesUntilClosingTime) {
        this.minutesUntilClosingTime = minutesUntilClosingTime;
    }

    public String getOpenTodayUntilTime() {
        return openTodayUntilTime;
    }

    public void setOpenTodayUntilTime(String openTodayUntilTime) {
        this.openTodayUntilTime = openTodayUntilTime;
    }

    public String getOpenTodayFromTime() {
        return openTodayFromTime;
    }

    public void setOpenTodayFromTime(String openTodayFromTime) {
        this.openTodayFromTime = openTodayFromTime;
    }

    public boolean isStoreOpen() {
        return storeOpen;
    }

    public void setStoreOpen(boolean storeOpen) {
        this.storeOpen = storeOpen;
    }

    public String getFestiveToday() {
        return festiveToday;
    }

    public void setFestiveToday(String festiveToday) {
        this.festiveToday = festiveToday;
    }

    public String getTotalCommunicationPendingNumber() {
        return totalCommunicationPendingNumber;
    }

    public void setTotalCommunicationPendingNumber(String totalCommunicationPendingNumber) {
        this.totalCommunicationPendingNumber = totalCommunicationPendingNumber;
    }

    public List<String> getGasStationList() {
        return gasStationList;
    }

    public void setGasStationList(List<String> gasStationList) {
        this.gasStationList = gasStationList;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getMapDetailFloor0() {
        return mapDetailFloor0;
    }

    public void setMapDetailFloor0(String mapDetailFloor0) {
        this.mapDetailFloor0 = mapDetailFloor0;
    }

    public String getMapDetailFloor1() {
        return mapDetailFloor1;
    }

    public void setMapDetailFloor1(String mapDetailFloor1) {
        this.mapDetailFloor1 = mapDetailFloor1;
    }

    public String getMapDetailFloor2() {
        return mapDetailFloor2;
    }

    public void setMapDetailFloor2(String mapDetailFloor2) {
        this.mapDetailFloor2 = mapDetailFloor2;
    }

    public String getMapMassFloor0() {
        return mapMassFloor0;
    }

    public void setMapMassFloor0(String mapMassFloor0) {
        this.mapMassFloor0 = mapMassFloor0;
    }

    public String getMapMassFloor1() {
        return mapMassFloor1;
    }

    public void setMapMassFloor1(String mapMassFloor1) {
        this.mapMassFloor1 = mapMassFloor1;
    }

    public String getMapMassFloor2() {
        return mapMassFloor2;
    }

    public void setMapMassFloor2(String mapMassFloor2) {
        this.mapMassFloor2 = mapMassFloor2;
    }

    public String getMapDetailFloor0Url() {
        return mapDetailFloor0Url;
    }

    public void setMapDetailFloor0Url(String mapDetailFloor0Url) {
        this.mapDetailFloor0Url = mapDetailFloor0Url;
    }

    public String getMapDetailFloor1Url() {
        return mapDetailFloor1Url;
    }

    public void setMapDetailFloor1Url(String mapDetailFloor1Url) {
        this.mapDetailFloor1Url = mapDetailFloor1Url;
    }

    public String getMapDetailFloor2Url() {
        return mapDetailFloor2Url;
    }

    public void setMapDetailFloor2Url(String mapDetailFloor2Url) {
        this.mapDetailFloor2Url = mapDetailFloor2Url;
    }

    public String getMapMassFloor0Url() {
        return mapMassFloor0Url;
    }

    public void setMapMassFloor0Url(String mapMassFloor0Url) {
        this.mapMassFloor0Url = mapMassFloor0Url;
    }

    public String getMapMassFloor1Url() {
        return mapMassFloor1Url;
    }

    public void setMapMassFloor1Url(String mapMassFloor1Url) {
        this.mapMassFloor1Url = mapMassFloor1Url;
    }

    public String getMapMassFloor2Url() {
        return mapMassFloor2Url;
    }

    public void setMapMassFloor2Url(String mapMassFloor2Url) {
        this.mapMassFloor2Url = mapMassFloor2Url;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    public boolean isAbleOnlineShopping() {
        return ableOnlineShopping;
    }

    public void setAbleOnlineShopping(boolean ableOnlineShopping) {
        this.ableOnlineShopping = ableOnlineShopping;
    }

    public String getUrlOnlineShopping() {
        return urlOnlineShopping;
    }

    public void setUrlOnlineShopping(String urlOnlineShopping) {
        this.urlOnlineShopping = urlOnlineShopping;
    }

    public boolean isMyClub() {
        return myClub;
    }

    public void setMyClub(boolean myClub) {
        this.myClub = myClub;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getFixProductName() {
        return fixProductName;
    }

    public void setFixProductName(String fixProductName) {
        this.fixProductName = fixProductName;
    }

    public String getEcomCode() {
        return ecomCode;
    }

    public void setEcomCode(String ecomCode) {
        this.ecomCode = ecomCode;
    }
}
