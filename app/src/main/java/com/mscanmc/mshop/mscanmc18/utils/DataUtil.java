package com.mscanmc.mshop.mscanmc18.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.text.Html;
import android.text.format.Formatter;
import android.util.TypedValue;

import com.mscanmc.mshop.mscanmc18.data.response.BasketItemResponse;
import com.mscanmc.mshop.mscanmc18.data.response.EntityDeskList;
import com.mscanmc.mshop.mscanmc18.data.response.ProductQuantityResponse;
import com.mscanmc.mshop.mscanmc18.data.response.Promo;
import com.symbol.emdk.personalshopper.CradleException;
import com.symbol.emdk.personalshopper.PersonalShopper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.WIFI_SERVICE;
import static com.mscanmc.mshop.mscanmc18.utils.Constants.DISCOUNT_PROMO;

public class DataUtil {
    public static final String DEVICE_TYPE = "SCAN";
    public static final String DEVICE_MODEL = "MC18";

    public Connectivity connectivity = new Connectivity();

    public static int getDpFromValue(Context context, int value) {
        return Math.round(
                TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        value,
                        context.getResources().getDisplayMetrics()));
    }

    public static String configureDateFormattedShort(
            Context context, int dayOfMonth, int monthOfYear, int year) {
        String dateFormatted;
        String language = context.getResources().getConfiguration().locale.toString();

        if (language.contentEquals("es_ES")) {
            dateFormatted = dayOfMonth + " " + getMonthShort(monthOfYear) + " " + year;
        } else {
            dateFormatted = dayOfMonth + "th of " + getMonthShort(monthOfYear) + " of " + year;
        }

        return dateFormatted;
    }

    public static String getMonthShort(int monthOfYear) {
        String result = "";

        switch (monthOfYear) {
            case 1:
                result = "Ene.";
                break;
            case 2:
                result = "Feb.";
                break;
            case 3:
                result = "Mar.";
                break;
            case 4:
                result = "Abr.";
                break;
            case 5:
                result = "May.";
                break;
            case 6:
                result = "Jun.";
                break;
            case 7:
                result = "Jul.";
                break;
            case 8:
                result = "Ago.";
                break;
            case 9:
                result = "Sep.";
                break;
            case 10:
                result = "Oct.";
                break;
            case 11:
                result = "Nov.";
                break;
            case 12:
                result = "Dic.";
                break;
        }

        return result;
    }

    public static int getBatteryLevel(Context context) {
        Intent batteryIntent =
                context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        if (batteryIntent == null) {
            return -1;
        } else {
            int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            if (level == -1 || scale == -1) {
                return 50;
            }

            return Math.round(((float) level / (float) scale) * 100.0f);
        }
    }

    public static String lastConnection() {
        DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
        Date rightNow = new Date();
        return dateFormatter.format(rightNow);
    }

    // TODO cambiar lo que devuelve
    public static int recoverStoreIdValue() {
        int storeId = 0;

        File root = new File(Environment.getExternalStorageDirectory(), "mScan");

        if (root.exists()) {
            File file = new File(root, "mScan_Store.txt");

            if (file.exists()) {
                StringBuilder text = new StringBuilder();

                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;

                    while ((line = br.readLine()) != null) {
                        text.append(line);
                        text.append('\n');
                    }
                    br.close();

                } catch (IOException e) {
                }
                storeId = Integer.parseInt(text.toString().trim());
            }
        }
        // return storeId;
        return 604;
    }

    public static String getPosition(PersonalShopper psObject) throws CradleException {
        int row = psObject.cradle.config.getLocation().row;
        int column = psObject.cradle.config.getLocation().column;
        int wall = psObject.cradle.config.getLocation().wall;
        return row + "-" + column + "-" + wall;
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0
                ? applicationInfo.nonLocalizedLabel.toString()
                : context.getString(stringId);
    }

    public static String getBrand() {
        return Build.MANUFACTURER;
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android SDK: " + sdkVersion + " (" + release + ")";
    }

    public static String getAppVersion(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pInfo == null) {
            return String.valueOf(-1);
        } else {
            return String.valueOf(pInfo.versionCode) + " " + String.valueOf(pInfo.versionName);
        }
    }

    public static String getDeviceType() {
        return android.os.Build.DEVICE;
    }

    public static String getConnection(Context context) {
        return Connectivity.getConnectionInfo(context);
    }

    public static String getSsid(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        assert wifiManager != null;
        WifiInfo info = wifiManager.getConnectionInfo();
        String wifi = info.getSSID().replace("\\\"", "");
        return wifi;
    }

    public static String getMac(Application application) {
        WifiManager manager = (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
        assert manager != null;
        WifiInfo info = manager.getConnectionInfo();
        return info.getMacAddress();
    }

    public static long getSignal(Context context) {
        WifiManager wifiManager =
                (WifiManager)
                        context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int numberOfLevels = 5;
        assert wifiManager != null;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
    }

    public static String getIpaddress(Application application) {
        WifiManager wm = (WifiManager) application.getSystemService(WIFI_SERVICE);
        return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
    }

    // TODO esto es nulo para las pistolas
    public static long getMajor() {
        return -1;
    }

    // TODO esto es nulo para las pistolas
    public static long getMinor() {
        return -1;
    }

    public static String getBrightness(Activity context) throws Settings.SettingNotFoundException {
        String brightness = "0.0";
        try {
            brightness =
                    String.valueOf(
                            Settings.System.getInt(
                                    context.getContentResolver(),
                                    Settings.System.SCREEN_BRIGHTNESS));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return brightness;
    }

    // TODO para las pistolas se devuleve false
    public static boolean getPapperOut() {
        return false;
    }

    //TODO si hay más estados, descomentar el waitting y añadir el siguiente o ponerlo por defecto.
    public static int convertCode(String code) {
        if (code.equalsIgnoreCase(Constants.END_OF_TRIP_CODE)) {
            return 1;
        } else if (code.equalsIgnoreCase(Constants.AUDIT_REQUIRED)) {
            return 2;
        } /*else if (code.equalsIgnoreCase(Constants.WAITING_ASSISTANCE)) {
            return 3;
        }*/ else {
            //default  waitting assistance
            return 3;
        }

    }

    public static String getCurrentTime() {
        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm");
        return dateFormat.format(cal1.getTime());
    }

    public static String getCurrentDate() {
        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd.MM.yy");
        return dateFormat.format(cal1.getTime());
    }

    public static String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public static String formatServerText(String textToFormat) {
        String textToReturn = "";
        if (textToFormat != null) {
            for (int i = 0; i < textToFormat.length(); i++) {
                String character = textToFormat.substring(i, i + 1);

                if (character.contains("\n")) {
                    textToReturn = textToReturn + "<br/>";
                } else {
                    textToReturn = textToReturn + character;
                }
            }
            Pattern regex = Pattern.compile("<([^<>]+)>([^<>]+)</\\1>");
            Matcher matcher = regex.matcher(textToReturn);
            String textWithFormat;
            if (matcher.find()) {
                String colorToSet = "#" + matcher.group(1);
                String beginningTag = "<b><font color=\"" + colorToSet + "\">";
                String endingTag = "</font></b>";
                textWithFormat = beginningTag + matcher.group(2) + endingTag;
                textToReturn = matcher.replaceAll(textWithFormat);
            }
        }
        String titleComponents[] = textToReturn.split("\n");
        return String.valueOf(Html.fromHtml(titleComponents[0]));
    }


    public static void deleteLeftZeros(EntityDeskList entityDeskList) {
        if (entityDeskList.getMyTurn() != null) {
            entityDeskList.setMyTurn(entityDeskList.getMyTurn().replaceFirst("^0+(?!$)", ""));
        }
        if (entityDeskList.getCurrentTurn() != null) {
            entityDeskList.setCurrentTurn(entityDeskList.getCurrentTurn().replaceFirst("^0+(?!$)", ""));
        }
        if (entityDeskList.getPredictedNextTurn() != null) {
            entityDeskList.setPredictedNextTurn(entityDeskList.getPredictedNextTurn().replaceFirst("^0+(?!$)", ""));
        }
    }

    public static String setPromoPrice(int quantity, Promo promo) {
        return String.valueOf(promo.getPriceStore() * quantity / 100);
    }
    public static String setPromoPrice(int quantity, double promo) {
        return String.valueOf(promo * quantity / 100);
    }

    public static double formatFinalPrice(ProductQuantityResponse basketResponse) {
        double tmp = 0;
        for (BasketItemResponse basketItemResponse : basketResponse.getBasket().getBasket()) {
            if (basketResponse.getBasket().getPromo() != null) {
                for (Promo promo : basketResponse.getBasket().getPromo()) {
                    if (promo != null) {
                        if (basketItemResponse.getProduct().getCode().getCodeValue().equalsIgnoreCase(promo.getProductCodeValue())) {
                            if (promo.getUnitMode().equalsIgnoreCase(DISCOUNT_PROMO)) {
                                //basketItemResponse.setFinalPrice(basketItemResponse.getFinalPrice() + (promo.getPriceStore() - (promo.getPriceStore() * promo.getUnitValuePromo() / 10000.00)));
                            }
                        }
                    }
                }
            }
        }

        for (BasketItemResponse basketItemResponse : basketResponse.getBasket().getBasket()) {
            tmp = tmp + (basketItemResponse.getFinalPrice());
        }
        return tmp;
    }
}
