package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FinishSessionResponse {
    @SerializedName("errors")
    @Expose
    private List<String> errors;
    @SerializedName("status")
    @Expose
    private StatusResponse status;
    @SerializedName("executeTime")
    @Expose
    private long executeTime;

    public FinishSessionResponse() {

    }

    public FinishSessionResponse(List<String> errors, StatusResponse status, long executeTime) {

        this.errors = errors;
        this.status = status;
        this.executeTime = executeTime;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public StatusResponse getStatus() {
        return status;
    }

    public void setStatus(StatusResponse status) {
        this.status = status;
    }

    public long getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(long executeTime) {
        this.executeTime = executeTime;
    }

}
