package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateConfigResponse {

    @SerializedName("response")
    @Expose
    private UpdateConfigBasicResponse response;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     */
    public UpdateConfigResponse() {
    }

    /**
     * @param response
     * @param executeTime
     */
    public UpdateConfigResponse(UpdateConfigBasicResponse response, double executeTime) {
        super();
        this.response = response;
        this.executeTime = executeTime;
    }

    public UpdateConfigBasicResponse getResponse() {
        return response;
    }

    public void setResponse(UpdateConfigBasicResponse response) {
        this.response = response;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

}