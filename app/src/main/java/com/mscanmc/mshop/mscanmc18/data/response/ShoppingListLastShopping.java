package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShoppingListLastShopping {

    @SerializedName("errors")
    @Expose
    private List<String> errors = null;
    @SerializedName("shoppingListDetails")
    @Expose
    private ShoppingListDetail shoppingList = null;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public ShoppingListLastShopping() {
    }

    /**
     *
     * @param shoppingList
     * @param errors
     * @param executeTime
     */
    public ShoppingListLastShopping(List<String> errors, ShoppingListDetail shoppingList, double executeTime) {
        super();
        this.errors = errors;
        this.shoppingList = shoppingList;
        this.executeTime = executeTime;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public ShoppingListDetail getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(ShoppingListDetail shoppingList) {
        this.shoppingList = shoppingList;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

}
