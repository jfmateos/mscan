package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateConfigRequest {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("identifier")
    @Expose
    private String identifier;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("battery")
    @Expose
    private String battery;

    @SerializedName("storeId")
    @Expose
    private int storeId;

    @SerializedName("deviceType")
    @Expose
    private String deviceType;

    @SerializedName("deviceModel")
    @Expose
    private String deviceModel;

    @SerializedName("cradle")
    @Expose
    private String cradle;

    @SerializedName("pushToken")
    @Expose
    private String pushToken;

    @SerializedName("typeApp")
    @Expose
    private String typeApp;

    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("model")
    @Expose
    private String model;

    @SerializedName("osVersion")
    @Expose
    private String osVersion;

    @SerializedName("appVersion")
    @Expose
    private String appVersion;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("connection")
    @Expose
    private String connection;

    @SerializedName("wifiSsid")
    @Expose
    private String wifiSsid;

    @SerializedName("mac")
    @Expose
    private String mac;

    @SerializedName("udid")
    @Expose
    private String udid;

    @SerializedName("ipAddress")
    @Expose
    private String ipAddress;

    @SerializedName("signal")
    @Expose
    private long signal;

    @SerializedName("major")
    @Expose
    private long major;

    @SerializedName("minor")
    @Expose
    private long minor;

    @SerializedName("brightness")
    @Expose
    private String brightness;

    @SerializedName("papperOut")
    @Expose
    private boolean papperOut;

    public UpdateConfigRequest(
            int id,
            String identifier,
            String status,
            String battery,
            int storeId,
            String deviceType,
            String deviceModel,
            String cradle,
            String pushToken,
            String typeApp,
            String brand,
            String model,
            String osVersion,
            String appVersion,
            String type,
            String connection,
            String wifiSsid,
            String mac,
            String udid,
            String ipAddress,
            long signal,
            long major,
            long minor,
            String brightness,
            boolean papperOut) {
        this.id = id;
        this.identifier = identifier;
        this.status = status;
        this.battery = battery;
        this.storeId = storeId;
        this.deviceType = deviceType;
        this.deviceModel = deviceModel;
        this.cradle = cradle;
        this.pushToken = pushToken;
        this.typeApp = typeApp;
        this.brand = brand;
        this.model = model;
        this.osVersion = osVersion;
        this.appVersion = appVersion;
        this.type = type;
        this.connection = connection;
        this.wifiSsid = wifiSsid;
        this.mac = mac;
        this.udid = udid;
        this.ipAddress = ipAddress;
        this.signal = signal;
        this.major = major;
        this.minor = minor;
        this.brightness = brightness;
        this.papperOut = papperOut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getCradle() {
        return cradle;
    }

    public void setCradle(String cradle) {
        this.cradle = cradle;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getTypeApp() {
        return typeApp;
    }

    public void setTypeApp(String typeApp) {
        this.typeApp = typeApp;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getWifiSsid() {
        return wifiSsid;
    }

    public void setWifiSsid(String wifiSsid) {
        this.wifiSsid = wifiSsid;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public long getSignal() {
        return signal;
    }

    public void setSignal(long signal) {
        this.signal = signal;
    }

    public long getMajor() {
        return major;
    }

    public void setMajor(long major) {
        this.major = major;
    }

    public long getMinor() {
        return minor;
    }

    public void setMinor(long minor) {
        this.minor = minor;
    }

    public String getBrightness() {
        return brightness;
    }

    public void setBrightness(String brightness) {
        this.brightness = brightness;
    }

    public boolean isPapperOut() {
        return papperOut;
    }

    public void setPapperOut(boolean papperOut) {
        this.papperOut = papperOut;
    }
}
