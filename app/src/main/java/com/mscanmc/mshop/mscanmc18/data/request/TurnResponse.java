package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TurnResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("idTurn")
    @Expose
    private int idTurn;

    @SerializedName("turn")
    @Expose
    private String turn;

    /** No args constructor for use in serialization */
    public TurnResponse() {}

    /**
     * @param message
     * @param idTurn
     * @param turn
     */
    public TurnResponse(String message, int idTurn, String turn) {
        super();
        this.message = message;
        this.idTurn = idTurn;
        this.turn = turn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getIdTurn() {
        return idTurn;
    }

    public void setIdTurn(int idTurn) {
        this.idTurn = idTurn;
    }

    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }
}
