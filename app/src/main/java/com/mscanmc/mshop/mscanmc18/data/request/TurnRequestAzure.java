package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TurnRequestAzure {
    @SerializedName("tokenSession")
    @Expose
    private String tokenSession;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("idEntity")
    @Expose
    private int idEntity;

    @SerializedName("idDesk")
    @Expose
    private int idDesk;
    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;


    @SerializedName("notifyBefore")
    @Expose
    private int notifyBefore;
    @SerializedName("smsNumber")
    @Expose
    private String smsNumber;

    /**
     * No args constructor for use in serialization
     */
    public TurnRequestAzure() {
    }

    public TurnRequestAzure(String tokenSession, String source, int idEntity, int idDesk, String deviceToken, int notifyBefore, String smsNumber) {
        this.tokenSession = tokenSession;
        this.source = source;
        this.idEntity = idEntity;
        this.idDesk = idDesk;
        this.deviceToken = deviceToken;
        this.notifyBefore = notifyBefore;
        this.smsNumber = smsNumber;
    }

    public String getTokenSession() {
        return tokenSession;
    }

    public void setTokenSession(String tokenSession) {
        this.tokenSession = tokenSession;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(int idEntity) {
        this.idEntity = idEntity;
    }

    public int getIdDesk() {
        return idDesk;
    }

    public void setIdDesk(int idDesk) {
        this.idDesk = idDesk;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public int getNotifyBefore() {
        return notifyBefore;
    }

    public void setNotifyBefore(int notifyBefore) {
        this.notifyBefore = notifyBefore;
    }

    public String getSmsNumber() {
        return smsNumber;
    }

    public void setSmsNumber(String smsNumber) {
        this.smsNumber = smsNumber;
    }
}
