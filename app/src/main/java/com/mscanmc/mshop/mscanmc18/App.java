package com.mscanmc.mshop.mscanmc18;

import android.app.Activity;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.stetho.Stetho;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.di.AppInjector;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.support.HasSupportFragmentInjector;
import me.pushy.sdk.Pushy;
import timber.log.Timber;

public class App extends android.app.Application
        implements HasActivityInjector, HasSupportFragmentInjector, HasFragmentInjector {
    private static Context context;
    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Inject
    PreferencesHelper preferencesHelper;
    @Inject
    DispatchingAndroidInjector<android.app.Fragment> fragmentDispatchingInjector;
    @Inject
    DispatchingAndroidInjector<android.support.v4.app.Fragment> supportDispatchingInjector;

    @Override
    public void onCreate() {
        AppInjector.init(this);
        super.onCreate();
        Timber.uprootAll();
        Timber.plant(new Timber.DebugTree());
        Stetho.initializeWithDefaults(this);
        Pushy.listen(this);
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);

    }

    /**
     * Este método es necesario para SDK < 21, dado que si no se ejecuta, da problemas de
     * compilacion.
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    @Override
    public AndroidInjector<android.support.v4.app.Fragment> supportFragmentInjector() {
        return supportDispatchingInjector;
    }

    @Override
    public AndroidInjector<android.app.Fragment> fragmentInjector() {
        return fragmentDispatchingInjector;
    }

}