package com.mscanmc.mshop.mscanmc18.ui.notifications;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.PreferencesHelper;
import com.mscanmc.mshop.mscanmc18.data.request.NotificationRequest;
import com.mscanmc.mshop.mscanmc18.data.response.ShopperEvent;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentNotificationBinding;
import com.mscanmc.mshop.mscanmc18.di.Injectable;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseFragment;
import com.mscanmc.mshop.mscanmc18.ui.base.ViewModelFactory;
import com.mscanmc.mshop.mscanmc18.utils.DataUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NotificationFragment extends BaseFragment implements Injectable {

    @Inject
    protected ViewModelFactory viewModelFactory;
    @Inject
    protected PreferencesHelper preferencesHelper;
    private static final String NOTIFICATION = "notification";
    private OnListFragmentInteractionListener mListener;
    FragmentNotificationBinding mBinding;
    List<ShopperEvent> mValues = new ArrayList<>();
    NotificationViewModel mViewModel;
    NotificationRecyclerViewAdapter notificationRecyclerViewAdapter;

    public NotificationFragment() {
    }

    public static NotificationFragment newInstance(List<ShopperEvent> shopperEvents) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putSerializable(NOTIFICATION, (Serializable) shopperEvents);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mValues = (List<ShopperEvent>) getArguments().getSerializable(NOTIFICATION);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        mViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(NotificationViewModel.class);
        mBinding.rvNotificationList.setLayoutManager(new LinearLayoutManager(mBinding.getRoot().getContext()));
        notificationRecyclerViewAdapter = new NotificationRecyclerViewAdapter(mValues, mListener);
        mBinding.rvNotificationList.setAdapter(notificationRecyclerViewAdapter);
        mBinding.srlNotifications.setColorSchemeResources(
                R.color.primary_dark,
                R.color.green,
                R.color.primary);
        mBinding.srlNotifications.setOnRefreshListener(() -> {
            getNotifications();
            mBinding.srlNotifications.setRefreshing(false);
        });

        getNotifications();
        return mBinding.getRoot();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Notifications
     */
    public void getNotifications() {
        mBinding.setIsLoading(true);
        mViewModel.getStoreDetail(new NotificationRequest(preferencesHelper.getShopperCtxt(getActivity().getApplicationContext()), DataUtil.getLanguage(), ""))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(notificationResponse -> {
                    updateList(notificationResponse.getShopperEvents());
                    mBinding.setIsLoading(false);

                }, e -> {
                    {
                        e.printStackTrace();
                        mBinding.setIsLoading(false);

                    }
                });
    }

    public void updateList(List<ShopperEvent> shopperEvents) {
        mValues.clear();
        mValues.addAll(shopperEvents);
        notificationRecyclerViewAdapter = new NotificationRecyclerViewAdapter(mValues, mListener);
        mBinding.rvNotificationList.setAdapter(notificationRecyclerViewAdapter);
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(ShopperEvent item);
    }
}
