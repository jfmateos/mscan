package com.mscanmc.mshop.mscanmc18.di.module.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.mscanmc.mshop.mscanmc18.BuildConfig;
import com.mscanmc.mshop.mscanmc18.domain.service.ScanService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Juan Francisco Mateos Redondo
 */
@Module
public class RetrofitModule {

    @Provides
    @Singleton
    ScanService getApiInterface(Retrofit retroFit) {
        return retroFit.create(ScanService.class);
    }

    @Provides
    @Singleton
    Retrofit getRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.URL_BASE_ROOT)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient OkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder().readTimeout(60,TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS).addInterceptor(httpLoggingInterceptor).addNetworkInterceptor(new StethoInterceptor()).build();
    }
}
