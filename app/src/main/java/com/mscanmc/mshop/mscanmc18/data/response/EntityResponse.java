package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.List;

public class EntityResponse {
    @SerializedName("idEntity")
    @Expose
    private int idEntity;

    @SerializedName("entityName")
    @Expose
    private String entityName;

    @SerializedName("entityAddress")
    @Expose
    private String entityAddress;

    @SerializedName("entityCP")
    @Expose
    private String entityCP;

    @SerializedName("entityCity")
    @Expose
    private String entityCity;

    @SerializedName("entityCountry")
    @Expose
    private String entityCountry;

    @SerializedName("entityPhone")
    @Expose
    private String entityPhone;

    @SerializedName("entityFav")
    @Expose
    private boolean entityFav;

    @SerializedName("entityImage")
    @Expose
    private String entityImage;

    @SerializedName("latitude")
    @Expose
    private String latitude;

    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("businessHours")
    @Expose
    private List<BussinessResponse> businessHours = null;

    /** No args constructor for use in serialization */
    public EntityResponse() {}

    /**
     * @param idEntity
     * @param entityName
     * @param entityAddress
     * @param entityCP
     * @param entityCity
     * @param entityCountry
     * @param entityPhone
     * @param entityFav
     * @param entityImage
     * @param latitude
     * @param longitude
     * @param businessHours
     */
    public EntityResponse(
            int idEntity,
            String entityName,
            String entityAddress,
            String entityCP,
            String entityCity,
            String entityCountry,
            String entityPhone,
            boolean entityFav,
            String entityImage,
            String latitude,
            String longitude,
            List<BussinessResponse> businessHours) {
        super();
        this.idEntity = idEntity;
        this.entityName = entityName;
        this.entityAddress = entityAddress;
        this.entityCP = entityCP;
        this.entityCity = entityCity;
        this.entityCountry = entityCountry;
        this.entityPhone = entityPhone;
        this.entityFav = entityFav;
        this.entityImage = entityImage;
        this.latitude = latitude;
        this.longitude = longitude;
        this.businessHours = businessHours;
    }

    public int getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(int idEntity) {
        this.idEntity = idEntity;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityAddress() {
        return entityAddress;
    }

    public void setEntityAddress(String entityAddress) {
        this.entityAddress = entityAddress;
    }

    public String getEntityCP() {
        return entityCP;
    }

    public void setEntityCP(String entityCP) {
        this.entityCP = entityCP;
    }

    public String getEntityCity() {
        return entityCity;
    }

    public void setEntityCity(String entityCity) {
        this.entityCity = entityCity;
    }

    public String getEntityCountry() {
        return entityCountry;
    }

    public void setEntityCountry(String entityCountry) {
        this.entityCountry = entityCountry;
    }

    public String getEntityPhone() {
        return entityPhone;
    }

    public void setEntityPhone(String entityPhone) {
        this.entityPhone = entityPhone;
    }

    public boolean getEntityFav() {
        return entityFav;
    }

    public void setEntityFav(boolean entityFav) {
        this.entityFav = entityFav;
    }

    public String getEntityImage() {
        return entityImage;
    }

    public void setEntityImage(String entityImage) {
        this.entityImage = entityImage;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public List<BussinessResponse> getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(List<BussinessResponse> businessHours) {
        this.businessHours = businessHours;
    }

    public BussinessResponse getOpenUntil() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        for (BussinessResponse businessHour : businessHours) {
            if (businessHour.getDay() == ((long) day)) {
                return businessHour;
            }
        }

        return null;
    }
}

