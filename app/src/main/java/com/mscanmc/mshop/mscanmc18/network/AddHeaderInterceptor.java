package com.mscanmc.mshop.mscanmc18.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Juan Francisco Mateos Redondo
 */
public class AddHeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        builder.removeHeader("Content-Type");
        builder.addHeader("Content-Type", "application/json");
        builder.addHeader("Accept", "application/json");
        return chain.proceed(builder.build());
    }
}
