package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Code {

    @SerializedName("codeType")
    @Expose
    private String codeType;
    @SerializedName("codeValue")
    @Expose
    private String codeValue;

    /**
     * No args constructor for use in serialization
     */
    public Code() {
    }

    /**
     * @param codeValue
     * @param codeType
     */
    public Code(String codeType, String codeValue) {
        super();
        this.codeType = codeType;
        this.codeValue = codeValue;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

}
