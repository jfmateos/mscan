package com.mscanmc.mshop.mscanmc18.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> cricket = new ArrayList<String>();
        cricket.add("1 Turn Before");
        cricket.add("2 Turns Before");
        cricket.add("3 Turns Before");
        cricket.add("4 Turns Before");
        cricket.add("5 Turns Before");

        expandableListDetail.put("Notify before", cricket);
        return expandableListDetail;
    }
}
