package com.mscanmc.mshop.mscanmc18.domain.service;

import com.mscanmc.mshop.mscanmc18.data.request.CustomerTurnsRequest;
import com.mscanmc.mshop.mscanmc18.data.request.EntityDetailRequest;
import com.mscanmc.mshop.mscanmc18.data.request.FavRequest;
import com.mscanmc.mshop.mscanmc18.data.request.GetCustomerTerminalStatusRequest;
import com.mscanmc.mshop.mscanmc18.data.request.GetEntityRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TurnRequest;
import com.mscanmc.mshop.mscanmc18.data.request.TurnRequestAzure;
import com.mscanmc.mshop.mscanmc18.data.request.TurnResponse;
import com.mscanmc.mshop.mscanmc18.data.response.CustomerTurnsResponse;
import com.mscanmc.mshop.mscanmc18.data.response.EntityDetailResponse;
import com.mscanmc.mshop.mscanmc18.data.response.GenericResponse;
import com.mscanmc.mshop.mscanmc18.data.response.GetCustomerTerminalStatusResponse;
import com.mscanmc.mshop.mscanmc18.data.response.GetEntityResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AzureService {

    @POST("app/wsGetCustomerTerminalStatus")
    Call<GetCustomerTerminalStatusResponse> getCustomerTerminalStatus(
            @Body GetCustomerTerminalStatusRequest getCustomerTerminalStatusRequest);

    @POST("api/wsGetEntities")
    Call<GetEntityResponse> getEntities(@Body GetEntityRequest entityRequest);

    @POST("app/wsGetCustomerTurns")
    Call<CustomerTurnsResponse> getCustomerTurns(@Body CustomerTurnsRequest ticketListRequest);

    @POST("app/wsSelectFavoriteEntity")
    Call<GenericResponse> getFavouriteEntity(@Body FavRequest favRequest);

    @POST("api/wsRequestNewTurn")
    Call<TurnResponse> getTurn(@Body TurnRequestAzure turnRequest);

    @POST("api/wsDetailEntityDesk")
    Call<EntityDetailResponse> getEntityDetail(@Body EntityDetailRequest entityDetailRequest);

    /*    @GET("v1/ticker")
    Call<List<CryptoPOJO>> getCryptoTicker();

    @GET("v1/ticker/{id}")
    Call<List<CryptoPOJO>> getCryptoDetail(@Path("id") String id);*/

}
