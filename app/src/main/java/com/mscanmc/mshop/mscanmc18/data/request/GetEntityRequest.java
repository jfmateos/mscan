package com.mscanmc.mshop.mscanmc18.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetEntityRequest {

    @SerializedName("tokenSession")
    @Expose
    private String tokenSession;

    /**
     * No args constructor for use in serialization
     */
    public GetEntityRequest() {
    }

    public GetEntityRequest(String tokenSession) {
        super();
        this.tokenSession = tokenSession;
    }

    public String getTokenSession() {
        return tokenSession;
    }

    public void setTokenSession(String tokenSession) {
        this.tokenSession = tokenSession;
    }
}
