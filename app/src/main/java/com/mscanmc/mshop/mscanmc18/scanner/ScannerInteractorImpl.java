package com.mscanmc.mshop.mscanmc18.scanner;

import android.os.AsyncTask;

import com.symbol.emdk.barcode.ScanDataCollection;

/**
 * Created by victor on 7/6/17.
 * Mshop Spain.
 */

public class ScannerInteractorImpl implements ScannerInteractor {
    private ScannerInteractorListener scannerInteractorListener;


    public ScannerInteractorImpl(ScannerInteractorListener scannerInteractorListener) {
        this.scannerInteractorListener = scannerInteractorListener;
    }

    @Override
    public void processScannedData(ScanDataCollection.LabelType labelType, String data) {
        new AsyncDataUpdate(labelType).execute(data);
    }


    private class AsyncDataUpdate extends AsyncTask<String, Void, String> {
        private ScanDataCollection.LabelType labelType;

        public AsyncDataUpdate(ScanDataCollection.LabelType labelType) {
            this.labelType = labelType;
        }

        @Override
        protected String doInBackground(String... params) {
//            StatusData.ScannerStates state = params.getState();
            return params[0];
        }

        protected void onPostExecute(String result) {
            if (result != null) {

                if (result.length() == 8 && labelType == ScanDataCollection.LabelType.EAN8) {
                    result = "00000" + result;
                }

                scannerInteractorListener.onScanValueProcessed(labelType, result);
            }
        }
    }
}
