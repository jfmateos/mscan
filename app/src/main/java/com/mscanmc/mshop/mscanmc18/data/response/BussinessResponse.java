package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BussinessResponse {

    @SerializedName("day")
    @Expose
    private int day;

    @SerializedName("openTimeMorning")
    @Expose
    private String openTimeMorning;

    @SerializedName("closeTimeMorning")
    @Expose
    private String closeTimeMorning;

    @SerializedName("openTimeAfternoon")
    @Expose
    private String openTimeAfternoon;

    @SerializedName("closeTimeAfternoon")
    @Expose
    private String closeTimeAfternoon;

    /** No args constructor for use in serialization */
    public BussinessResponse() {}

    /**
     * @param openTimeAfternoon
     * @param closeTimeMorning
     * @param closeTimeAfternoon
     * @param day
     * @param openTimeMorning
     */
    public BussinessResponse(
            int day,
            String openTimeMorning,
            String closeTimeMorning,
            String openTimeAfternoon,
            String closeTimeAfternoon) {
        super();
        this.day = day;
        this.openTimeMorning = openTimeMorning;
        this.closeTimeMorning = closeTimeMorning;
        this.openTimeAfternoon = openTimeAfternoon;
        this.closeTimeAfternoon = closeTimeAfternoon;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getOpenTimeMorning() {
        return openTimeMorning;
    }

    public void setOpenTimeMorning(String openTimeMorning) {
        this.openTimeMorning = openTimeMorning;
    }

    public String getCloseTimeMorning() {
        return closeTimeMorning;
    }

    public void setCloseTimeMorning(String closeTimeMorning) {
        this.closeTimeMorning = closeTimeMorning;
    }

    public String getOpenTimeAfternoon() {
        return openTimeAfternoon;
    }

    public void setOpenTimeAfternoon(String openTimeAfternoon) {
        this.openTimeAfternoon = openTimeAfternoon;
    }

    public String getCloseTimeAfternoon() {
        return closeTimeAfternoon;
    }

    public void setCloseTimeAfternoon(String closeTimeAfternoon) {
        this.closeTimeAfternoon = closeTimeAfternoon;
    }


}

