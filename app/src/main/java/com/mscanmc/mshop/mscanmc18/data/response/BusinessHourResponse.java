package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessHourResponse {

    @SerializedName("day")
    @Expose
    private int day;
    @SerializedName("openTime")
    @Expose
    private String openTime;
    @SerializedName("closeTime")
    @Expose
    private String closeTime;
    @SerializedName("openTimeAfternoon")
    @Expose
    private String openTimeAfternoon;
    @SerializedName("closeTimeAfternoon")
    @Expose
    private String closeTimeAfternoon;
    @SerializedName("comment")
    @Expose
    private String comment;

    /**
     * No args constructor for use in serialization
     *
     */
    public BusinessHourResponse() {
    }

    /**
     *
     * @param closeTime
     * @param openTimeAfternoon
     * @param closeTimeAfternoon
     * @param openTime
     * @param day
     * @param comment
     */
    public BusinessHourResponse(int day, String openTime, String closeTime, String openTimeAfternoon, String closeTimeAfternoon, String comment) {
        super();
        this.day = day;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.openTimeAfternoon = openTimeAfternoon;
        this.closeTimeAfternoon = closeTimeAfternoon;
        this.comment = comment;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getOpenTimeAfternoon() {
        return openTimeAfternoon;
    }

    public void setOpenTimeAfternoon(String openTimeAfternoon) {
        this.openTimeAfternoon = openTimeAfternoon;
    }

    public String getCloseTimeAfternoon() {
        return closeTimeAfternoon;
    }

    public void setCloseTimeAfternoon(String closeTimeAfternoon) {
        this.closeTimeAfternoon = closeTimeAfternoon;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}