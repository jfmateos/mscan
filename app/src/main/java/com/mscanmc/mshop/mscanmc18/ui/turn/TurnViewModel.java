package com.mscanmc.mshop.mscanmc18.ui.turn;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;

import com.mscanmc.mshop.mscanmc18.data.request.TurnRequest;
import com.mscanmc.mshop.mscanmc18.data.response.DeskListGeneric;
import com.mscanmc.mshop.mscanmc18.domain.repository.MScanRepository;
import com.mscanmc.mshop.mscanmc18.ui.base.BaseViewModel;

import javax.inject.Inject;

public class TurnViewModel extends BaseViewModel {
    MScanRepository repository;
    private final MediatorLiveData<DeskListGeneric> turnRequestMLD =
            new MediatorLiveData<>();

    @Inject
    public TurnViewModel(MScanRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<DeskListGeneric> getTurnRequest(
            TurnRequest updateConfigRequest) {
        LiveData<DeskListGeneric> scrap = repository.getTurnRequest(updateConfigRequest);
        turnRequestMLD.addSource(
                scrap,
                storeDetailResponse -> {
                    if (turnRequestMLD != null) {
                        turnRequestMLD.postValue(storeDetailResponse);
                    }
                });
        return turnRequestMLD;
    }
}
