package com.mscanmc.mshop.mscanmc18.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mscanmc.mshop.mscanmc18.utils.Constants;

import timber.log.Timber;

public class UpdatedTokenBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String token = intent.getStringExtra(Constants.TOKEN_SENT);

        if (token != null) {
            Timber.wtf("TOKEN_SENT");
        }
    }
}
