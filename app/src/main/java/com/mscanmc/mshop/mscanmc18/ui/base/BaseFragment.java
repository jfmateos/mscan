package com.mscanmc.mshop.mscanmc18.ui.base;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.utils.LifecycleSupportFragment;
import com.mscanmc.mshop.mscanmc18.utils.SnackBarUtil;

/**
 * Created by Juan Francisco Mateos Redondo
 */
public abstract class BaseFragment extends LifecycleSupportFragment {

    private BaseActivity baseActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            this.baseActivity = (BaseActivity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        baseActivity = null;
        super.onDetach();
    }

    public void popBack(Fragment fragment) {
        baseActivity.getSupportFragmentManager().popBackStack();
    }

    public void showSnackMessage(View view, String message) {
        SnackBarUtil snackBarUtil = new SnackBarUtil();
        snackBarUtil.styleSnackbar(
                view,
                message,
                getResources().getColor(R.color.primary));
    }
}
