package com.mscanmc.mshop.mscanmc18.data.request;

import java.util.HashMap;

public class NotificationRequest {
    private String shopperCtx;
    private String lang;
    private String eventLastUpdate;

    public NotificationRequest(String shopperCtx, String lang, String eventLastUpdate) {
        this.shopperCtx = shopperCtx;
        this.lang = lang;
        this.eventLastUpdate = eventLastUpdate;
    }

    public HashMap<String, String> toHashMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("shopperCtx", shopperCtx);
        hashMap.put("lang", lang);
        hashMap.put("eventLastUpdate", eventLastUpdate);

        return hashMap;
    }
}
