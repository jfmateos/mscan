package com.mscanmc.mshop.mscanmc18.ui.turn;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.response.EntityDeskList;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentTurnBinding;
import com.mscanmc.mshop.mscanmc18.utils.DataUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyTurnRecyclerViewAdapter extends RecyclerView.Adapter<MyTurnRecyclerViewAdapter.ViewHolder> {

    private final List<EntityDeskList> mValues;
    private final RequestTurn mListener;
    FragmentTurnBinding mBinding;
    ViewGroup viewGroup;

    public MyTurnRecyclerViewAdapter(List<EntityDeskList> items, RequestTurn listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        viewGroup = parent;
        mBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_turn, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.binding.executePendingBindings();
       // mValues.get(position).setMyTurn(mValues.get(position).getMyTurn().replaceFirst("^0+(?!$)", ""));
        DataUtil.deleteLeftZeros(mValues.get(position));
        mBinding.setDesk(mValues.get(position));
        if (mValues.get(position).getDeskName().toLowerCase().contains("panetteria")) {
            Picasso.get().load("http://dev.m-shop.mobi/mBase/mscan/boulangerie.jpg").into(mBinding.ivIconTurn);
        } else if (mValues.get(position).getDeskName().toLowerCase().contains("pescheria")) {
            Picasso.get().load("http://dev.m-shop.mobi/mBase/mscan/boucherie.jpg").into(mBinding.ivIconTurn);
        } else if(mValues.get(position).getDeskName().toLowerCase().contains("macelleria")){
            Picasso.get().load("http://dev.m-shop.mobi/mBase/mscan/poissonerie.jpg").into(mBinding.ivIconTurn);
        }else{
            Picasso.get().load(R.drawable.placeholder).into(mBinding.ivIconTurn);
        }
        mBinding.clTurn.setOnClickListener(v -> {
            if (mValues.get(position).getMyTurn() != null) {
                showYourTurn(mValues.get(position));
            } else {
                showTurnDialog(mValues.get(position));
            }
    });

}

    private void showTurnDialog(EntityDeskList deskList) {
        LayoutInflater layoutInflater = LayoutInflater.from(mBinding.getRoot().getContext());

        View promptView = layoutInflater.inflate(R.layout.dialog_turn, null);
        final AlertDialog mAlertDialog =
                new AlertDialog.Builder(mBinding.getRoot().getContext()).create();
        AppCompatButton bAccept = (AppCompatButton) promptView.findViewById(R.id.turn_accept);
        AppCompatButton bCancel = (AppCompatButton) promptView.findViewById(R.id.turn_cancel);
        AppCompatTextView deskname = (AppCompatTextView) promptView.findViewById(R.id.tv_desk_name);
        AppCompatTextView turn = (AppCompatTextView) promptView.findViewById(R.id.tv_take_turn_number);
        Spinner spinner = (Spinner) promptView.findViewById(R.id.sp_notify_before);
        AppCompatTextView attendig = (AppCompatTextView) promptView.findViewById(R.id.tv_attentdig_to);
        AppCompatTextView before = (AppCompatTextView) promptView.findViewById(R.id.tv_people_before);
        deskname.setText(deskList.getDeskName());
        turn.setText(deskList.getPredictedNextTurn() != null ? String.valueOf(deskList.getPredictedNextTurn().replaceFirst("^0+(?!$)", "")) : "-");
        attendig.setText(String.format(mBinding.getRoot().getContext().getString(R.string.attending_to), String.valueOf(deskList.getCurrentTurn()).replaceFirst("^0+(?!$)", "")));
        before.setText(String.format(mBinding.getRoot().getContext().getString(R.string.people_before_format), String.valueOf(deskList.getPredictedNextTurn()).replaceFirst("^0+(?!$)", "")));
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(mBinding.getRoot().getContext(),
                        R.array.notify_before,
                        android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        bAccept.setOnClickListener(
                v -> {
                    //mListener.requestTurn(String.valueOf(deskList.getIdDesk()), spinner.getSelectedItemPosition());
                    mListener.getTurn(deskList, spinner.getSelectedItemPosition());
                    mAlertDialog.dismiss();
                });
        bCancel.setOnClickListener(
                v -> {
                    mAlertDialog.dismiss();
                });
        mAlertDialog.setView(promptView);
        mAlertDialog.show();
    }

    private void showYourTurn(EntityDeskList deskList) {
        LayoutInflater layoutInflater = LayoutInflater.from(mBinding.getRoot().getContext());

        View promptView = layoutInflater.inflate(R.layout.dialog_taken_turn, null);
        final AlertDialog mAlertDialog =
                new AlertDialog.Builder(mBinding.getRoot().getContext()).create();
        AppCompatImageView imageView = (AppCompatImageView) promptView.findViewById(R.id.ic_close_taken_turn);
        AppCompatTextView title = (AppCompatTextView) promptView.findViewById(R.id.tv_take_turn_desk) ;
        AppCompatTextView turn = (AppCompatTextView) promptView.findViewById(R.id.tv_take_turn_number) ;
        AppCompatTextView date = (AppCompatTextView) promptView.findViewById(R.id.tv_date) ;
        AppCompatTextView hour = (AppCompatTextView) promptView.findViewById(R.id.tv_hour) ;
        title.setText(deskList.getDeskName());
        turn.setText(deskList.getMyTurn().replaceFirst("^0+(?!$)", ""));
        date.setText(DataUtil.getCurrentDate());
        hour.setText(DataUtil.getCurrentTime());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.setView(promptView);
        mAlertDialog.show();
    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }

public class ViewHolder extends RecyclerView.ViewHolder {

    FragmentTurnBinding binding;

    public ViewHolder(FragmentTurnBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

}

public interface RequestTurn {
    void requestTurn(String deskCode, int item);

    void getTurn(EntityDeskList s, int selectedItemPosition);
}
}
