package com.mscanmc.mshop.mscanmc18.data.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EntityDetailResponse {

    @SerializedName("idEntity")
    @Expose
    private int idEntity;

    @SerializedName("entityDeskList")
    @Expose
    private List<EntityDeskList> entityDeskList = null;

    public EntityDetailResponse() {}

    /**
     * @param idEntity
     * @param entityDeskList
     */
    public EntityDetailResponse(int idEntity, List<EntityDeskList> entityDeskList) {
        super();
        this.idEntity = idEntity;
        this.entityDeskList = entityDeskList;
    }

    public int getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(int idEntity) {
        this.idEntity = idEntity;
    }

    public List<EntityDeskList> getEntityDeskList() {
        return entityDeskList;
    }

    public void setEntityDeskList(List<EntityDeskList> entityDeskList) {
        this.entityDeskList = entityDeskList;
    }
}

