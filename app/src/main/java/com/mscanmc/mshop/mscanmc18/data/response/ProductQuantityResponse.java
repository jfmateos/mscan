package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductQuantityResponse {

    @SerializedName("basket")
    @Expose
    private Basket basket;
    @SerializedName("errors")
    @Expose
    private List<ErrorResponse> errors = null;
    @SerializedName("executeTime")
    @Expose
    private double executeTime;

    /**
     * No args constructor for use in serialization
     */
    public ProductQuantityResponse() {
    }

    /**
     * @param errors
     * @param basket
     * @param executeTime
     */
    public ProductQuantityResponse(Basket basket, List<ErrorResponse> errors, double executeTime) {
        super();
        this.basket = basket;
        this.errors = errors;
        this.executeTime = executeTime;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    public List<ErrorResponse> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorResponse> errors) {
        this.errors = errors;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

}
