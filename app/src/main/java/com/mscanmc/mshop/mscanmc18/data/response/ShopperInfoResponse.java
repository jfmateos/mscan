package com.mscanmc.mshop.mscanmc18.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShopperInfoResponse {

    @SerializedName("id")
    @Expose
    private double id;
    @SerializedName("encodedId")
    @Expose
    private String encodedId;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("secondLastName")
    @Expose
    private String secondLastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("docType")
    @Expose
    private String docType;
    @SerializedName("idNumber")
    @Expose
    private String idNumber;
    @SerializedName("birthDate")
    @Expose
    private String birthDate;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("hasLoyaltyCard")
    @Expose
    private String hasLoyaltyCard;
    @SerializedName("acceptMarketingMessage")
    @Expose
    private boolean acceptMarketingMessage;
    @SerializedName("blocked")
    @Expose
    private String blocked;
    @SerializedName("auditLevel")
    @Expose
    private String auditLevel;
    @SerializedName("goldenShopper")
    @Expose
    private String goldenShopper;
    @SerializedName("loyaltyCardNumber")
    @Expose
    private String loyaltyCardNumber;
    @SerializedName("personalLoyaltyCard")
    @Expose
    private String personalLoyaltyCard;
    @SerializedName("shareListWithLoyaltyMembers")
    @Expose
    private boolean shareListWithLoyaltyMembers;
    @SerializedName("comPendingNumber")
    @Expose
    private String comPendingNumber;
    @SerializedName("loyaltyCardStatus")
    @Expose
    private String loyaltyCardStatus;
    @SerializedName("cancellationDate")
    @Expose
    private String cancellationDate;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("lopdCancellation")
    @Expose
    private String lopdCancellation;
    @SerializedName("validationDate")
    @Expose
    private String validationDate;
    @SerializedName("validation")
    @Expose
    private String validation;
    @SerializedName("avatarUrl")
    @Expose
    private String avatarUrl;
    @SerializedName("sendPushOffersCatalogs")
    @Expose
    private boolean sendPushOffersCatalogs;
    @SerializedName("acceptSearchHistory")
    @Expose
    private boolean acceptSearchHistory;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("nbAudits")
    @Expose
    private double nbAudits;
    @SerializedName("userTest")
    @Expose
    private boolean userTest;
    @SerializedName("auditAll")
    @Expose
    private String auditAll;
    @SerializedName("flagGoldenShopper")
    @Expose
    private String flagGoldenShopper;
    @SerializedName("typeAuditShopper")
    @Expose
    private String typeAuditShopper;

    /**
     * No args constructor for use in serialization
     *
     */
    public ShopperInfoResponse() {
    }

    /**
     *
     * @param phone
     * @param flagGoldenShopper
     * @param docType
     * @param hasLoyaltyCard
     * @param lastname
     * @param shareListWithLoyaltyMembers
     * @param password
     * @param id
     * @param cancellationDate
     * @param postalCode
     * @param avatarUrl
     * @param loyaltyCardStatus
     * @param loyaltyCardNumber
     * @param gender
     * @param birthDate
     * @param blocked
     * @param validationDate
     * @param acceptMarketingMessage
     * @param idNumber
     * @param validation
     * @param comPendingNumber
     * @param secondLastName
     * @param firstname
     * @param typeAuditShopper
     * @param sendPushOffersCatalogs
     * @param creationDate
     * @param userTest
     * @param email
     * @param acceptSearchHistory
     * @param auditAll
     * @param comment
     * @param encodedId
     * @param goldenShopper
     * @param personalLoyaltyCard
     * @param lopdCancellation
     * @param auditLevel
     * @param nbAudits
     */
    public ShopperInfoResponse(double id, String encodedId, String firstname, String lastname, String secondLastName, String email, String gender, String docType, String idNumber, String birthDate, String postalCode, String hasLoyaltyCard, boolean acceptMarketingMessage, String blocked, String auditLevel, String goldenShopper, String loyaltyCardNumber, String personalLoyaltyCard, boolean shareListWithLoyaltyMembers, String comPendingNumber, String loyaltyCardStatus, String cancellationDate, String creationDate, String password, String lopdCancellation, String validationDate, String validation, String avatarUrl, boolean sendPushOffersCatalogs, boolean acceptSearchHistory, String phone, String comment, double nbAudits, boolean userTest, String auditAll, String flagGoldenShopper, String typeAuditShopper) {
        super();
        this.id = id;
        this.encodedId = encodedId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.secondLastName = secondLastName;
        this.email = email;
        this.gender = gender;
        this.docType = docType;
        this.idNumber = idNumber;
        this.birthDate = birthDate;
        this.postalCode = postalCode;
        this.hasLoyaltyCard = hasLoyaltyCard;
        this.acceptMarketingMessage = acceptMarketingMessage;
        this.blocked = blocked;
        this.auditLevel = auditLevel;
        this.goldenShopper = goldenShopper;
        this.loyaltyCardNumber = loyaltyCardNumber;
        this.personalLoyaltyCard = personalLoyaltyCard;
        this.shareListWithLoyaltyMembers = shareListWithLoyaltyMembers;
        this.comPendingNumber = comPendingNumber;
        this.loyaltyCardStatus = loyaltyCardStatus;
        this.cancellationDate = cancellationDate;
        this.creationDate = creationDate;
        this.password = password;
        this.lopdCancellation = lopdCancellation;
        this.validationDate = validationDate;
        this.validation = validation;
        this.avatarUrl = avatarUrl;
        this.sendPushOffersCatalogs = sendPushOffersCatalogs;
        this.acceptSearchHistory = acceptSearchHistory;
        this.phone = phone;
        this.comment = comment;
        this.nbAudits = nbAudits;
        this.userTest = userTest;
        this.auditAll = auditAll;
        this.flagGoldenShopper = flagGoldenShopper;
        this.typeAuditShopper = typeAuditShopper;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getEncodedId() {
        return encodedId;
    }

    public void setEncodedId(String encodedId) {
        this.encodedId = encodedId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getHasLoyaltyCard() {
        return hasLoyaltyCard;
    }

    public void setHasLoyaltyCard(String hasLoyaltyCard) {
        this.hasLoyaltyCard = hasLoyaltyCard;
    }

    public boolean isAcceptMarketingMessage() {
        return acceptMarketingMessage;
    }

    public void setAcceptMarketingMessage(boolean acceptMarketingMessage) {
        this.acceptMarketingMessage = acceptMarketingMessage;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    public String getAuditLevel() {
        return auditLevel;
    }

    public void setAuditLevel(String auditLevel) {
        this.auditLevel = auditLevel;
    }

    public String getGoldenShopper() {
        return goldenShopper;
    }

    public void setGoldenShopper(String goldenShopper) {
        this.goldenShopper = goldenShopper;
    }

    public String getLoyaltyCardNumber() {
        return loyaltyCardNumber;
    }

    public void setLoyaltyCardNumber(String loyaltyCardNumber) {
        this.loyaltyCardNumber = loyaltyCardNumber;
    }

    public String getPersonalLoyaltyCard() {
        return personalLoyaltyCard;
    }

    public void setPersonalLoyaltyCard(String personalLoyaltyCard) {
        this.personalLoyaltyCard = personalLoyaltyCard;
    }

    public boolean isShareListWithLoyaltyMembers() {
        return shareListWithLoyaltyMembers;
    }

    public void setShareListWithLoyaltyMembers(boolean shareListWithLoyaltyMembers) {
        this.shareListWithLoyaltyMembers = shareListWithLoyaltyMembers;
    }

    public String getComPendingNumber() {
        return comPendingNumber;
    }

    public void setComPendingNumber(String comPendingNumber) {
        this.comPendingNumber = comPendingNumber;
    }

    public String getLoyaltyCardStatus() {
        return loyaltyCardStatus;
    }

    public void setLoyaltyCardStatus(String loyaltyCardStatus) {
        this.loyaltyCardStatus = loyaltyCardStatus;
    }

    public String getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(String cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLopdCancellation() {
        return lopdCancellation;
    }

    public void setLopdCancellation(String lopdCancellation) {
        this.lopdCancellation = lopdCancellation;
    }

    public String getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(String validationDate) {
        this.validationDate = validationDate;
    }

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public boolean isSendPushOffersCatalogs() {
        return sendPushOffersCatalogs;
    }

    public void setSendPushOffersCatalogs(boolean sendPushOffersCatalogs) {
        this.sendPushOffersCatalogs = sendPushOffersCatalogs;
    }

    public boolean isAcceptSearchHistory() {
        return acceptSearchHistory;
    }

    public void setAcceptSearchHistory(boolean acceptSearchHistory) {
        this.acceptSearchHistory = acceptSearchHistory;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getNbAudits() {
        return nbAudits;
    }

    public void setNbAudits(double nbAudits) {
        this.nbAudits = nbAudits;
    }

    public boolean isUserTest() {
        return userTest;
    }

    public void setUserTest(boolean userTest) {
        this.userTest = userTest;
    }

    public String getAuditAll() {
        return auditAll;
    }

    public void setAuditAll(String auditAll) {
        this.auditAll = auditAll;
    }

    public String getFlagGoldenShopper() {
        return flagGoldenShopper;
    }

    public void setFlagGoldenShopper(String flagGoldenShopper) {
        this.flagGoldenShopper = flagGoldenShopper;
    }

    public String getTypeAuditShopper() {
        return typeAuditShopper;
    }

    public void setTypeAuditShopper(String typeAuditShopper) {
        this.typeAuditShopper = typeAuditShopper;
    }

}