package com.mscanmc.mshop.mscanmc18.ui.cart;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import com.mscanmc.mshop.mscanmc18.R;
import com.mscanmc.mshop.mscanmc18.data.response.BasketItemResponse;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentSavedCartItemBinding;
import com.mscanmc.mshop.mscanmc18.databinding.FragmentSavedShoppingItemBinding;

import java.util.ArrayList;
import java.util.List;

public class CurrentCartRecyclerViewAdapter extends RecyclerView.Adapter<CurrentCartRecyclerViewAdapter.ViewHolder> {

    private  List<BasketItemResponse> mValues  = new ArrayList<>();
    private  UpdateCartList mListener;

    FragmentSavedCartItemBinding mBinging;

    public CurrentCartRecyclerViewAdapter(List<BasketItemResponse> items, UpdateCartList listener) {
        mValues.clear();
        for (BasketItemResponse item : items) {
            if(item.getQuantity() != 0){
                mValues.add(item);
            }
        }
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        mBinging = DataBindingUtil.inflate(inflater, R.layout.fragment_saved_cart_item, parent, false);
        return new ViewHolder(mBinging);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.binding.executePendingBindings();
        mBinging.setShopping(mValues.get(position));
       /* holder.binding.clSavedList.setOnTouchListener(
                new OnSwipeTouchListener(mBinging.getRoot().getContext()) {
                    @Override
                    public void onSwipeLeft() {

                    }

                    @Override
                    public void onSwipeRight() {
                        holder.binding.clSavedList.startAnimation(inFromRightAnimation());
                        mListener.updateCartList(mValues.get(position));
                        notifyDataSetChanged();
                    }

                });*/
    }


    private Animation inFromRightAnimation() {
        Animation inFromRight =
                new TranslateAnimation(
                        Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, +1.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void updateList(List<BasketItemResponse> basket) {
        mValues.clear();
        mValues.addAll(basket);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        FragmentSavedCartItemBinding binding;

        public ViewHolder(FragmentSavedCartItemBinding view) {
            super(view.getRoot());
            binding = view;
        }

    }

    public interface UpdateCartList {
        public void updateCartList(BasketItemResponse basketItemResponse);
    }
}
